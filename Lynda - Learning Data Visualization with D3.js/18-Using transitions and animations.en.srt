1
00:00:00.05 --> 00:00:03.08
- [Instructor] In D3 you can
add animations to elements

2
00:00:03.08 --> 00:00:06.02
with something called transitions.

3
00:00:06.02 --> 00:00:07.03
In their simplest forms,

4
00:00:07.03 --> 00:00:11.02
a transition is a way of
animating a selection.

5
00:00:11.02 --> 00:00:13.06
The main method for creating
a transition of course

6
00:00:13.06 --> 00:00:15.04
is the transition method.

7
00:00:15.04 --> 00:00:17.05
This allows you to set up a transition

8
00:00:17.05 --> 00:00:19.07
and then there are a few other methods

9
00:00:19.07 --> 00:00:20.09
associated with them.

10
00:00:20.09 --> 00:00:23.09
So for example you can
add a duration method,

11
00:00:23.09 --> 00:00:26.09
that let's you modify how
long the transition takes.

12
00:00:26.09 --> 00:00:30.03
By default, they will
last 250 milliseconds.

13
00:00:30.03 --> 00:00:32.01
You can also specify a delay

14
00:00:32.01 --> 00:00:34.07
so that the transition
doesn't start right away,

15
00:00:34.07 --> 00:00:37.00
but after a period of time.

16
00:00:37.00 --> 00:00:40.00
So let's take a look at how
this works in a project.

17
00:00:40.00 --> 00:00:42.08
So we can easily set a transition

18
00:00:42.08 --> 00:00:45.06
on something like an event if we want to,

19
00:00:45.06 --> 00:00:48.00
and what we do is

20
00:00:48.00 --> 00:00:52.00
we can add just a transition
method right here,

21
00:00:52.00 --> 00:00:54.02
and what we should see here is that

22
00:00:54.02 --> 00:00:56.00
the change to the yellow color

23
00:00:56.00 --> 00:00:58.05
takes a little bit longer than before.

24
00:00:58.05 --> 00:01:01.02
Now this means that I also need to have

25
00:01:01.02 --> 00:01:04.02
a transition down here.

26
00:01:04.02 --> 00:01:07.00
And now you can see that
there's a little bit of a delay

27
00:01:07.00 --> 00:01:09.05
before the transition happens,

28
00:01:09.05 --> 00:01:11.05
as I roll over each of the items.

29
00:01:11.05 --> 00:01:14.01
So I can actually make
that a little bit longer

30
00:01:14.01 --> 00:01:16.06
by adding a duration property.

31
00:01:16.06 --> 00:01:19.03
And we'll make it one second,

32
00:01:19.03 --> 00:01:20.08
let's refresh,

33
00:01:20.08 --> 00:01:22.01
and now you can see that

34
00:01:22.01 --> 00:01:24.02
I have to stay in there
a little bit longer.

35
00:01:24.02 --> 00:01:28.08
Let's go ahead and sort of
make a few less of these

36
00:01:28.08 --> 00:01:31.04
bars so that we can see
this a little more clearly.

37
00:01:31.04 --> 00:01:33.08
So now as I roll over you
can see that it's sort of

38
00:01:33.08 --> 00:01:35.09
sliding into that color,

39
00:01:35.09 --> 00:01:37.08
and then

40
00:01:37.08 --> 00:01:39.07
as I get out it's just going to make it

41
00:01:39.07 --> 00:01:43.04
the other color that it use to be.

42
00:01:43.04 --> 00:01:44.09
So those are two of the main components,

43
00:01:44.09 --> 00:01:48.04
you can also add a delay to a transition,

44
00:01:48.04 --> 00:01:50.08
and if you put a number in here,

45
00:01:50.08 --> 00:01:53.03
I got to make sure I put
the period here at the top,

46
00:01:53.03 --> 00:01:55.09
and then we can say 400.

47
00:01:55.09 --> 00:01:58.01
That means that the

48
00:01:58.01 --> 00:02:00.09
effect is not going to take place

49
00:02:00.09 --> 00:02:03.04
until after 400 milliseconds.

50
00:02:03.04 --> 00:02:04.08
So it's not going to happen right away,

51
00:02:04.08 --> 00:02:07.03
it's going to have a
little bit of a delay,

52
00:02:07.03 --> 00:02:09.03
and that looks okay.

53
00:02:09.03 --> 00:02:12.00
I think it's probably not the right effect

54
00:02:12.00 --> 00:02:13.03
for this type of thing,

55
00:02:13.03 --> 00:02:15.03
so I'm going to take these out of here.

56
00:02:15.03 --> 00:02:17.08
And so what I want to do now is actually

57
00:02:17.08 --> 00:02:20.05
show you how you can use a transition

58
00:02:20.05 --> 00:02:24.04
to create how this bar
is actually displayed,

59
00:02:24.04 --> 00:02:26.01
how it's drawn at the beginning.

60
00:02:26.01 --> 00:02:30.04
So I'm back here to this roll
over being instantaneous,

61
00:02:30.04 --> 00:02:33.00
and let's go ahead and
put the semi colon here

62
00:02:33.00 --> 00:02:36.00
kind of at the end of all these commands.

63
00:02:36.00 --> 00:02:38.04
So in order to control the entire graph,

64
00:02:38.04 --> 00:02:41.05
I'm going to need to create a variable

65
00:02:41.05 --> 00:02:43.01
for this chart.

66
00:02:43.01 --> 00:02:47.02
So right now I'm calling D3 directly

67
00:02:47.02 --> 00:02:49.01
and then doing everything

68
00:02:49.01 --> 00:02:52.00
after this sort of initial D3 call.

69
00:02:52.00 --> 00:02:52.08
What I want to do is

70
00:02:52.08 --> 00:02:55.05
actually create a variable for my chart,

71
00:02:55.05 --> 00:02:58.02
I'm going to call it myChart.

72
00:02:58.02 --> 00:03:00.09
And then I'm going to make it
equal to all of these commands,

73
00:03:00.09 --> 00:03:02.08
and if I want to I can
put this on the next line

74
00:03:02.08 --> 00:03:04.03
and we can kind of keep it

75
00:03:04.03 --> 00:03:06.07
in the same way that we had it before.

76
00:03:06.07 --> 00:03:10.04
So what this will do is
allow me to call this chart

77
00:03:10.04 --> 00:03:11.09
whenever I need it,

78
00:03:11.09 --> 00:03:14.03
and what I'm going to need
to do to make this happen

79
00:03:14.03 --> 00:03:16.05
is go ahead

80
00:03:16.05 --> 00:03:20.03
and change the way that our height

81
00:03:20.03 --> 00:03:22.09
so this height is going
to be something that I do

82
00:03:22.09 --> 00:03:24.05
in a separate place,

83
00:03:24.05 --> 00:03:28.00
so I'm going to replace
that with attr('height')

84
00:03:28.00 --> 00:03:29.09
and I'm going to set the
height of these elements

85
00:03:29.09 --> 00:03:32.02
to zero to begin with.

86
00:03:32.02 --> 00:03:35.04
And then I want to do
the same thing for the y.

87
00:03:35.04 --> 00:03:37.04
I want to paste the height,

88
00:03:37.04 --> 00:03:38.08
little method that I had before,

89
00:03:38.08 --> 00:03:40.03
so that I can copy these two,

90
00:03:40.03 --> 00:03:42.02
'cause I'm going to need it down further

91
00:03:42.02 --> 00:03:46.08
when I start to control my
animation away from this element.

92
00:03:46.08 --> 00:03:48.05
So I'm going to cut this out,

93
00:03:48.05 --> 00:03:50.06
and then do attr

94
00:03:50.06 --> 00:03:53.02
and then change the y

95
00:03:53.02 --> 00:03:55.04
to the height.

96
00:03:55.04 --> 00:03:56.05
Alright let's go ahead and save that,

97
00:03:56.05 --> 00:03:58.07
and what should happen is your bars

98
00:03:58.07 --> 00:04:01.03
should just completely
disappear because right now

99
00:04:01.03 --> 00:04:04.01
they don't have any height

100
00:04:04.01 --> 00:04:06.04
or position at all.

101
00:04:06.04 --> 00:04:08.06
And what we'll do is right here,

102
00:04:08.06 --> 00:04:12.00
after this entire sort of
set of drawing commands,

103
00:04:12.00 --> 00:04:13.06
we're going to create

104
00:04:13.06 --> 00:04:17.02
a modification so we'll call myChart,

105
00:04:17.02 --> 00:04:19.05
the variable that I just created,

106
00:04:19.05 --> 00:04:25.01
and then we will set up a
transition for that chart itself.

107
00:04:25.01 --> 00:04:29.05
And then I can paste my methods so now

108
00:04:29.05 --> 00:04:32.09
the height is going to sort of animate to

109
00:04:32.09 --> 00:04:34.03
whatever it use to before,

110
00:04:34.03 --> 00:04:36.01
moving these two properties here,

111
00:04:36.01 --> 00:04:38.06
will allow this height

112
00:04:38.06 --> 00:04:41.06
and position to be animated

113
00:04:41.06 --> 00:04:44.06
so we need to make sure that

114
00:04:44.06 --> 00:04:46.06
we add a couple of things here,

115
00:04:46.06 --> 00:04:48.05
so let's actually save this

116
00:04:48.05 --> 00:04:50.06
and you can see that
it's sort of happening,

117
00:04:50.06 --> 00:04:55.03
and if I refresh, the bars kind
of appear with an animation.

118
00:04:55.03 --> 00:04:57.07
Now if I want to sort of delay those,

119
00:04:57.07 --> 00:04:59.04
I'll show you how to do that.

120
00:04:59.04 --> 00:05:01.09
So I'm going to set a delay here,

121
00:05:01.09 --> 00:05:05.09
and this is going to be a callback,

122
00:05:05.09 --> 00:05:08.06
getting the data as well as the index,

123
00:05:08.06 --> 00:05:11.02
and then I'm going to return

124
00:05:11.02 --> 00:05:12.07
the index

125
00:05:12.07 --> 00:05:14.05
times 20,

126
00:05:14.05 --> 00:05:18.04
so when I do that you'll
see that the bars get drawn,

127
00:05:18.04 --> 00:05:20.06
it's just that they're
not going to to get drawn

128
00:05:20.06 --> 00:05:21.07
at the same time,

129
00:05:21.07 --> 00:05:25.08
they're going to each get
drawn sort of 20 milliseconds

130
00:05:25.08 --> 00:05:28.05
after the previous one,
so that's pretty cool.

131
00:05:28.05 --> 00:05:31.09
And if I want to I can
add a duration here,

132
00:05:31.09 --> 00:05:34.06
to make things last a little bit longer,

133
00:05:34.06 --> 00:05:37.03
or to control how long
those animations happen,

134
00:05:37.03 --> 00:05:38.07
so they're going to take a second,

135
00:05:38.07 --> 00:05:41.00
you can modify this to whatever you want.

136
00:05:41.00 --> 00:05:43.08
There are even ways to control
the speed of the transition

137
00:05:43.08 --> 00:05:47.00
so we can do that with the ease method.

138
00:05:47.00 --> 00:05:48.01
And what you do here

139
00:05:48.01 --> 00:05:52.03
is you add one of the
predefined easing functions,

140
00:05:52.03 --> 00:05:53.06
you can make your own custom ones,

141
00:05:53.06 --> 00:05:56.03
but there are plenty of them available,

142
00:05:56.03 --> 00:05:58.04
one that I'm going to
use right here is called

143
00:05:58.04 --> 00:06:03.00
d3.easeBounceOut

144
00:06:03.00 --> 00:06:05.02
and of course there's an easeBounceIn,

145
00:06:05.02 --> 00:06:06.04
but it doesn't look as good,

146
00:06:06.04 --> 00:06:08.08
so what you get is this little sort of

147
00:06:08.08 --> 00:06:12.08
bouncing at the top or
the end of the transition

148
00:06:12.08 --> 00:06:15.04
and then the values kind of snap in place.

149
00:06:15.04 --> 00:06:17.09
Let me play that one again
'cause it's pretty cool.

150
00:06:17.09 --> 00:06:21.04
I think it works really well
for this kind of bar chart.

151
00:06:21.04 --> 00:06:24.04
Transitions adds a whole new level of fun,

152
00:06:24.04 --> 00:06:26.09
and make your charts come to life.

153
00:06:26.09 --> 00:06:28.07
If you want to learn
more about transitions,

154
00:06:28.07 --> 00:06:32.07
make sure you check this
URL for more information.

