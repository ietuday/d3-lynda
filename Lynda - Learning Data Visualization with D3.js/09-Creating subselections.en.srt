1
00:00:00.05 --> 00:00:01.08
- [Narrator] So far we've been working

2
00:00:01.08 --> 00:00:03.06
with existing DOM elements.

3
00:00:03.06 --> 00:00:05.03
In a real project you usually

4
00:00:05.03 --> 00:00:08.02
create these elements programmatically.

5
00:00:08.02 --> 00:00:11.06
D3 let's you create a
placeholder for these elements

6
00:00:11.06 --> 00:00:13.08
and then que up commands for them.

7
00:00:13.08 --> 00:00:15.03
So let's take a look at how that works.

8
00:00:15.03 --> 00:00:17.07
First of all, the enter method changes

9
00:00:17.07 --> 00:00:19.08
the way that the data method works.

10
00:00:19.08 --> 00:00:22.04
Once you use enter, you're
no longer appending things

11
00:00:22.04 --> 00:00:24.04
to the current selection,
you're placing them

12
00:00:24.04 --> 00:00:28.00
into something D3 calls a Sub-selection.

13
00:00:28.00 --> 00:00:30.01
These Sub-selections allow you to isolate

14
00:00:30.01 --> 00:00:33.06
elements we want to work
with and then act upon them.

15
00:00:33.06 --> 00:00:36.03
Now, if you do need to
exit a Sub-selection

16
00:00:36.03 --> 00:00:38.03
you can use the exit method.

17
00:00:38.03 --> 00:00:39.04
So let's take a look at how this works

18
00:00:39.04 --> 00:00:41.02
and we're going to create a simpler

19
00:00:41.02 --> 00:00:44.02
version of our table programmatically.

20
00:00:44.02 --> 00:00:46.00
So the first thing I
want to do is just get

21
00:00:46.00 --> 00:00:49.02
rid of everything inside the tbody

22
00:00:49.02 --> 00:00:52.03
and we'll just leave
it empty for right now.

23
00:00:52.03 --> 00:00:54.02
And then let's go ahead and clear out

24
00:00:54.02 --> 00:00:56.06
our previous script
from the previous video.

25
00:00:56.06 --> 00:00:58.00
And what we want to do is first

26
00:00:58.00 --> 00:01:00.03
define the data that we want to use.

27
00:01:00.03 --> 00:01:04.00
So I'm going to create
a variable called mydata

28
00:01:04.00 --> 00:01:06.01
and I'm going to pass along as usual,

29
00:01:06.01 --> 00:01:07.08
an array into this variable.

30
00:01:07.08 --> 00:01:09.04
Now, if you want to you can create more

31
00:01:09.04 --> 00:01:13.00
complex data elements by using Objects.

32
00:01:13.00 --> 00:01:15.07
So instead of adding specific values here

33
00:01:15.07 --> 00:01:19.07
I can create an Object and the
first one is going to have a

34
00:01:19.07 --> 00:01:24.08
property of date and then in
here I'm going to add some dates.

35
00:01:24.08 --> 00:01:28.09
I'm also going to add a
variable for the low temperature

36
00:01:28.09 --> 00:01:34.07
as well as the high temperature.

37
00:01:34.07 --> 00:01:36.06
Alright, once we have this data

38
00:01:36.06 --> 00:01:39.09
then we can target our selection.

39
00:01:39.09 --> 00:01:41.07
And another thing that you can do with D3

40
00:01:41.07 --> 00:01:44.02
is actually add multiple selections.

41
00:01:44.02 --> 00:01:47.08
So you can use the D3
method to select something

42
00:01:47.08 --> 00:01:50.08
and in here we can
target the tbody element.

43
00:01:50.08 --> 00:01:54.00
So that would be the
main body of our table,

44
00:01:54.00 --> 00:01:55.09
where we want to insert the data.

45
00:01:55.09 --> 00:01:59.06
And then with this script we
can target another selection

46
00:01:59.06 --> 00:02:02.01
by using another selection method.

47
00:02:02.01 --> 00:02:04.03
So we can actually say go get the tbody

48
00:02:04.03 --> 00:02:08.05
and then find something called selectAll.

49
00:02:08.05 --> 00:02:12.06
And this is going to be a tr
and is one of the interesting

50
00:02:12.06 --> 00:02:16.01
things about using the enter method.

51
00:02:16.01 --> 00:02:18.08
So this is actually a
little bit of time travel.

52
00:02:18.08 --> 00:02:20.09
We want to create some elements

53
00:02:20.09 --> 00:02:24.04
and then we want to insert
things into those elements.

54
00:02:24.04 --> 00:02:26.03
So the first thing we need to do

55
00:02:26.03 --> 00:02:29.06
is actually create the element
or refer to the element

56
00:02:29.06 --> 00:02:32.07
but that element doesn't
actually yet exist.

57
00:02:32.07 --> 00:02:35.08
So this selectAll is
selecting something that is

58
00:02:35.08 --> 00:02:38.06
not yet in this index, that html file.

59
00:02:38.06 --> 00:02:40.00
We're going to refer to it in just

60
00:02:40.00 --> 00:02:42.08
a minute with our enter command.

61
00:02:42.08 --> 00:02:45.02
So then I'm going to pass along the data

62
00:02:45.02 --> 00:02:49.01
using the data method
and pass it along mydata.

63
00:02:49.01 --> 00:02:52.08
And then I'm going to use
the enter command here

64
00:02:52.08 --> 00:02:59.01
to enter this selection and
then I'm going to append a tr.

65
00:02:59.01 --> 00:03:01.08
So this is going to be
the tr's that we are going

66
00:03:01.08 --> 00:03:04.03
to be inserting with the data that

67
00:03:04.03 --> 00:03:06.06
we're going to get from this data command.

68
00:03:06.06 --> 00:03:08.02
So something weird is happening here

69
00:03:08.02 --> 00:03:10.04
and I want to point your attention to it.

70
00:03:10.04 --> 00:03:14.02
I'm appending these tr's but
I'm also selecting them above.

71
00:03:14.02 --> 00:03:15.06
So you can see right here on this line

72
00:03:15.06 --> 00:03:20.05
I am asking D3 to select
tr's that don't yet exist

73
00:03:20.05 --> 00:03:24.05
and I am actually appending
those tr's right here.

74
00:03:24.05 --> 00:03:26.02
So it's a little bit of time travel

75
00:03:26.02 --> 00:03:28.06
and you just have to remember
that you're going to make a

76
00:03:28.06 --> 00:03:30.06
selection on something that doesn't exist

77
00:03:30.06 --> 00:03:33.00
and then use the enter command to enter

78
00:03:33.00 --> 00:03:36.05
that selection or each individual tr.

79
00:03:36.05 --> 00:03:38.09
And then you have to
make sure that you append

80
00:03:38.09 --> 00:03:41.07
the tr's so that they actually exist.

81
00:03:41.07 --> 00:03:42.06
So let's keep on going.

82
00:03:42.06 --> 00:03:46.03
Then we can use any one of our functions.

83
00:03:46.03 --> 00:03:49.02
We'll go ahead and use the html function

84
00:03:49.02 --> 00:03:52.09
and pass along the data here.

85
00:03:52.09 --> 00:03:56.06
And so now we can do something
with each individual tr

86
00:03:56.06 --> 00:03:59.03
that we created by
using the append command

87
00:03:59.03 --> 00:04:02.06
and we select it by using selectAll.

88
00:04:02.06 --> 00:04:05.06
So what we want to do here is just return

89
00:04:05.06 --> 00:04:07.08
and this will just be sort of a

90
00:04:07.08 --> 00:04:10.09
normal series of table commands.

91
00:04:10.09 --> 00:04:18.01
So th scope is row here and
then we'll refer to the date.

92
00:04:18.01 --> 00:04:21.07
So this variable d contains the data

93
00:04:21.07 --> 00:04:24.02
that we're getting from this data command.

94
00:04:24.02 --> 00:04:27.03
So we can use that and then plus,

95
00:04:27.03 --> 00:04:30.07
then use our date property.

96
00:04:30.07 --> 00:04:32.06
So each one of these will come up

97
00:04:32.06 --> 00:04:36.05
as a property under the data.

98
00:04:36.05 --> 00:04:39.03
Then let's go ahead and close that th

99
00:04:39.03 --> 00:04:45.06
and create a new td and we'll
use the low temperature here.

100
00:04:45.06 --> 00:04:48.05
We will create another line here.

101
00:04:48.05 --> 00:04:50.08
You can put these all in
one line if you wanted to.

102
00:04:50.08 --> 00:04:55.05
We'll close out the td
and then open up a new one

103
00:04:55.05 --> 00:05:00.01
and into this new one we'll
place the high temperature.

104
00:05:00.01 --> 00:05:04.06
And then we will close out the td.

105
00:05:04.06 --> 00:05:08.00
So if we save that, we should
get all the information.

106
00:05:08.00 --> 00:05:10.08
It looks like I missed something here.

107
00:05:10.08 --> 00:05:16.04
So d.date and it just looks like I need to

108
00:05:16.04 --> 00:05:20.03
close out this tag right here and then we

109
00:05:20.03 --> 00:05:22.04
should be able to see the dates.

110
00:05:22.04 --> 00:05:24.07
This is definitely a
simpler version of what

111
00:05:24.07 --> 00:05:28.03
we had done before but
as you can see we can

112
00:05:28.03 --> 00:05:31.07
actually create all the data dynamically

113
00:05:31.07 --> 00:05:33.09
by using the enter command.

114
00:05:33.09 --> 00:05:36.04
As long as we remember that
we first have to select the

115
00:05:36.04 --> 00:05:40.06
element that we're going to
append into the enter command.

