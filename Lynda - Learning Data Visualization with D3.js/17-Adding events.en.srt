1
00:00:00.06 --> 00:00:02.02
- [Narrator] Events are an integral part

2
00:00:02.02 --> 00:00:04.05
to building any type of interactivity.

3
00:00:04.05 --> 00:00:07.01
And D3 provides it's
own way of handling them

4
00:00:07.01 --> 00:00:09.09
and then react when those happen.

5
00:00:09.09 --> 00:00:12.08
Now you call events with
the on event handler

6
00:00:12.08 --> 00:00:16.01
and then you use any
traditional JavaScript calls

7
00:00:16.01 --> 00:00:17.06
in order to respond to them.

8
00:00:17.06 --> 00:00:20.05
So you can use things
like mouseout, mouseover,

9
00:00:20.05 --> 00:00:23.05
or anything else that
your browser responds to.

10
00:00:23.05 --> 00:00:26.04
Once you have an event
then what you want to do

11
00:00:26.04 --> 00:00:29.02
is modify the properties of that element.

12
00:00:29.02 --> 00:00:32.00
And in order to get to
the specific properties

13
00:00:32.00 --> 00:00:34.08
of the current element
that you're targeting

14
00:00:34.08 --> 00:00:38.00
with the event, you use the this keyword.

15
00:00:38.00 --> 00:00:40.00
So let's take a look at how that works.

16
00:00:40.00 --> 00:00:41.09
At the very end of our script,

17
00:00:41.09 --> 00:00:44.02
I'm going to add an event handler here.

18
00:00:44.02 --> 00:00:48.07
So I'll do on and then I'll
target first the mouseover

19
00:00:48.07 --> 00:00:51.00
and this is going to
of course, like usual,

20
00:00:51.00 --> 00:00:53.05
be a callback.

21
00:00:53.05 --> 00:00:56.00
And once I am here, then I need to target

22
00:00:56.00 --> 00:01:00.01
the current bar that I
am on so I'll use a D3

23
00:01:00.01 --> 00:01:05.09
and then select the current
element using the this keyword.

24
00:01:05.09 --> 00:01:11.02
And then I can change the
opacity to anything I want to.

25
00:01:11.02 --> 00:01:14.06
So I'll do .5 right now and
now when I roll over one

26
00:01:14.06 --> 00:01:17.07
of these bars, it turns at 50% opaque.

27
00:01:17.07 --> 00:01:19.06
If I want to undo that,

28
00:01:19.06 --> 00:01:22.01
then I'm going to target another event.

29
00:01:22.01 --> 00:01:24.01
So I'll just copy this
one cause it's pretty much

30
00:01:24.01 --> 00:01:25.05
the same thing.

31
00:01:25.05 --> 00:01:30.03
And instead of mouseover,
I'll target mouseout.

32
00:01:30.03 --> 00:01:33.07
And change the opacity back to one.

33
00:01:33.07 --> 00:01:36.00
So when I roll over,
as soon as I roll out,

34
00:01:36.00 --> 00:01:39.07
it will set it self back
to the original opacity.

35
00:01:39.07 --> 00:01:41.00
If I want to do a color,

36
00:01:41.00 --> 00:01:43.02
things are going to be
a little bit different.

37
00:01:43.02 --> 00:01:46.02
First, I'm going to set
up a variable up here.

38
00:01:46.02 --> 00:01:50.09
Call it tempColor, just
to hold a temporary color

39
00:01:50.09 --> 00:01:54.01
when I roll over and out.

40
00:01:54.01 --> 00:01:55.08
So I'll go back down here

41
00:01:55.08 --> 00:02:00.07
and what I'm going to do is
set that variable tempColor

42
00:02:00.07 --> 00:02:04.01
to the current fill.

43
00:02:04.01 --> 00:02:06.02
And then when I select this,

44
00:02:06.02 --> 00:02:12.00
let's go ahead and change
the fill to some color.

45
00:02:12.00 --> 00:02:14.02
We can just use yellow here for now.

46
00:02:14.02 --> 00:02:18.01
And let's make sure that a
common mistake is forgetting

47
00:02:18.01 --> 00:02:20.06
to put a comma when you
create a new variable.

48
00:02:20.06 --> 00:02:21.06
So let's go ahead and save that.

49
00:02:21.06 --> 00:02:24.09
And you can see that
everything's working fine now.

50
00:02:24.09 --> 00:02:27.02
And as I roll over these elements,

51
00:02:27.02 --> 00:02:29.01
it's going to turn them yellow.

52
00:02:29.01 --> 00:02:30.04
So if I need to switch it back,

53
00:02:30.04 --> 00:02:35.00
since I've stored the color
in this temp color variable,

54
00:02:35.00 --> 00:02:41.03
then all I need to do
is change the style fill

55
00:02:41.03 --> 00:02:44.05
on mouse out to that temp color.

56
00:02:44.05 --> 00:02:47.01
Alright, so if we save that,
you see that the colors

57
00:02:47.01 --> 00:02:48.06
are working just fine.

58
00:02:48.06 --> 00:02:51.04
Roll over and when I roll
out, it's going to switch back

59
00:02:51.04 --> 00:02:55.03
to whatever that color in
the temp color variable is.

60
00:02:55.03 --> 00:02:59.03
I don't really need this
opacity here or here

61
00:02:59.03 --> 00:03:01.07
because it's really not
doing anything with them.

62
00:03:01.07 --> 00:03:05.01
It's just changing them
to that color and back.

63
00:03:05.01 --> 00:03:06.09
So events are pretty easy to understand

64
00:03:06.09 --> 00:03:09.00
and we will have other opportunities

65
00:03:09.00 --> 00:03:12.01
to work with them in later videos.

66
00:03:12.01 --> 00:03:15.00
If you're curious about events in D3,

67
00:03:15.00 --> 00:03:17.02
make sure you check out this URL

68
00:03:17.02 --> 00:03:19.07
for all the information about events

69
00:03:19.07 --> 00:03:22.00
from the API documentation.

