1
00:00:00.02 --> 00:00:01.07
- [Instructor] The code for this course

2
00:00:01.07 --> 00:00:06.01
is freely available in GitHub
for all users at this URL.

3
00:00:06.01 --> 00:00:07.06
In GitHub, you'll see that the project

4
00:00:07.06 --> 00:00:09.09
is organized into branches.

5
00:00:09.09 --> 00:00:12.08
Each branch corresponds
to a video in the course.

6
00:00:12.08 --> 00:00:14.05
They are named according to the video

7
00:00:14.05 --> 00:00:16.03
that they correspond to.

8
00:00:16.03 --> 00:00:21.00
If you see a branch that
starts with a name like 01_05b,

9
00:00:21.00 --> 00:00:22.03
that means that that branch

10
00:00:22.03 --> 00:00:26.02
corresponds to the first
chapter and the fifth movie.

11
00:00:26.02 --> 00:00:28.05
Also, if you see a B
at the end of the name,

12
00:00:28.05 --> 00:00:31.08
those are how the files look
at the beginning of the video.

13
00:00:31.08 --> 00:00:33.07
And if you see an E at
the end of the name,

14
00:00:33.07 --> 00:00:36.06
that's how the files look
at the end of the video.

15
00:00:36.06 --> 00:00:38.00
The best way to work with a project

16
00:00:38.00 --> 00:00:40.08
is to clone all these branches at once.

17
00:00:40.08 --> 00:00:43.00
In order to do that, you'll need Node.js

18
00:00:43.00 --> 00:00:45.00
as well as Git installed.

19
00:00:45.00 --> 00:00:47.03
And you can find Git and
download the installer

20
00:00:47.03 --> 00:00:49.09
for your platform at this URL.

21
00:00:49.09 --> 00:00:52.03
When you install Git,
make sure that you add

22
00:00:52.03 --> 00:00:55.01
the Git BASH Terminal option in Windows.

23
00:00:55.01 --> 00:00:58.00
It makes running the
commands more consistent.

24
00:00:58.00 --> 00:00:59.02
So let me show you how to clone

25
00:00:59.02 --> 00:01:01.06
all the branches for this project.

26
00:01:01.06 --> 00:01:04.01
First, I'm going to go to the GitHub Repo,

27
00:01:04.01 --> 00:01:06.05
and then click on this button

28
00:01:06.05 --> 00:01:08.04
and click on this icon right here

29
00:01:08.04 --> 00:01:11.01
to copy the Git link to the clipboard.

30
00:01:11.01 --> 00:01:13.00
I'm going to switch over to a terminal,

31
00:01:13.00 --> 00:01:14.02
and I'm using in a Mac,

32
00:01:14.02 --> 00:01:17.02
so I'm going to use the
terminal application.

33
00:01:17.02 --> 00:01:20.00
On a PC, you can run Git BASH.

34
00:01:20.00 --> 00:01:24.06
And so I'm going to type CD
and then tilde slash desktop.

35
00:01:24.06 --> 00:01:26.02
So switch over to the desktop.

36
00:01:26.02 --> 00:01:30.05
Then I'll create a directory
with the MKDIR command

37
00:01:30.05 --> 00:01:32.00
and give it a name.

38
00:01:32.00 --> 00:01:34.07
I'll call mine D3.

39
00:01:34.07 --> 00:01:38.09
And then I'll do a CD D3 command to switch

40
00:01:38.09 --> 00:01:40.02
into that directory.

41
00:01:40.02 --> 00:01:41.07
You can see that the folder appears

42
00:01:41.07 --> 00:01:43.04
right here on the desktop.

43
00:01:43.04 --> 00:01:47.07
And once I do that, I can
issue a Git Clone command

44
00:01:47.07 --> 00:01:50.03
with the minus minus bare option.

45
00:01:50.03 --> 00:01:53.01
And then, I'm going to
paste the URL from Git

46
00:01:53.01 --> 00:01:56.05
and add an extra .git at the end.

47
00:01:56.05 --> 00:01:58.06
This is going to download
just the contents

48
00:01:58.06 --> 00:02:00.06
of the Git folder.

49
00:02:00.06 --> 00:02:03.08
Now we need to convert this
to a regular repository.

50
00:02:03.08 --> 00:02:06.09
So I'm going to use the Git Config command

51
00:02:06.09 --> 00:02:12.00
and change a variable
called core.bare to false.

52
00:02:12.00 --> 00:02:13.07
Once I do this,

53
00:02:13.07 --> 00:02:16.09
our folder will no longer
be a bare repository.

54
00:02:16.09 --> 00:02:19.03
Now, if I issue a Git reset command

55
00:02:19.03 --> 00:02:22.05
with the minus minus hard option,

56
00:02:22.05 --> 00:02:25.09
everything will be converted
to a regular repository.

57
00:02:25.09 --> 00:02:27.05
Let's go ahead and clear this out.

58
00:02:27.05 --> 00:02:29.06
And if we do a Git Branch command,

59
00:02:29.06 --> 00:02:31.05
we should be able to
see all of the branches

60
00:02:31.05 --> 00:02:32.09
for the project.

61
00:02:32.09 --> 00:02:34.02
The next thing we need to do

62
00:02:34.02 --> 00:02:36.08
is install the project dependencies.

63
00:02:36.08 --> 00:02:41.00
In order to do that, I need
to use an NPM install command.

64
00:02:41.00 --> 00:02:43.00
Depending on your permissions on a Mac,

65
00:02:43.00 --> 00:02:45.03
you may need to type in S-U-D-O

66
00:02:45.03 --> 00:02:48.01
at the beginning of this command.

67
00:02:48.01 --> 00:02:50.00
All right, this is done
installing dependencies.

68
00:02:50.00 --> 00:02:51.08
Let's go ahead and clear the screen.

69
00:02:51.08 --> 00:02:53.04
So once the installation is done,

70
00:02:53.04 --> 00:02:56.06
I can switch to any of
the branches I want to,

71
00:02:56.06 --> 00:02:58.07
using the Checkout command.

72
00:02:58.07 --> 00:03:01.01
The default branch is the master branch.

73
00:03:01.01 --> 00:03:02.07
We're already on that.

74
00:03:02.07 --> 00:03:05.04
But you can type in any
other branch you want to,

75
00:03:05.04 --> 00:03:07.03
if you want to check that out.

76
00:03:07.03 --> 00:03:11.00
This project uses Gulp JS
to create and run a server,

77
00:03:11.00 --> 00:03:14.00
which is required for
these type of projects

78
00:03:14.00 --> 00:03:15.05
that use AJAX.

79
00:03:15.05 --> 00:03:18.02
In order to get it going,
just run the Gulp command

80
00:03:18.02 --> 00:03:22.01
and this will open up the
project in your default browser.

81
00:03:22.01 --> 00:03:24.04
You should start the videos
with the project opened

82
00:03:24.04 --> 00:03:26.04
in your favorite text editor.

83
00:03:26.04 --> 00:03:29.07
I'm going to use Visual
Studio Code for this course.

84
00:03:29.07 --> 00:03:33.06
I'll usually have either
deindexed that HTML document open,

85
00:03:33.06 --> 00:03:37.01
or the Javascript
document in the JS folder

86
00:03:37.01 --> 00:03:40.08
called Script.js.

87
00:03:40.08 --> 00:03:42.04
I'll start each video in the course

88
00:03:42.04 --> 00:03:45.01
with the Gulp command already
running in the background

89
00:03:45.01 --> 00:03:47.04
and the code running inside our editor

90
00:03:47.04 --> 00:03:50.02
as well as a copy of the browser.

91
00:03:50.02 --> 00:03:52.03
All right, so it's time to get started.

