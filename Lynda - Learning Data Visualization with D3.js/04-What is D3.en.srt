1
00:00:00.02 --> 00:00:01.04
- [Instructor] Before we get started,

2
00:00:01.04 --> 00:00:03.04
let's talk about what D3 is,

3
00:00:03.04 --> 00:00:05.06
and discuss some of its features.

4
00:00:05.06 --> 00:00:09.06
D3 is a framework for building
data driven visualizations.

5
00:00:09.06 --> 00:00:13.04
It let's you build graphics
that use common web standards,

6
00:00:13.04 --> 00:00:16.00
which makes the graphics easily accessible

7
00:00:16.00 --> 00:00:17.08
through any web browser.

8
00:00:17.08 --> 00:00:19.03
That means that you can use JavaScript

9
00:00:19.03 --> 00:00:23.00
to generate regular HTML
and CSS visualizations,

10
00:00:23.00 --> 00:00:25.08
or the slightly more flexible and advanced

11
00:00:25.08 --> 00:00:29.05
scalable vector graphics, known as SVG.

12
00:00:29.05 --> 00:00:31.04
D3 uses conventions that are familiar

13
00:00:31.04 --> 00:00:34.06
to most advanced users,
so learning the language

14
00:00:34.06 --> 00:00:37.04
should be pretty easy for
web designers and developers

15
00:00:37.04 --> 00:00:39.05
who understand JavaScript.

16
00:00:39.05 --> 00:00:43.07
D3 uses a CSS-like format
for selecting elements,

17
00:00:43.07 --> 00:00:47.01
so if you already know how
to work with CSS or jQuery,

18
00:00:47.01 --> 00:00:49.09
you should already know
how to make D3 selections.

19
00:00:49.09 --> 00:00:52.05
If you have some experience
working with jQuery,

20
00:00:52.05 --> 00:00:55.06
where the results of one
function are fed to another,

21
00:00:55.06 --> 00:00:58.02
then D3 will be really easy to pick up.

22
00:00:58.02 --> 00:01:00.03
A lot of what you do in D3 is issue

23
00:01:00.03 --> 00:01:02.04
an entire state of drawn instructions

24
00:01:02.04 --> 00:01:05.04
stringed together as a single command.

25
00:01:05.04 --> 00:01:07.00
Once you create your graphics,

26
00:01:07.00 --> 00:01:09.01
you don't use some strange
proprietary language

27
00:01:09.01 --> 00:01:11.04
to style things, you can style things

28
00:01:11.04 --> 00:01:13.06
through CSS commands just as you would

29
00:01:13.06 --> 00:01:15.09
any other page element.

30
00:01:15.09 --> 00:01:17.09
D3 makes parsing, or the process of

31
00:01:17.09 --> 00:01:21.02
importing data from common
formats, super simple.

32
00:01:21.02 --> 00:01:23.00
Building parsers yourself can be

33
00:01:23.00 --> 00:01:24.07
programmatically challenging,

34
00:01:24.07 --> 00:01:27.00
but it is baked right into D3

35
00:01:27.00 --> 00:01:29.00
so you don't have to worry about it.

36
00:01:29.00 --> 00:01:31.09
You can then easily
bind data to functions,

37
00:01:31.09 --> 00:01:35.02
and execute a function
on a sequence of data.

38
00:01:35.02 --> 00:01:37.07
We're going to be taking a
look at that in this course.

39
00:01:37.07 --> 00:01:41.00
D3 provides a rich language
for building transitions

40
00:01:41.00 --> 00:01:43.04
and animations, which makes building

41
00:01:43.04 --> 00:01:46.00
interactive graphics super simple.

42
00:01:46.00 --> 00:01:48.09
Overall, you'll find that
creating graphics with D3

43
00:01:48.09 --> 00:01:52.03
will be easy once you learn
the basics of the language,

44
00:01:52.03 --> 00:01:54.09
and that's what this course is all about.

45
00:01:54.09 --> 00:01:56.06
So let's keep going.

