1
00:00:00.06 --> 00:00:01.09
- [Instructor] This is
not a beginner course,

2
00:00:01.09 --> 00:00:04.05
so there's a few things that
you should be comfortable with

3
00:00:04.05 --> 00:00:06.08
before you take this course.

4
00:00:06.08 --> 00:00:10.06
I'm assuming that you already
know HTML and CSS pretty well

5
00:00:10.06 --> 00:00:13.06
and are generally comfortable
building websites.

6
00:00:13.06 --> 00:00:15.00
If you need more help with HTML,

7
00:00:15.00 --> 00:00:18.02
make sure you check out
HTML Essential Training.

8
00:00:18.02 --> 00:00:20.06
You should also be pretty
comfortable with JavaScript,

9
00:00:20.06 --> 00:00:22.02
so if you need help with that,

10
00:00:22.02 --> 00:00:24.09
check out JavaScript Essential Training.

11
00:00:24.09 --> 00:00:28.00
In this course, we're also
going to be using a JSON file,

12
00:00:28.00 --> 00:00:31.01
so you should be familiar
with JSON as a format,

13
00:00:31.01 --> 00:00:33.02
as well as JavaScript objects.

14
00:00:33.02 --> 00:00:34.02
If you need help with that,

15
00:00:34.02 --> 00:00:37.01
check out JavaScript and JSON.

16
00:00:37.01 --> 00:00:38.03
Finally, you should also know how

17
00:00:38.03 --> 00:00:42.03
to work with modern build
tools, like GitHub and Gulp.js.

18
00:00:42.03 --> 00:00:44.01
If you need help with
that, check out the course,

19
00:00:44.01 --> 00:00:48.06
Web Project Workflows with
Gulp.js, Git, and Browserify.

20
00:00:48.06 --> 00:00:50.01
If you need help with Git and GitHub,

21
00:00:50.01 --> 00:00:53.01
make sure you check out
Learning Git and GitHub.

22
00:00:53.01 --> 00:00:54.09
If you feel comfortable
with these requirements,

23
00:00:54.09 --> 00:00:57.01
then it's time to get started.

