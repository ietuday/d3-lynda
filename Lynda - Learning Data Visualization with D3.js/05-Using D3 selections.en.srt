1
00:00:00.09 --> 00:00:02.09
- [Instructor] In order
to do anything with D3

2
00:00:02.09 --> 00:00:06.00
the first thing you need to
learn is how to select elements.

3
00:00:06.00 --> 00:00:08.07
Now, D3 has its own set
of selection methods

4
00:00:08.07 --> 00:00:12.06
that are a lot like JQuery and
Javascript selection methods

5
00:00:12.06 --> 00:00:14.04
Now, to get started,
we're going to take a look

6
00:00:14.04 --> 00:00:17.04
at how to make selections
using the traditional DOM,

7
00:00:17.04 --> 00:00:20.01
which, if you have some
experience working with CSS,

8
00:00:20.01 --> 00:00:22.07
should be something you're familiar with.

9
00:00:22.07 --> 00:00:25.08
Now, D3 comes with
selectors for DOM elements.

10
00:00:25.08 --> 00:00:28.08
The DOM, of course, is
the Document Object Model,

11
00:00:28.08 --> 00:00:31.08
or the structure of any HTML page.

12
00:00:31.08 --> 00:00:34.05
If you've used CSS before,
you should be familiar

13
00:00:34.05 --> 00:00:38.04
with how to target DOM elements
by using CSS selectors.

14
00:00:38.04 --> 00:00:42.05
There are two D3 methods
for selecting DOM elements.

15
00:00:42.05 --> 00:00:45.09
The first is called select
and then you add a string

16
00:00:45.09 --> 00:00:48.06
of selectors, so this should be in quotes.

17
00:00:48.06 --> 00:00:51.01
select will choose the
first element in the DOM

18
00:00:51.01 --> 00:00:55.02
that matches the CSS selector
string that you specify.

19
00:00:55.02 --> 00:00:57.07
selectAll, in contrast,
will return something

20
00:00:57.07 --> 00:01:00.01
that looks like an array of elements.

21
00:01:00.01 --> 00:01:02.03
It's not exactly an array and you can't do

22
00:01:02.03 --> 00:01:04.09
all the things that you
can do with Javascript

23
00:01:04.09 --> 00:01:07.08
to that array-like
element, but it pretty much

24
00:01:07.08 --> 00:01:09.03
works like one.

25
00:01:09.03 --> 00:01:11.07
Once you have a selection,
it can be automatically

26
00:01:11.07 --> 00:01:15.07
acted upon by passing other D3 methods.

27
00:01:15.07 --> 00:01:18.05
The string you specify is any CSS selector

28
00:01:18.05 --> 00:01:21.05
that you're used to working
with to target an element.

29
00:01:21.05 --> 00:01:24.01
So, this is just like
using a querySelector

30
00:01:24.01 --> 00:01:27.09
or querySelectorAll in
Javascript or the dollar sign

31
00:01:27.09 --> 00:01:30.00
selector in JQuery.

32
00:01:30.00 --> 00:01:31.09
Now, this only selects the element

33
00:01:31.09 --> 00:01:33.09
and in order to do something with them,

34
00:01:33.09 --> 00:01:38.02
you'll need to use a variety
of methods that D3 provides.

35
00:01:38.02 --> 00:01:40.08
Once such method is the text method,

36
00:01:40.08 --> 00:01:42.09
which replaces the text of the element

37
00:01:42.09 --> 00:01:44.08
with whatever you type in there.

38
00:01:44.08 --> 00:01:46.09
We'll take a look at
that in just a minute.

39
00:01:46.09 --> 00:01:50.00
The nice thing about D3 is that
you can chain the selectors

40
00:01:50.00 --> 00:01:53.02
together to form complex commands.

41
00:01:53.02 --> 00:01:55.01
So, let's take a look at this in action.

42
00:01:55.01 --> 00:01:58.02
I've got a simple page
right here, with a table.

43
00:01:58.02 --> 00:02:00.08
This is a bootstrap table, so you can see

44
00:02:00.08 --> 00:02:04.01
that I've added a class of
table and something called

45
00:02:04.01 --> 00:02:06.02
table-hover, which gives
you this nice, sort of,

46
00:02:06.02 --> 00:02:10.01
hover effect and then inside
here I have a header section

47
00:02:10.01 --> 00:02:14.03
and then a body section with
a bunch of these different

48
00:02:14.03 --> 00:02:15.04
TRs.

49
00:02:15.04 --> 00:02:18.04
This first one right
here has a table heading

50
00:02:18.04 --> 00:02:21.03
which allows you to select these elements

51
00:02:21.03 --> 00:02:23.06
and then each one of the temperatures

52
00:02:23.06 --> 00:02:25.08
are in either a day-low

53
00:02:25.08 --> 00:02:26.08
class

54
00:02:26.08 --> 00:02:28.06
or a day-high class

55
00:02:28.06 --> 00:02:32.02
and each one of those is
attached to a TD, notice that,

56
00:02:32.02 --> 00:02:36.03
and then I have the actual
temperature inside a SPAN

57
00:02:36.03 --> 00:02:38.07
with a class of temp.

58
00:02:38.07 --> 00:02:41.06
So, let's say that we
wanted to target an element.

59
00:02:41.06 --> 00:02:45.02
Let's open up our script.js file

60
00:02:45.02 --> 00:02:47.05
and whenever you want
to do anything with D3

61
00:02:47.05 --> 00:02:51.03
you'll notice that is
name-spaced to the D3 method,

62
00:02:51.03 --> 00:02:55.00
so you always start by
saying D3 and then something.

63
00:02:55.00 --> 00:02:58.08
So, we can say: D3 and
then use our select method

64
00:02:58.08 --> 00:03:00.07
and then include something in here.

65
00:03:00.07 --> 00:03:03.01
So, let's say that we
want to target the element

66
00:03:03.01 --> 00:03:06.00
with a class of temperature or temp

67
00:03:06.00 --> 00:03:09.07
and then we want to modify
the text inside that element

68
00:03:09.07 --> 00:03:13.06
to something that says hot.

69
00:03:13.06 --> 00:03:15.08
So, if we save that, you'll
notice that it grabbed

70
00:03:15.08 --> 00:03:20.02
the very first element
that had a class of temp

71
00:03:20.02 --> 00:03:22.04
and it replaced the text in there

72
00:03:22.04 --> 00:03:23.05
with this

73
00:03:23.05 --> 00:03:24.04
text

74
00:03:24.04 --> 00:03:25.07
of hot.

75
00:03:25.07 --> 00:03:27.02
So, that's pretty cool.

76
00:03:27.02 --> 00:03:29.06
Now, let's see what
happens if we change this

77
00:03:29.06 --> 00:03:31.07
to a selectAll method.

78
00:03:31.07 --> 00:03:33.06
So, let's go ahead and save that

79
00:03:33.06 --> 00:03:37.06
and notice now that all the
temperatures have been replaced.

80
00:03:37.06 --> 00:03:39.08
Now, let's say that we
wanted to only replace

81
00:03:39.08 --> 00:03:41.07
the high temperatures.

82
00:03:41.07 --> 00:03:43.09
So, we could say: selectAll

83
00:03:43.09 --> 00:03:46.04
and then in addition to temp,

84
00:03:46.04 --> 00:03:50.05
we could specify that we
want the day-high temperature

85
00:03:50.05 --> 00:03:52.08
and if we save that you can
see that it selected only

86
00:03:52.08 --> 00:03:54.03
the high temperatures.

87
00:03:54.03 --> 00:03:55.04
That's pretty convenient.

88
00:03:55.04 --> 00:03:57.02
It let's you just add a bunch of classes

89
00:03:57.02 --> 00:04:00.05
just like you would do in
CSS to target any element

90
00:04:00.05 --> 00:04:02.02
in your DOM.

91
00:04:02.02 --> 00:04:04.03
So, in addition to this,
if we want to target

92
00:04:04.03 --> 00:04:08.00
a specific element number
we can use something like

93
00:04:08.00 --> 00:04:10.06
the nth-child selector in CSS.

94
00:04:10.06 --> 00:04:13.05
So, in addition to doing
all this, we could say:

95
00:04:13.05 --> 00:04:16.00
I want to choose a row

96
00:04:16.00 --> 00:04:18.00
and we'll do an nth

97
00:04:18.00 --> 00:04:19.01
child

98
00:04:19.01 --> 00:04:22.00
and we'll specify the first element

99
00:04:22.00 --> 00:04:25.00
and then we'll look for
the day-high temperature

100
00:04:25.00 --> 00:04:27.05
and replace the text with hot.

101
00:04:27.05 --> 00:04:30.01
So, now, notice that it's
allowing me to select

102
00:04:30.01 --> 00:04:32.02
the first element, the first row,

103
00:04:32.02 --> 00:04:35.04
and then the high
temperature for that row.

104
00:04:35.04 --> 00:04:38.09
So, if I wanted to, I could
select the second element

105
00:04:38.09 --> 00:04:41.06
and so on and so forth.

106
00:04:41.06 --> 00:04:44.02
Now, do notice that, like Javascript,

107
00:04:44.02 --> 00:04:48.00
the nth-child selector
is not zero-indexed,

108
00:04:48.00 --> 00:04:51.09
so, the first element is
actually element number one.

109
00:04:51.09 --> 00:04:53.02
If you're used to Javascript, you know

110
00:04:53.02 --> 00:04:56.02
that that normally would
be the zeroth element

111
00:04:56.02 --> 00:04:57.09
but that's not going to work.

112
00:04:57.09 --> 00:05:00.01
So, we need to make sure
that we remember that

113
00:05:00.01 --> 00:05:02.07
and you can even use
other selectors like even

114
00:05:02.07 --> 00:05:05.09
to select every other element, here,

115
00:05:05.09 --> 00:05:08.08
and even some of the more
complicated selectors

116
00:05:08.08 --> 00:05:10.07
that you can use with nth-child.

117
00:05:10.07 --> 00:05:13.06
So, really, everything
that you can do in CSS

118
00:05:13.06 --> 00:05:16.03
can be specified as a selector

119
00:05:16.03 --> 00:05:19.08
and you can use either select
to select the first element

120
00:05:19.08 --> 00:05:21.08
if that's the only thing that you need

121
00:05:21.08 --> 00:05:26.04
or use select all to selectAll
the selectors that match.

122
00:05:26.04 --> 00:05:29.08
As I mention, this is not exactly an array

123
00:05:29.08 --> 00:05:32.09
so you can't come in
here and add any sort of

124
00:05:32.09 --> 00:05:36.02
random Javascript commands
once you have elements.

125
00:05:36.02 --> 00:05:39.08
You have to use the
commands that D3 provides.

126
00:05:39.08 --> 00:05:42.03
So, selecting elements is
one of the key principles

127
00:05:42.03 --> 00:05:43.09
of D3.

128
00:05:43.09 --> 00:05:46.09
The two D3 selection
methods combined with CSS'

129
00:05:46.09 --> 00:05:50.06
own nth-child selectors allow
you to have a laser-like

130
00:05:50.06 --> 00:05:54.01
focus to get any element
that you want to act upon.

