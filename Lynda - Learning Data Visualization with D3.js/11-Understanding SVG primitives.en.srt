1
00:00:00.05 --> 00:00:02.09
- [Instructor] Basic shapes
in any graphic language

2
00:00:02.09 --> 00:00:04.05
are called primitives.

3
00:00:04.05 --> 00:00:07.04
So let's take a look at how
we can create simple graphics

4
00:00:07.04 --> 00:00:10.03
using the basics of SVG primitives.

5
00:00:10.03 --> 00:00:14.02
To create an SVG graphics
you can use the SVG tag

6
00:00:14.02 --> 00:00:17.02
which looks just like any other HTML tag

7
00:00:17.02 --> 00:00:18.08
and just like with any other tag

8
00:00:18.08 --> 00:00:21.04
you can assign this tag attributes

9
00:00:21.04 --> 00:00:24.03
and typically for a graphic
you specify the width

10
00:00:24.03 --> 00:00:27.01
and height of the canvas
that you want to use

11
00:00:27.01 --> 00:00:29.05
with the particular graphic.

12
00:00:29.05 --> 00:00:31.03
Just like with any other HTML tag

13
00:00:31.03 --> 00:00:33.05
we can add a style attribute

14
00:00:33.05 --> 00:00:36.07
which will let us control
how this shape looks.

15
00:00:36.07 --> 00:00:39.09
The main primitives for
creating graphics in SVG

16
00:00:39.09 --> 00:00:42.06
are rect, of course, for rectangles,

17
00:00:42.06 --> 00:00:46.00
circle for ovals and circles then line

18
00:00:46.00 --> 00:00:51.05
and text as well as the
polyline which creates polygons.

19
00:00:51.05 --> 00:00:54.03
You can also group elements with the G tag

20
00:00:54.03 --> 00:00:57.05
and even assign any element an ID

21
00:00:57.05 --> 00:00:59.09
so that you refer to it later.

22
00:00:59.09 --> 00:01:02.01
Now one reason to do this is that

23
00:01:02.01 --> 00:01:05.06
if you want to reuse an element with an ID

24
00:01:05.06 --> 00:01:09.01
you can use the xlink:href attribute

25
00:01:09.01 --> 00:01:11.02
which lets you create an element

26
00:01:11.02 --> 00:01:13.08
that is a copy of another element.

27
00:01:13.08 --> 00:01:15.03
Once you create that copy

28
00:01:15.03 --> 00:01:17.09
you can reposition it at a different place

29
00:01:17.09 --> 00:01:20.05
and also change some of its styles

30
00:01:20.05 --> 00:01:23.02
so let's go take a look at how this works.

31
00:01:23.02 --> 00:01:25.08
So here I have a slightly
different HTML file

32
00:01:25.08 --> 00:01:29.01
and our script file is
now completely empty.

33
00:01:29.01 --> 00:01:31.07
We're not going to do scripts right now,

34
00:01:31.07 --> 00:01:35.01
we're going to do everything
inside this index.html.

35
00:01:35.01 --> 00:01:37.06
It's pretty much the
page that you saw before

36
00:01:37.06 --> 00:01:41.04
but it just has the
container completely empty

37
00:01:41.04 --> 00:01:45.06
except for a headline
as well as a placeholder

38
00:01:45.06 --> 00:01:48.05
for us to create our graphics in.

39
00:01:48.05 --> 00:01:51.03
So, right here I can create a graphic

40
00:01:51.03 --> 00:01:55.00
by specifying the SVG tag

41
00:01:55.00 --> 00:01:59.05
and it looks just like
any other tag in HTML

42
00:01:59.05 --> 00:02:04.00
so you close it out with a
closing slash and then an SVG

43
00:02:04.00 --> 00:02:06.08
and then in here you can add attributes.

44
00:02:06.08 --> 00:02:09.04
The attributes are pretty
much what you would find

45
00:02:09.04 --> 00:02:13.05
in other common HTML tags
so you can have a width

46
00:02:13.05 --> 00:02:15.04
just like you would for an image

47
00:02:15.04 --> 00:02:20.01
and then create a width
here as well as a height

48
00:02:20.01 --> 00:02:22.02
and that's pretty common for SVG graphics,

49
00:02:22.02 --> 00:02:26.02
you specify, sort of the
original size of the SVG

50
00:02:26.02 --> 00:02:28.08
and then if we want to
we can use the style tag

51
00:02:28.08 --> 00:02:32.02
and add sort of a background color here

52
00:02:32.02 --> 00:02:37.01
and what we'll do here
is say style background

53
00:02:37.01 --> 00:02:39.06
and then we'll give it a color.

54
00:02:39.06 --> 00:02:41.07
Let's go ahead and put
this in the next line

55
00:02:41.07 --> 00:02:43.07
so we can see it a little bit better.

56
00:02:43.07 --> 00:02:45.07
So you can see that we
created this graphic.

57
00:02:45.07 --> 00:02:51.01
It's 600 by 400 and it
has a background color

58
00:02:51.01 --> 00:02:54.04
of this, sort of, gray so that's how

59
00:02:54.04 --> 00:02:57.01
you create a basic, sort of area,

60
00:02:57.01 --> 00:03:00.06
and other than this you
start adding primitives

61
00:03:00.06 --> 00:03:03.06
that will draw elements within the page

62
00:03:03.06 --> 00:03:07.03
so one of the primitives
is the line primitive

63
00:03:07.03 --> 00:03:12.05
and this is a element that
doesn't require a closing tab

64
00:03:12.05 --> 00:03:16.03
and because we're using XML
when you create an element,

65
00:03:16.03 --> 00:03:18.03
like an HTML, you do an image

66
00:03:18.03 --> 00:03:21.09
and it doesn't have a
closing version of the tag,

67
00:03:21.09 --> 00:03:27.06
you use a slash and this is
sort of a self-closing element.

68
00:03:27.06 --> 00:03:30.03
So, without having this extra slash here,

69
00:03:30.03 --> 00:03:34.09
it would not be a properly
formatted XML document.

70
00:03:34.09 --> 00:03:36.06
So you have to be a
little bit more careful

71
00:03:36.06 --> 00:03:41.01
when you create SVG graphics
and add this extra slash.

72
00:03:41.01 --> 00:03:45.02
So in here we usually enter a position

73
00:03:45.02 --> 00:03:49.01
and for lines we have to
enter two X and Y positions.

74
00:03:49.01 --> 00:03:53.04
The position, sort of, that
starts the line X and Y

75
00:03:53.04 --> 00:03:56.09
and then the X and Y position
that finishes the line.

76
00:03:56.09 --> 00:04:00.09
So you would say x1 equals
and then we could say

77
00:04:00.09 --> 00:04:05.02
we're going to position the
first X at position zero

78
00:04:05.02 --> 00:04:10.02
and then we will do Y one
and then we'll say at 200

79
00:04:10.02 --> 00:04:14.03
and then we'll position
the second X position

80
00:04:14.03 --> 00:04:17.01
or the second point, we'll get x2

81
00:04:17.01 --> 00:04:18.08
and then we'll specify another position,

82
00:04:18.08 --> 00:04:27.06
so we'll say 600 and then
y2 and for this we'll do 200

83
00:04:27.06 --> 00:04:31.03
and if we wanted to style
this line, make it look nice,

84
00:04:31.03 --> 00:04:35.06
we can add a style attribute
and, as I mentioned,

85
00:04:35.06 --> 00:04:39.00
we do need to have that ending slash.

86
00:04:39.00 --> 00:04:46.08
So for style we'll add a stroke
and we'll give that a color

87
00:04:46.08 --> 00:04:50.09
and then we can add another
element or another property here

88
00:04:50.09 --> 00:04:55.05
stroke with and we can say 40px.

89
00:04:55.05 --> 00:04:58.07
So notice that these aren't exactly like

90
00:04:58.07 --> 00:05:02.03
what you would see in HTML

91
00:05:02.03 --> 00:05:06.06
and actually this style
begins in this quote

92
00:05:06.06 --> 00:05:09.05
and this quote should be here at the end

93
00:05:09.05 --> 00:05:11.06
and let's go ahead and save that.

94
00:05:11.06 --> 00:05:14.03
So let's go ahead and modify this,

95
00:05:14.03 --> 00:05:16.01
so we could put the y2 at 300,

96
00:05:16.01 --> 00:05:17.00
you can see that the beginning

97
00:05:17.00 --> 00:05:21.00
and the end of the line
happen at those positions,

98
00:05:21.00 --> 00:05:25.02
so this is just setting
two points x1 and y1

99
00:05:25.02 --> 00:05:30.01
and then x2 and y2 and then
we can modify, sort of,

100
00:05:30.01 --> 00:05:36.03
any properties that we want to right here.

101
00:05:36.03 --> 00:05:38.01
So as you can see, we're using properties

102
00:05:38.01 --> 00:05:41.04
that we're not used to seeing in HTML,

103
00:05:41.04 --> 00:05:45.08
we don't really use
stroke width and stroke

104
00:05:45.08 --> 00:05:48.04
and that is one of the main differences

105
00:05:48.04 --> 00:05:51.02
between HTML and SVG.

106
00:05:51.02 --> 00:05:53.04
SVG has its own properties

107
00:05:53.04 --> 00:05:57.00
but you use the style
attribute or if you want to

108
00:05:57.00 --> 00:05:59.03
you can put this in a separate style sheet

109
00:05:59.03 --> 00:06:02.01
or in an embedded style sheet,
it doesn't really matter,

110
00:06:02.01 --> 00:06:04.06
it works in pretty much the same way.

111
00:06:04.06 --> 00:06:08.00
So let's try another common primitive.

112
00:06:08.00 --> 00:06:11.06
We'll create a rectangle
next, so we'll say rect.

113
00:06:11.06 --> 00:06:14.08
This is another one
with a self-closing tag

114
00:06:14.08 --> 00:06:18.04
and often you'll see that I, kind of,

115
00:06:18.04 --> 00:06:23.00
indent these a little bit just
to make them easier to see.

116
00:06:23.00 --> 00:06:30.04
So we'll say X equals
200, Y equals 100 here

117
00:06:30.04 --> 00:06:35.04
and we'll then add a width value 200

118
00:06:35.04 --> 00:06:42.09
and a height value of 200 and
then for the style attribute,

119
00:06:42.09 --> 00:06:47.07
we will add a fill and
we'll give it a color here.

120
00:06:47.07 --> 00:06:55.03
So we'll use CB4B19, you can really use

121
00:06:55.03 --> 00:06:58.06
any color you want to here,
this is sort of like a red.

122
00:06:58.06 --> 00:07:00.04
And if I save this, you can see

123
00:07:00.04 --> 00:07:03.06
the rectangle appears
right here, pretty cool.

124
00:07:03.06 --> 00:07:07.05
So you can see that these are
fairly easy to figure out.

125
00:07:07.05 --> 00:07:10.01
It's stuff that right
now fits within our box,

126
00:07:10.01 --> 00:07:12.06
so let's just add some more.

127
00:07:12.06 --> 00:07:19.08
Let's try a circle next, so
we'll do circle self-closing tag

128
00:07:19.08 --> 00:07:23.08
and we will do CX because you're defining

129
00:07:23.08 --> 00:07:28.07
the center position of
this element 300 here

130
00:07:28.07 --> 00:07:33.01
and then we'll do CY equals 200

131
00:07:33.01 --> 00:07:42.05
and style is going to
be a fill of 8440043,

132
00:07:42.05 --> 00:07:46.00
let's take a look at that and, actually,

133
00:07:46.00 --> 00:07:48.01
we need to add also another parameter here

134
00:07:48.01 --> 00:07:51.04
called radius, or R, and say that this has

135
00:07:51.04 --> 00:07:56.02
a radius of 50 units so
this will be 50 pixels

136
00:07:56.02 --> 00:07:58.06
and you can see the
circle appear right here,

137
00:07:58.06 --> 00:08:01.05
notice that they're also being
drawn on top of each other,

138
00:08:01.05 --> 00:08:05.03
so they are layered in the
order that they're drawn

139
00:08:05.03 --> 00:08:07.08
and let's try some more.

140
00:08:07.08 --> 00:08:12.05
If we wanted to add some
text you use the text tag

141
00:08:12.05 --> 00:08:18.06
and for that one we'll
do a position as well

142
00:08:18.06 --> 00:08:22.00
and then text has a few other attributes,

143
00:08:22.00 --> 00:08:25.01
so you can say font
family and we'll just use

144
00:08:25.01 --> 00:08:31.08
the default sans serif
font here, font size,

145
00:08:31.08 --> 00:08:37.01
and we'll do a fill of white

146
00:08:37.01 --> 00:08:41.06
and text is actually one that
isn't a self-closing tag,

147
00:08:41.06 --> 00:08:46.05
you actually do have to close
it out and we'll do text

148
00:08:46.05 --> 00:08:48.04
and then what you do is you put the text

149
00:08:48.04 --> 00:08:52.08
that you actually want to show
up in your SVG inside here

150
00:08:52.08 --> 00:09:00.06
so we can say SVG Hello SVG here

151
00:09:00.06 --> 00:09:02.08
and it looks like I forgot a quote

152
00:09:02.08 --> 00:09:04.07
right here on this X position,

153
00:09:04.07 --> 00:09:07.06
oh I need to just have
one quote right here

154
00:09:07.06 --> 00:09:09.07
and then you can see that
the text appears here.

155
00:09:09.07 --> 00:09:13.08
Now this is regular sort
of text that you can select

156
00:09:13.08 --> 00:09:16.08
and do stuff with and
really any of these elements

157
00:09:16.08 --> 00:09:22.05
can be targeted with
java script or css and

158
00:09:22.05 --> 00:09:23.09
pretty easy to, sort of, work with.

159
00:09:23.09 --> 00:09:27.06
Let's try one more, we'll do a triangle.

160
00:09:27.06 --> 00:09:29.04
So for a triangle we're actually going to

161
00:09:29.04 --> 00:09:31.02
do something slightly different.

162
00:09:31.02 --> 00:09:33.02
There isn't a triangle primitive

163
00:09:33.02 --> 00:09:35.07
but there is a polyline primitive

164
00:09:35.07 --> 00:09:39.04
and that will let you
draw triangles pretty well

165
00:09:39.04 --> 00:09:42.05
and one thing that you
can do with elements

166
00:09:42.05 --> 00:09:45.06
that you want to maybe reuse is go ahead

167
00:09:45.06 --> 00:09:48.05
and put them in a group
and give them an ID

168
00:09:48.05 --> 00:09:50.09
so I'm going to give
this an ID of a triangle

169
00:09:50.09 --> 00:09:54.07
and the groups are, sort of,
beginning and ending elements

170
00:09:54.07 --> 00:09:58.08
and then inside here we
can create another element

171
00:09:58.08 --> 00:10:04.04
or really any primitive so
I going to do a polyline

172
00:10:04.04 --> 00:10:08.08
and then this will be
a self-closing tag here

173
00:10:08.08 --> 00:10:13.07
and for polyline we have
to send it some points

174
00:10:13.07 --> 00:10:17.00
that we want to plot in our polyline

175
00:10:17.00 --> 00:10:21.07
and then for here you just
set a series of XY positions

176
00:10:21.07 --> 00:10:25.06
that you want to put points
in, so you can say 10,

177
00:10:25.06 --> 00:10:30.03
let's do 10, 35, and then we'll do a comma

178
00:10:30.03 --> 00:10:32.09
for the next two points, 30, 10,

179
00:10:32.09 --> 00:10:36.09
and there should be a space
in between the coordinates

180
00:10:36.09 --> 00:10:44.05
and then 50, 35, and
that will be our points

181
00:10:44.05 --> 00:10:48.02
and then we'll do a style attributes here

182
00:10:48.02 --> 00:10:49.07
and it will have a fill,

183
00:10:49.07 --> 00:10:52.09
so whenever you want to fill an item

184
00:10:52.09 --> 00:10:56.05
with some color in SVG you use fill.

185
00:10:56.05 --> 00:10:58.06
You may be used to an HTML using something

186
00:10:58.06 --> 00:11:04.03
like background color or
color, in SVG it's fill,

187
00:11:04.03 --> 00:11:07.02
and you can see the triangle
kind of appearing up there,

188
00:11:07.02 --> 00:11:08.07
that's pretty cool.

189
00:11:08.07 --> 00:11:12.04
So another cool thing that
you can do with SVG elements

190
00:11:12.04 --> 00:11:18.09
is refer to another element,
so we could say I want to use

191
00:11:18.09 --> 00:11:24.09
another element and I'll use xlink:href

192
00:11:24.09 --> 00:11:29.05
and since we've given our
group here an id of triangle

193
00:11:29.05 --> 00:11:31.08
we can actually refer to that in here

194
00:11:31.08 --> 00:11:36.08
so we'll say we want the
element with an ID of triangle,

195
00:11:36.08 --> 00:11:39.05
right, and then we'll
specify a different position

196
00:11:39.05 --> 00:11:45.07
for this so position 30 and zero

197
00:11:45.07 --> 00:11:51.03
and let's go ahead and
self-close this use link here,

198
00:11:51.03 --> 00:11:52.02
I'll save it, and you can see

199
00:11:52.02 --> 00:11:53.08
that there's another triangle now,

200
00:11:53.08 --> 00:11:57.00
we didn't have to, sort of, do
this entire group over again.

201
00:11:57.00 --> 00:11:59.05
We're just referring to that same element

202
00:11:59.05 --> 00:12:03.02
and then positioning it
at a different place.

203
00:12:03.02 --> 00:12:05.06
So that's a great way of saving yourself

204
00:12:05.06 --> 00:12:07.04
some drawing commands.

205
00:12:07.04 --> 00:12:10.03
If you know HTML, drawing things with SVG

206
00:12:10.03 --> 00:12:12.00
is really not a big deal.

207
00:12:12.00 --> 00:12:15.01
The main thing to learn is the right tags,

208
00:12:15.01 --> 00:12:17.07
attributes, and special styles.

209
00:12:17.07 --> 00:12:21.03
You can find out more
about SVG at this URL.

