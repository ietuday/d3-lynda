1
00:00:00.03 --> 00:00:01.01
- [Instructor] Although learning

2
00:00:01.01 --> 00:00:03.01
to select content is important,

3
00:00:03.01 --> 00:00:05.03
it's what you do with that content

4
00:00:05.03 --> 00:00:07.08
or that selection that makes an impact.

5
00:00:07.08 --> 00:00:09.08
So, let's take a look
at some of the methods

6
00:00:09.08 --> 00:00:12.07
that you can use to act upon elements.

7
00:00:12.07 --> 00:00:14.08
We've already seen the text element,

8
00:00:14.08 --> 00:00:19.00
which is the simplest way to
change the text of a selection.

9
00:00:19.00 --> 00:00:21.09
But it doesn't really
allow you to use HTML,

10
00:00:21.09 --> 00:00:23.06
so if you need to do that,

11
00:00:23.06 --> 00:00:25.08
you'll need to use the HTML method,

12
00:00:25.08 --> 00:00:29.02
which lets you use more complex HTML tags.

13
00:00:29.02 --> 00:00:32.08
The append method allows
you to add another element

14
00:00:32.08 --> 00:00:35.07
to the last child of
the current selection.

15
00:00:35.07 --> 00:00:39.01
And the insert method is
a little bit more precise.

16
00:00:39.01 --> 00:00:41.01
It allows you to add an element

17
00:00:41.01 --> 00:00:45.00
to a more specific position
within the selection.

18
00:00:45.00 --> 00:00:47.02
Finally, of course, since you can append

19
00:00:47.02 --> 00:00:50.06
and insert elements, you
can also remove them.

20
00:00:50.06 --> 00:00:52.08
So, let's take a look at how this works.

21
00:00:52.08 --> 00:00:54.04
One of the conventions in the three

22
00:00:54.04 --> 00:00:59.01
is that you add any additional
commands in a separate line.

23
00:00:59.01 --> 00:01:01.03
So, normally, you would see this text,

24
00:01:01.03 --> 00:01:04.02
sort of, in the next line
and then also indented

25
00:01:04.02 --> 00:01:06.00
under this main method.

26
00:01:06.00 --> 00:01:08.05
It just keeps things a
little bit more organized.

27
00:01:08.05 --> 00:01:11.07
Now, notice that if I
wanted to add the word hot,

28
00:01:11.07 --> 00:01:14.02
let's go ahead and modify this

29
00:01:14.02 --> 00:01:16.01
so that it just chooses the first element.

30
00:01:16.01 --> 00:01:18.08
So, we'll do an nth-child(1).

31
00:01:18.08 --> 00:01:22.00
That allows you to choose
just this first element.

32
00:01:22.00 --> 00:01:25.06
And then let's try adding
in some HTML in here

33
00:01:25.06 --> 00:01:29.04
so you can see that
it's not going to work.

34
00:01:29.04 --> 00:01:32.00
So it's literally
inserting our strong tags

35
00:01:32.00 --> 00:01:35.01
and displaying that, and
obviously, that's not what we want.

36
00:01:35.01 --> 00:01:36.07
If we do want to add HTML,

37
00:01:36.07 --> 00:01:39.07
we should be using the
HTML method right here.

38
00:01:39.07 --> 00:01:43.03
And notice that now this
is a bold piece of text

39
00:01:43.03 --> 00:01:46.05
instead of just the actual tag.

40
00:01:46.05 --> 00:01:48.06
Now let's say that we wanted to add a span

41
00:01:48.06 --> 00:01:51.00
at the end of this first item.

42
00:01:51.00 --> 00:01:52.00
We could do it like this.

43
00:01:52.00 --> 00:01:54.07
We'll just send an append command,

44
00:01:54.07 --> 00:01:59.08
and right here, we're
going to ask for a span.

45
00:01:59.08 --> 00:02:01.03
We can type in whatever we want here,

46
00:02:01.03 --> 00:02:04.03
so it could be a span or
a dib or anything else.

47
00:02:04.03 --> 00:02:08.00
And then we'll add in another HTML.

48
00:02:08.00 --> 00:02:09.08
It really could just be text.

49
00:02:09.08 --> 00:02:12.03
And then we'll just say today here.

50
00:02:12.03 --> 00:02:14.04
We do need to put these in quotes.

51
00:02:14.04 --> 00:02:16.07
So once I do that, you
can see that it appends it

52
00:02:16.07 --> 00:02:19.04
as a child of the last element.

53
00:02:19.04 --> 00:02:24.02
Now, because we didn't
put in a space here,

54
00:02:24.02 --> 00:02:25.09
it's right next to the current element.

55
00:02:25.09 --> 00:02:30.02
So we actually want to add
an additional space there.

56
00:02:30.02 --> 00:02:32.09
And that works pretty well
if you want to add something

57
00:02:32.09 --> 00:02:35.05
to the child of the selection,
so the important thing

58
00:02:35.05 --> 00:02:38.02
in the three is to understand
that the first thing you do

59
00:02:38.02 --> 00:02:39.07
is make a selection.

60
00:02:39.07 --> 00:02:41.04
And then you use these methods to control

61
00:02:41.04 --> 00:02:43.08
what happens in that selection.

62
00:02:43.08 --> 00:02:46.02
In the case of append,
it's adding something

63
00:02:46.02 --> 00:02:49.02
to the child of the
currently selected element.

64
00:02:49.02 --> 00:02:51.03
If we wanted to add something, say,

65
00:02:51.03 --> 00:02:53.03
to the beginning of an element,

66
00:02:53.03 --> 00:02:55.05
we would have to use the insert command.

67
00:02:55.05 --> 00:02:58.00
So we would say insert.

68
00:02:58.00 --> 00:03:02.01
And then we put in the element
that we want to insert on,

69
00:03:02.01 --> 00:03:04.07
and then we can also add a position

70
00:03:04.07 --> 00:03:07.00
of where we want to insert this item.

71
00:03:07.00 --> 00:03:10.04
So if I don't specify anything other than

72
00:03:10.04 --> 00:03:12.09
an insert command here,
it's just going to work

73
00:03:12.09 --> 00:03:14.05
like an append command.

74
00:03:14.05 --> 00:03:19.03
But I can also add something
like first-child here,

75
00:03:19.03 --> 00:03:22.06
and I do need to put in the colon.

76
00:03:22.06 --> 00:03:25.07
And now today is going to
appear at the beginning

77
00:03:25.07 --> 00:03:28.02
of this element, so
obviously, we would need

78
00:03:28.02 --> 00:03:33.02
to move the space right here afterwards.

79
00:03:33.02 --> 00:03:35.07
So sometimes you use double
quotations like this,

80
00:03:35.07 --> 00:03:38.08
and sometimes you can use
the single quotations.

81
00:03:38.08 --> 00:03:42.08
The reason why you may
want to do that is because,

82
00:03:42.08 --> 00:03:45.05
say that you wanted to
add a class right here.

83
00:03:45.05 --> 00:03:48.03
Well, if you're using
the double quotation,

84
00:03:48.03 --> 00:03:50.06
this would cause a problem,

85
00:03:50.06 --> 00:03:53.05
and when D3 encounters a problem,

86
00:03:53.05 --> 00:03:56.09
it's essentially going to
leave your items alone.

87
00:03:56.09 --> 00:03:58.09
So here, if we wanted to add a class,

88
00:03:58.09 --> 00:04:01.06
it would be better to
use single quotations.

89
00:04:01.06 --> 00:04:04.04
It just doesn't really
matter, and it sort of depends

90
00:04:04.04 --> 00:04:06.02
on what you're trying to do.

91
00:04:06.02 --> 00:04:07.03
Notice that now it works,

92
00:04:07.03 --> 00:04:09.07
and we could put in
another class here of item

93
00:04:09.07 --> 00:04:11.06
or whatever else we wanted.

94
00:04:11.06 --> 00:04:13.09
So, you can use either or.

95
00:04:13.09 --> 00:04:15.03
I'm going to undo those.

96
00:04:15.03 --> 00:04:17.05
Just make sure that you
use the single quotes

97
00:04:17.05 --> 00:04:19.03
when you want to actually insert

98
00:04:19.03 --> 00:04:22.00
regular HTML quotes in here.

99
00:04:22.00 --> 00:04:25.01
Now instead of first-child,
we can also include

100
00:04:25.01 --> 00:04:29.07
a specific element, so we
could do, say, strong here.

101
00:04:29.07 --> 00:04:32.02
And this would work in
exactly the same way

102
00:04:32.02 --> 00:04:36.03
because now, we've got a
strong tag inside this element,

103
00:04:36.03 --> 00:04:37.04
so we're just saying.

104
00:04:37.04 --> 00:04:40.09
Go ahead and put a span
before this strong tag,

105
00:04:40.09 --> 00:04:44.00
and insert an element right there.

106
00:04:44.00 --> 00:04:45.04
And this element would be a span.

107
00:04:45.04 --> 00:04:49.06
And then I want to enter
the HTML in there of today.

108
00:04:49.06 --> 00:04:51.09
Which, again, this could be text.

109
00:04:51.09 --> 00:04:54.05
And of course, since you
can append and insert,

110
00:04:54.05 --> 00:04:56.01
it means that you can also remove,

111
00:04:56.01 --> 00:04:58.08
and that makes a lot of sense,
so I'm just going to use

112
00:04:58.08 --> 00:05:01.02
another D3 selection here.

113
00:05:01.02 --> 00:05:03.08
And I'll use selectAll.

114
00:05:03.08 --> 00:05:07.04
And then I will type in a tr,

115
00:05:07.04 --> 00:05:09.00
which is a table row.

116
00:05:09.00 --> 00:05:13.03
And nth-child, let's
see, we'll do five here.

117
00:05:13.03 --> 00:05:15.07
And then we could just,
once we have that selection,

118
00:05:15.07 --> 00:05:17.08
then we can say remove.

119
00:05:17.08 --> 00:05:20.05
And what that will do is
remove the last element.

120
00:05:20.05 --> 00:05:24.02
Remember, nth-child is not zero indexed,

121
00:05:24.02 --> 00:05:26.07
so it would remove the fifth one.

122
00:05:26.07 --> 00:05:28.06
We can also remove any of the other ones.

123
00:05:28.06 --> 00:05:30.09
So if we remove the third one,

124
00:05:30.09 --> 00:05:34.06
notice that we get rid
of April 2nd right here.

125
00:05:34.06 --> 00:05:37.04
And so this is a really handy one.

126
00:05:37.04 --> 00:05:39.00
You can remove an element.

127
00:05:39.00 --> 00:05:41.03
The key thing here to remember is that,

128
00:05:41.03 --> 00:05:42.05
first you make a selection,

129
00:05:42.05 --> 00:05:45.00
and then you can use any of these methods

130
00:05:45.00 --> 00:05:49.02
to control how D3 is
managing that selection.

131
00:05:49.02 --> 00:05:52.05
And you can insert,
append, remove elements.

132
00:05:52.05 --> 00:05:55.04
There's actually a few
other D3 selection methods.

133
00:05:55.04 --> 00:05:58.08
You can take a look at those at this URL.

