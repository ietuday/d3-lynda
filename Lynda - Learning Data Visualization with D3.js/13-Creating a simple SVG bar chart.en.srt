1
00:00:00.06 --> 00:00:02.06
- [Instructor] Alright so
it's time to put everything

2
00:00:02.06 --> 00:00:06.02
we've learned into practice
by creating a simple

3
00:00:06.02 --> 00:00:09.02
bar chart that uses data as well.

4
00:00:09.02 --> 00:00:11.06
So as you can see my index at HTML

5
00:00:11.06 --> 00:00:15.08
is back to just being
pretty simple with a div

6
00:00:15.08 --> 00:00:17.09
that has an ID of viz.

7
00:00:17.09 --> 00:00:20.03
Right now in everything we do we'll

8
00:00:20.03 --> 00:00:22.01
be inserted into this div.

9
00:00:22.01 --> 00:00:24.07
My script file is completely empty.

10
00:00:24.07 --> 00:00:26.07
So I'm going to start off by creating

11
00:00:26.07 --> 00:00:29.06
a variable called bar data.

12
00:00:29.06 --> 00:00:32.02
And then setting that
to an array with some

13
00:00:32.02 --> 00:00:35.05
simple values here.

14
00:00:35.05 --> 00:00:37.09
Now let's go ahead and create another set

15
00:00:37.09 --> 00:00:41.07
of variables that we can
insert into our graphic.

16
00:00:41.07 --> 00:00:44.01
So we'll create a
variable I'll call height,

17
00:00:44.01 --> 00:00:47.03
and that's going to have
the height of the SVG

18
00:00:47.03 --> 00:00:54.07
graphic and some other variables as well.

19
00:00:54.07 --> 00:00:57.06
So the bar width variable
will be the width

20
00:00:57.06 --> 00:01:00.09
of our bars and the offset is how much

21
00:01:00.09 --> 00:01:03.02
space in between.

22
00:01:03.02 --> 00:01:05.05
So first we need to start off by using

23
00:01:05.05 --> 00:01:08.01
d3 to select our element.

24
00:01:08.01 --> 00:01:10.09
So since there's only
one element to select,

25
00:01:10.09 --> 00:01:14.03
we will target it like this using

26
00:01:14.03 --> 00:01:18.04
the ID of viz and
immediately we'll go ahead

27
00:01:18.04 --> 00:01:21.07
and append a SVG graphic.

28
00:01:21.07 --> 00:01:25.04
And then inside that we
will create some attributes.

29
00:01:25.04 --> 00:01:31.02
So attribute width, I'll
set that to the width

30
00:01:31.02 --> 00:01:35.00
that we generate it
above in that variable.

31
00:01:35.00 --> 00:01:36.08
Do the same thing with the height,

32
00:01:36.08 --> 00:01:39.02
just set it to the height variable,

33
00:01:39.02 --> 00:01:47.03
and then add the style that
sets the background color.

34
00:01:47.03 --> 00:01:50.03
So that creates our graphic
with the proper background,

35
00:01:50.03 --> 00:01:53.02
just like we did before.

36
00:01:53.02 --> 00:01:55.09
And now it's time to start using our data.

37
00:01:55.09 --> 00:02:00.04
So what we're going to do
here is do a select all.

38
00:02:00.04 --> 00:02:05.00
So we're going to use the
enter with the data attribute.

39
00:02:05.00 --> 00:02:07.04
So first we have to select the elements

40
00:02:07.04 --> 00:02:09.02
that we're going to be creating,

41
00:02:09.02 --> 00:02:11.06
they're going to be a
series of rectangles.

42
00:02:11.06 --> 00:02:16.00
And then we have to
call the data attribute

43
00:02:16.00 --> 00:02:20.00
with our bar data and
then we'll use the enter

44
00:02:20.00 --> 00:02:24.06
command and append the
rectangles that we're calling.

45
00:02:24.06 --> 00:02:29.08
So remember we are selecting
rectangles before they exist

46
00:02:29.08 --> 00:02:33.05
using this data that we
are generating up here.

47
00:02:33.05 --> 00:02:36.00
And then with the enter command,

48
00:02:36.00 --> 00:02:39.06
we're going to append
all of the rectangles

49
00:02:39.06 --> 00:02:40.04
that we're doin' here.

50
00:02:40.04 --> 00:02:44.02
So a series of bars will be
just a series of rectangles

51
00:02:44.02 --> 00:02:46.09
that we're going to be
generating with our data.

52
00:02:46.09 --> 00:02:50.07
And so in here we can
actually start styling

53
00:02:50.07 --> 00:02:52.06
these individual bars.

54
00:02:52.06 --> 00:02:58.08
So I'll say fill.

55
00:02:58.08 --> 00:03:02.01
Alright so now we need to set an attribute

56
00:03:02.01 --> 00:03:06.02
for the width and this is going to be set

57
00:03:06.02 --> 00:03:08.06
to the bar default the width which we

58
00:03:08.06 --> 00:03:09.09
call bar width here.

59
00:03:09.09 --> 00:03:12.05
So they're all going to be the same width.

60
00:03:12.05 --> 00:03:15.01
And then here's where it
gets sort of interesting.

61
00:03:15.01 --> 00:03:17.05
We'll set up another attribute,

62
00:03:17.05 --> 00:03:21.02
and for the height of each
one of the different elements,

63
00:03:21.02 --> 00:03:25.03
this is going to use a
call back with our data,

64
00:03:25.03 --> 00:03:28.09
and just return that data.

65
00:03:28.09 --> 00:03:31.03
So if we save this you'll
see that we actually

66
00:03:31.03 --> 00:03:32.06
have all of the bars,

67
00:03:32.06 --> 00:03:34.02
but they're all the same color,

68
00:03:34.02 --> 00:03:36.03
and they're sitting right
on top of eachother.

69
00:03:36.03 --> 00:03:39.01
So that's why it looks like a single bar.

70
00:03:39.01 --> 00:03:41.07
That means that we need to also set

71
00:03:41.07 --> 00:03:48.01
something for the X position.

72
00:03:48.01 --> 00:03:52.01
And for this one we
will also need the index

73
00:03:52.01 --> 00:03:56.02
and we're going to return the index

74
00:03:56.02 --> 00:04:02.05
times our width plus the bar offset.

75
00:04:02.05 --> 00:04:04.09
And when you save that
you can see that they all

76
00:04:04.09 --> 00:04:07.01
kind of appear next to each other.

77
00:04:07.01 --> 00:04:09.09
They have the offset in between,

78
00:04:09.09 --> 00:04:12.04
and they're all that default width.

79
00:04:12.04 --> 00:04:14.05
So we're getting something now.

80
00:04:14.05 --> 00:04:16.09
If you do want to align
them to the bottom,

81
00:04:16.09 --> 00:04:19.06
we can add another attribute here

82
00:04:19.06 --> 00:04:23.00
for the Y position.

83
00:04:23.00 --> 00:04:26.08
Again a call back,

84
00:04:26.08 --> 00:04:31.05
and return the height minus the data

85
00:04:31.05 --> 00:04:35.01
so that now it aligns to the bottom.

86
00:04:35.01 --> 00:04:37.05
So the reason that it's by default

87
00:04:37.05 --> 00:04:40.00
going to align to the top is,

88
00:04:40.00 --> 00:04:42.01
the weird Y coordinate system

89
00:04:42.01 --> 00:04:44.00
that happens with computers.

90
00:04:44.00 --> 00:04:45.07
And we're used to seeing
a coordinate system

91
00:04:45.07 --> 00:04:49.04
that starts with 00
position on the bottom left,

92
00:04:49.04 --> 00:04:51.04
but computers have a coordinate system

93
00:04:51.04 --> 00:04:54.09
that has a 00 on the top left,

94
00:04:54.09 --> 00:04:58.02
and that's why we have to reverse things.

95
00:04:58.02 --> 00:05:01.05
So as you can see this
is a pretty simple way

96
00:05:01.05 --> 00:05:04.01
of creating a graphic by using everything

97
00:05:04.01 --> 00:05:06.06
that we've learned so far.

98
00:05:06.06 --> 00:05:11.02
Combining our use of data
as well as selections,

99
00:05:11.02 --> 00:05:13.01
attributes and style,

100
00:05:13.01 --> 00:05:16.08
to create a very simple looking bar chart.

101
00:05:16.08 --> 00:05:20.02
We're going to improve on
this in the next chapter.

