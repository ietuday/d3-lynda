1
00:00:00.06 --> 00:00:02.02
- [Narrator] Although
D3 can handle and work

2
00:00:02.02 --> 00:00:05.01
with either HTML or SVG graphics,

3
00:00:05.01 --> 00:00:06.04
when you're working with D3,

4
00:00:06.04 --> 00:00:10.04
you'll often want to
use SVG instead of HTML.

5
00:00:10.04 --> 00:00:13.04
SVG stands for Scalable Vector Graphics,

6
00:00:13.04 --> 00:00:17.01
and it definitely has some
advantages over regular HTML.

7
00:00:17.01 --> 00:00:18.04
So let's take a look.

8
00:00:18.04 --> 00:00:22.01
Now, HTML is really good at
letting us draw simple shapes,

9
00:00:22.01 --> 00:00:24.06
like rectangles and maybe circles,

10
00:00:24.06 --> 00:00:27.05
but not so good at any
other type of shape.

11
00:00:27.05 --> 00:00:30.02
The main reason we pick
SVG is because you can draw

12
00:00:30.02 --> 00:00:34.06
complex curves and shapes
with an HTML-like language.

13
00:00:34.06 --> 00:00:38.08
Now, SVG is an XML format just like HTML,

14
00:00:38.08 --> 00:00:40.09
so it looks a lot like HTML

15
00:00:40.09 --> 00:00:44.08
and it uses beginning and
ending tags, plus attributes,

16
00:00:44.08 --> 00:00:47.01
so it's easy to read and write.

17
00:00:47.01 --> 00:00:49.05
If you're familiar with writing HTML,

18
00:00:49.05 --> 00:00:53.01
then SVG's going to be
really easy to pick up.

19
00:00:53.01 --> 00:00:55.07
Another really awesome thing about SVG is

20
00:00:55.07 --> 00:01:00.01
that you can access DOM element
in SVG just like you would

21
00:01:00.01 --> 00:01:03.09
with CSS, and you can
apply CSS-like styles.

22
00:01:03.09 --> 00:01:07.06
Now these aren't exactly like
the CSS styles that you know,

23
00:01:07.06 --> 00:01:09.08
there's a few differences, but they work

24
00:01:09.08 --> 00:01:12.06
in exactly the same way,
and as a matter of fact,

25
00:01:12.06 --> 00:01:15.01
if you already have an
existing style sheet,

26
00:01:15.01 --> 00:01:19.04
you can add SVG commands into
your regular style sheet.

27
00:01:19.04 --> 00:01:22.04
You can create complex
SVG graphics with programs

28
00:01:22.04 --> 00:01:25.02
like Adobe Illustrator
and then export them

29
00:01:25.02 --> 00:01:28.00
into your regular HTML pages.

30
00:01:28.00 --> 00:01:30.09
Now the interesting thing
about SVG graphics is

31
00:01:30.09 --> 00:01:33.05
that they are also scriptable,

32
00:01:33.05 --> 00:01:35.00
which means that you can program them

33
00:01:35.00 --> 00:01:37.02
to do things with JavaScript.

34
00:01:37.02 --> 00:01:38.07
That means that you can make portions

35
00:01:38.07 --> 00:01:41.07
of your SVG graphics respond

36
00:01:41.07 --> 00:01:44.07
to things like clicks or drags.

37
00:01:44.07 --> 00:01:47.09
You can attach events, animate
them, and do pretty much

38
00:01:47.09 --> 00:01:52.05
whatever you can do with
JavaScript within an SVG graphic.

39
00:01:52.05 --> 00:01:55.07
So learning how to work with
SVG graphics in D3 is going

40
00:01:55.07 --> 00:01:58.09
to give you all the tools you
need to not just display them,

41
00:01:58.09 --> 00:02:01.09
but also make them fully interactive.

