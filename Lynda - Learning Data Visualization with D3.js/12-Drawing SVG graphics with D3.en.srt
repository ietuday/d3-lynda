1
00:00:00.05 --> 00:00:01.05
- [Instructor] In the last video,

2
00:00:01.05 --> 00:00:04.05
we created an SVG graphic manually.

3
00:00:04.05 --> 00:00:06.06
In this movie, I'm going
to show you how to use

4
00:00:06.06 --> 00:00:09.07
the SVG commands we've
been learning to create

5
00:00:09.07 --> 00:00:12.09
some of these primitives using D3.

6
00:00:12.09 --> 00:00:14.06
So the way that you do
this is pretty simple,

7
00:00:14.06 --> 00:00:18.03
you create an element and
then you target that element

8
00:00:18.03 --> 00:00:21.05
by using D3's selection methods.

9
00:00:21.05 --> 00:00:23.08
Once we have that element,
you can use methods

10
00:00:23.08 --> 00:00:28.07
like append or insert to
add items into that element.

11
00:00:28.07 --> 00:00:32.06
So you add an SVG element
and then inside that,

12
00:00:32.06 --> 00:00:35.08
you can add additional
elements and then use

13
00:00:35.08 --> 00:00:40.04
the attribute methods or
the style methods in D3

14
00:00:40.04 --> 00:00:42.08
to control how those elements look.

15
00:00:42.08 --> 00:00:45.05
So let's go ahead and move over here

16
00:00:45.05 --> 00:00:47.06
and I'll show you that
right now our index,

17
00:00:47.06 --> 00:00:49.07
that html file looks
a little bit different

18
00:00:49.07 --> 00:00:52.06
than we had in the last video.

19
00:00:52.06 --> 00:00:54.01
We have removed the SVG graphic

20
00:00:54.01 --> 00:00:56.02
and right now I've created this div

21
00:00:56.02 --> 00:00:59.07
with an id of viz that we're going to use

22
00:00:59.07 --> 00:01:02.04
to place our graphics inside.

23
00:01:02.04 --> 00:01:05.05
So if we go back into the
script that you'll notice

24
00:01:05.05 --> 00:01:07.05
that is completely empty.

25
00:01:07.05 --> 00:01:11.08
And what we need to do
is use the D3 command

26
00:01:11.08 --> 00:01:15.05
so this will be the
select command to target

27
00:01:15.05 --> 00:01:20.00
the graphic that we have
and we gave this graphic

28
00:01:20.00 --> 00:01:22.02
an id of viz.

29
00:01:22.02 --> 00:01:25.02
And then you can just use
sort of regular D3 commands

30
00:01:25.02 --> 00:01:27.04
that you're probably already used to.

31
00:01:27.04 --> 00:01:33.00
So we'll use and append
command to add an SVG tag

32
00:01:33.00 --> 00:01:38.03
and then inside that, we
can add an attribute, right?

33
00:01:38.03 --> 00:01:40.00
So we need to add some attributes in order

34
00:01:40.00 --> 00:01:44.01
to create an SVG and we'll
use the width attributes,

35
00:01:44.01 --> 00:01:49.06
set it to 600 and we'll
need to also set the height.

36
00:01:49.06 --> 00:01:51.09
And we'll set that to 400.

37
00:01:51.09 --> 00:01:55.07
And then we can use the style attribute

38
00:01:55.07 --> 00:01:59.09
to set the background color,
just like we did before

39
00:01:59.09 --> 00:02:04.02
but now we can do the same
thing with these D3 commands.

40
00:02:04.02 --> 00:02:09.03
And then we'll do 93A1A1.

41
00:02:09.03 --> 00:02:10.08
And let's go ahead and save this.

42
00:02:10.08 --> 00:02:13.06
You can see that we have the SVG now.

43
00:02:13.06 --> 00:02:18.04
And we've created this box
by using these parameters.

44
00:02:18.04 --> 00:02:21.03
So once we have that, then we can insert

45
00:02:21.03 --> 00:02:23.05
elements inside the SVG.

46
00:02:23.05 --> 00:02:27.08
So we can say append,
and then inside this SVG

47
00:02:27.08 --> 00:02:30.02
we can add a rectangle.

48
00:02:30.02 --> 00:02:33.09
And then in here we can add an attribute.

49
00:02:33.09 --> 00:02:39.09
And just like we did before,
we will add an X position.

50
00:02:39.09 --> 00:02:44.06
And then we'll add a Y position here.

51
00:02:44.06 --> 00:02:50.05
And rectangles need height

52
00:02:50.05 --> 00:02:53.09
as well as width.

53
00:02:53.09 --> 00:03:00.02
And then we'll use style
to fill that rectangle in.

54
00:03:00.02 --> 00:03:01.08
And let's go ahead and
save that and you can see

55
00:03:01.08 --> 00:03:04.04
that the rectangle appears right here, so.

56
00:03:04.04 --> 00:03:09.00
I do have this window sort of
a bit smaller than the width

57
00:03:09.00 --> 00:03:12.05
of my primitive here, or my SVG graphic.

58
00:03:12.05 --> 00:03:16.00
So that's why you only see
sort of half of it right now.

59
00:03:16.00 --> 00:03:20.09
So if you want to add
another item to the SVG now,

60
00:03:20.09 --> 00:03:24.00
well you can't really do
an append command because

61
00:03:24.00 --> 00:03:29.04
this is going to append this
element inside the rectangle

62
00:03:29.04 --> 00:03:31.00
and that wouldn't show.

63
00:03:31.00 --> 00:03:32.08
So what we actually have to do is just

64
00:03:32.08 --> 00:03:35.07
do a different selection.

65
00:03:35.07 --> 00:03:40.09
So we'll just do D3, select,
and then we'll once again

66
00:03:40.09 --> 00:03:44.08
target the visualization, so V-I-Z.

67
00:03:44.08 --> 00:03:49.00
And we'll also target the SVG inside it.

68
00:03:49.00 --> 00:03:51.02
So sometimes you have to do this.

69
00:03:51.02 --> 00:03:55.07
And then we can append a circle

70
00:03:55.07 --> 00:04:04.08
with a center of 300, and 200.

71
00:04:04.08 --> 00:04:13.05
And a radius of 50 here, and
we'll give it a style as well.

72
00:04:13.05 --> 00:04:16.01
And now you can see the circle inside it.

73
00:04:16.01 --> 00:04:19.01
So sometimes you do have
to sort of go outside

74
00:04:19.01 --> 00:04:22.02
the selection so you can't
just append everything

75
00:04:22.02 --> 00:04:24.09
because every time you
append, you sort of go

76
00:04:24.09 --> 00:04:29.04
inside the element that you are in.

77
00:04:29.04 --> 00:04:31.07
So any other graphic we
put in here will just be

78
00:04:31.07 --> 00:04:34.07
a repeat of all of these
different commands.

79
00:04:34.07 --> 00:04:38.00
D3 does get more powerful
when you can combine data

80
00:04:38.00 --> 00:04:39.03
to create graphics.

81
00:04:39.03 --> 00:04:41.08
We'll take a look at
that in the next video.

