1
00:00:00.07 --> 00:00:02.01
- [Instructor] To make
our graphic look better,

2
00:00:02.01 --> 00:00:04.06
we'll have to take a look
at how to add margins.

3
00:00:04.06 --> 00:00:07.00
Now you could try to
figure this out by yourself

4
00:00:07.00 --> 00:00:08.09
but there's actually a convention

5
00:00:08.09 --> 00:00:11.09
that people use when
working with d3 graphics

6
00:00:11.09 --> 00:00:13.07
and it's pretty cool.

7
00:00:13.07 --> 00:00:16.01
You can take a look at it at this URL

8
00:00:16.01 --> 00:00:18.09
and essentially what you
need to do is translate

9
00:00:18.09 --> 00:00:22.02
the chart that you want to sort of display

10
00:00:22.02 --> 00:00:25.07
and then add an object
that has the margins

11
00:00:25.07 --> 00:00:28.04
that you want to set on each side.

12
00:00:28.04 --> 00:00:30.03
So let's take a look at how this works

13
00:00:30.03 --> 00:00:32.03
in our project.

14
00:00:32.03 --> 00:00:34.09
First, I'm going to right
underneath temperatures,

15
00:00:34.09 --> 00:00:37.03
add an object that is going to hold

16
00:00:37.03 --> 00:00:39.05
the values for our margins.

17
00:00:39.05 --> 00:00:41.08
So we'll call this margin
and it's going to be

18
00:00:41.08 --> 00:00:47.08
an object and we'll use
top, we'll set that to zero,

19
00:00:47.08 --> 00:00:51.00
right, we'll set that to 30.

20
00:00:51.00 --> 00:00:52.05
Actually, let's go
ahead and clear them out

21
00:00:52.05 --> 00:00:54.05
and have them all just
be zero for right now.

22
00:00:54.05 --> 00:00:59.09
Bottom, zero, and left, zero.

23
00:00:59.09 --> 00:01:04.00
And then we'll also need
to modify this height

24
00:01:04.00 --> 00:01:07.01
so that it now takes
into account our margins.

25
00:01:07.01 --> 00:01:14.05
So we'll say the height
minus margin top minus margin

26
00:01:14.05 --> 00:01:18.02
and bottom, actually
should be period here.

27
00:01:18.02 --> 00:01:20.05
And so what we're doing
is we're saying hey,

28
00:01:20.05 --> 00:01:22.04
I'm going to set my height here

29
00:01:22.04 --> 00:01:25.07
and then I'm going to take the margin top

30
00:01:25.07 --> 00:01:29.01
and margin bottom and
subtract it from this number.

31
00:01:29.01 --> 00:01:33.01
And do the same thing for the width

32
00:01:33.01 --> 00:01:38.06
and that should be instead
of margin top, of course,

33
00:01:38.06 --> 00:01:43.07
margin left and margin right.

34
00:01:43.07 --> 00:01:47.00
So once we do that, we
need to add these margins

35
00:01:47.00 --> 00:01:49.03
where the chart is going to be drawn

36
00:01:49.03 --> 00:01:53.07
so we're going to scroll
down and find the place here,

37
00:01:53.07 --> 00:01:56.03
my chart where the chart is getting drawn

38
00:01:56.03 --> 00:01:59.09
and we also need to add those right here.

39
00:01:59.09 --> 00:02:03.03
So it's going to be the same thing

40
00:02:03.03 --> 00:02:06.07
and instead of minus,
they're going to be pluses.

41
00:02:06.07 --> 00:02:10.07
We're going to add them into this section

42
00:02:10.07 --> 00:02:14.03
and actually these should be right here

43
00:02:14.03 --> 00:02:22.08
and we'll put some here
that will be left and right.

44
00:02:22.08 --> 00:02:25.03
One last thing we'll
need to do is translate

45
00:02:25.03 --> 00:02:29.02
or move the object by our margins.

46
00:02:29.02 --> 00:02:31.07
So right after we append the group,

47
00:02:31.07 --> 00:02:35.04
we're going to add a transform attribute

48
00:02:35.04 --> 00:02:43.03
and that's going to be translate

49
00:02:43.03 --> 00:02:50.00
and we'll use margin left,

50
00:02:50.00 --> 00:02:53.03
and margin right.

51
00:02:53.03 --> 00:02:55.01
Right there, let's go
ahead and space these out.

52
00:02:55.01 --> 00:02:58.00
Let's go ahead and put
this on the next line

53
00:02:58.00 --> 00:03:00.02
just to make it easier to read.

54
00:03:00.02 --> 00:03:02.03
And of course, actually this needs to be

55
00:03:02.03 --> 00:03:07.02
periods right here.

56
00:03:07.02 --> 00:03:12.05
And we need plus signs in here.

57
00:03:12.05 --> 00:03:16.01
Alright, so this is all the setup we need

58
00:03:16.01 --> 00:03:19.01
and now all we need to do
whenever we want to add

59
00:03:19.01 --> 00:03:23.01
a margin is to modify in
this object right here.

60
00:03:23.01 --> 00:03:27.07
So if we say add 30 pixels to the left,

61
00:03:27.07 --> 00:03:29.09
you can see that the margins shift

62
00:03:29.09 --> 00:03:32.09
or the bars shift over by 30 pixels.

63
00:03:32.09 --> 00:03:36.06
So we actually probably need something

64
00:03:36.06 --> 00:03:38.09
closer to 20 pixels.

65
00:03:38.09 --> 00:03:41.00
You sort of have to play around with these

66
00:03:41.00 --> 00:03:43.01
and now this looks really good.

67
00:03:43.01 --> 00:03:44.06
So we're going to need some more margins

68
00:03:44.06 --> 00:03:48.01
at the bottom for that zero to show up,

69
00:03:48.01 --> 00:03:54.08
and for that we'll do
a bottom margin of 30

70
00:03:54.08 --> 00:03:57.05
and that should be enough
for the zero to show up

71
00:03:57.05 --> 00:03:59.09
and also for anything we put down here

72
00:03:59.09 --> 00:04:01.07
to show up too.

73
00:04:01.07 --> 00:04:04.01
So this convention actually
makes it super easy

74
00:04:04.01 --> 00:04:08.04
to add guides and go
ahead and control them

75
00:04:08.04 --> 00:04:10.07
in a single place and also have one place

76
00:04:10.07 --> 00:04:13.08
where we put in our width and height.

77
00:04:13.08 --> 00:04:18.04
Everything is nicely placed
at the top of our code

78
00:04:18.04 --> 00:04:21.01
and we don't have to worry ever again

79
00:04:21.01 --> 00:04:25.01
about adjusting any of
the sizes anywhere else

80
00:04:25.01 --> 00:04:26.04
in our project.

