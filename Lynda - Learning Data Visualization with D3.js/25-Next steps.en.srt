1
00:00:00.05 --> 00:00:01.05
- [Ray] Hey there, it's Ray again.

2
00:00:01.05 --> 00:00:03.01
Thanks for watching this course.

3
00:00:03.01 --> 00:00:04.09
D3 is extremely deep,

4
00:00:04.09 --> 00:00:07.08
and this is just an introduction
to the main components.

5
00:00:07.08 --> 00:00:09.01
So if you want to learn more,

6
00:00:09.01 --> 00:00:12.00
make sure you look at the D3 website.

7
00:00:12.00 --> 00:00:14.03
There's a couple of
really cool links in here,

8
00:00:14.03 --> 00:00:17.05
including this link for examples,

9
00:00:17.05 --> 00:00:20.08
where you can find a
gallery of D3 examples.

10
00:00:20.08 --> 00:00:24.08
And a lot of these will
have code right next to them

11
00:00:24.08 --> 00:00:26.07
with explanations on how to do

12
00:00:26.07 --> 00:00:30.00
this particular type of chart.

13
00:00:30.00 --> 00:00:31.01
You also want to make sure that you

14
00:00:31.01 --> 00:00:33.00
visit this documentation link,

15
00:00:33.00 --> 00:00:38.05
where you can find a deeper
description of D3's API.

16
00:00:38.05 --> 00:00:41.09
Another great place to go
is Mike Bostock's Blocks

17
00:00:41.09 --> 00:00:44.09
which has a lot of additional examples

18
00:00:44.09 --> 00:00:48.00
with code on how to do
these examples as well.

19
00:00:48.00 --> 00:00:51.01
You should also check out the
GitHub repo for this project.

20
00:00:51.01 --> 00:00:54.07
It has the latest version
of the code for this course.

21
00:00:54.07 --> 00:00:56.02
Now if you want to learn more about me,

22
00:00:56.02 --> 00:01:00.01
you can reach me at my
personal website, Raybo.org,

23
00:01:00.01 --> 00:01:03.00
or through Twitter, GitHub, LinkedIn,

24
00:01:03.00 --> 00:01:05.07
or just about any other
social media network

25
00:01:05.07 --> 00:01:07.05
@planetoftheweb.

26
00:01:07.05 --> 00:01:09.06
Once again, thanks for
watching this course,

27
00:01:09.06 --> 00:01:11.05
and have fun learning.

