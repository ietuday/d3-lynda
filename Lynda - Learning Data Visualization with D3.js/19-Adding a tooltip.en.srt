1
00:00:00.01 --> 00:00:01.04
- [Instructor] Most of the time,

2
00:00:01.04 --> 00:00:04.03
you're going to be
working within D3 objects

3
00:00:04.03 --> 00:00:07.00
to create and manage SVG elements.

4
00:00:07.00 --> 00:00:08.09
But sometimes, you'll
need to work directly

5
00:00:08.09 --> 00:00:10.06
with your page's dom.

6
00:00:10.06 --> 00:00:12.05
Let's use the document object model

7
00:00:12.05 --> 00:00:16.05
to add a tooltip that will
add values to our data

8
00:00:16.05 --> 00:00:19.00
on top of our bar chart elements.

9
00:00:19.00 --> 00:00:21.04
So what we need to do is

10
00:00:21.04 --> 00:00:23.01
create another variable.

11
00:00:23.01 --> 00:00:26.04
We can do this before this
chart variable right here.

12
00:00:26.04 --> 00:00:29.09
And this one is going
to be called tooltip.

13
00:00:29.09 --> 00:00:33.05
And what we'll do here is use D3 to select

14
00:00:33.05 --> 00:00:36.08
the body tag of the current page.

15
00:00:36.08 --> 00:00:39.06
And then, we're going to append.

16
00:00:39.06 --> 00:00:44.00
So let's go ahead and do
that here, append a div.

17
00:00:44.00 --> 00:00:45.09
This will create a new div.

18
00:00:45.09 --> 00:00:50.03
And then in this div, we'll
modify a few attributes.

19
00:00:50.03 --> 00:00:54.04
So we'll set the position to absolute.

20
00:00:54.04 --> 00:00:57.01
So actually, let's do style.

21
00:00:57.01 --> 00:00:59.00
And we're going to create an element

22
00:00:59.00 --> 00:01:02.09
that has a little bit of
padding around our text.

23
00:01:02.09 --> 00:01:06.06
So this will get a padding of
zero at the top and bottom,

24
00:01:06.06 --> 00:01:09.02
and 10 pixels on each side.

25
00:01:09.02 --> 00:01:13.00
So this is just like adding
any sort of HTML element

26
00:01:13.00 --> 00:01:15.06
with Javascript, except that
you're using the D3 classes

27
00:01:15.06 --> 00:01:18.03
just to make everything
a little bit easier.

28
00:01:18.03 --> 00:01:23.05
And we'll also give
this thing a background.

29
00:01:23.05 --> 00:01:26.03
And we'll make that white.

30
00:01:26.03 --> 00:01:29.03
Finally, we'll go ahead and

31
00:01:29.03 --> 00:01:31.04
set the opacity

32
00:01:31.04 --> 00:01:33.02
to zero to begin with.

33
00:01:33.02 --> 00:01:37.04
So we've got this element, and
we want to add it to the dom.

34
00:01:37.04 --> 00:01:39.02
So

35
00:01:39.02 --> 00:01:42.02
we're going to go down
to our rollover states.

36
00:01:42.02 --> 00:01:45.08
Remember, right here our
mouseover is where we handle

37
00:01:45.08 --> 00:01:51.00
what happens when we mouseover
one of the individual bars.

38
00:01:51.00 --> 00:01:53.06
And what we'll do here is

39
00:01:53.06 --> 00:01:56.05
we will target the tooltip

40
00:01:56.05 --> 00:01:59.09
HTML and pass along the data.

41
00:01:59.09 --> 00:02:03.09
And then in here, we can add
a couple of style attributes

42
00:02:03.09 --> 00:02:06.01
that position this element

43
00:02:06.01 --> 00:02:08.04
around the D3 element itself.

44
00:02:08.04 --> 00:02:09.05
So,

45
00:02:09.05 --> 00:02:11.02
every D3 event

46
00:02:11.02 --> 00:02:14.08
has a method or a property called Event.

47
00:02:14.08 --> 00:02:17.08
And in that event, it
has information about

48
00:02:17.08 --> 00:02:20.02
what's happening with the current element.

49
00:02:20.02 --> 00:02:22.09
So each one of these mouseover events

50
00:02:22.09 --> 00:02:24.07
are going to create a target

51
00:02:24.07 --> 00:02:28.04
and also have information about
the position of the mouse,

52
00:02:28.04 --> 00:02:30.01
as well as the page.

53
00:02:30.01 --> 00:02:31.08
So we can target those, just like we would

54
00:02:31.08 --> 00:02:35.06
any other regular Javascript
event information.

55
00:02:35.06 --> 00:02:39.09
So we'll get the page X
position of the element,

56
00:02:39.09 --> 00:02:43.09
and then we'll just subtract
35 pixels from that.

57
00:02:43.09 --> 00:02:45.08
And then we will add...

58
00:02:45.08 --> 00:02:47.07
We have to add the word PX

59
00:02:47.07 --> 00:02:49.06
because we're setting a CSS style,

60
00:02:49.06 --> 00:02:53.09
so left requires you to use
a sort of pixel property.

61
00:02:53.09 --> 00:02:56.04
Let's go ahead and make this smaller here

62
00:02:56.04 --> 00:02:58.05
and then we'll make this a little wider

63
00:02:58.05 --> 00:03:00.06
so we have a little bit
more breathing room.

64
00:03:00.06 --> 00:03:02.06
And I'm going to copy this one,

65
00:03:02.06 --> 00:03:05.03
and I'll do style top,

66
00:03:05.03 --> 00:03:07.03
D3 event,

67
00:03:07.03 --> 00:03:09.05
page Y,

68
00:03:09.05 --> 00:03:12.02
minus 30 this time,

69
00:03:12.02 --> 00:03:14.04
and PX.

70
00:03:14.04 --> 00:03:15.09
All right, so that's almost done.

71
00:03:15.09 --> 00:03:20.01
I also need to manage the
transition of this element.

72
00:03:20.01 --> 00:03:25.02
So I'm going to add right
here tooltip transition,

73
00:03:25.02 --> 00:03:29.08
and I'll add a duration
of 200 milliseconds,

74
00:03:29.08 --> 00:03:33.02
and change the opacity

75
00:03:33.02 --> 00:03:37.06
to .9.

76
00:03:37.06 --> 00:03:40.03
And whenever we roll
over each of these items,

77
00:03:40.03 --> 00:03:42.09
you can now see there's
a little bit of a tooltip

78
00:03:42.09 --> 00:03:46.06
with the data for that bar chart element.

79
00:03:46.06 --> 00:03:50.03
So we really just combine
the different ways

80
00:03:50.03 --> 00:03:52.05
of working with our elements.

81
00:03:52.05 --> 00:03:56.07
You can combine HTML and
SVG elements together.

82
00:03:56.07 --> 00:03:58.01
The only interesting thing here

83
00:03:58.01 --> 00:04:01.00
that's brand new is the fact that

84
00:04:01.00 --> 00:04:03.08
every event has an event

85
00:04:03.08 --> 00:04:06.06
sort of method or property
that you can access

86
00:04:06.06 --> 00:04:10.01
to gather additional data about your page.

