1
00:00:00.03 --> 00:00:01.06
- [Ray] Hey there, this is Ray Villalobos,

2
00:00:01.06 --> 00:00:04.01
senior staff author, and in this course,

3
00:00:04.01 --> 00:00:06.06
I'm going to introduce you
to the basic components

4
00:00:06.06 --> 00:00:08.02
of the D3 framework.

5
00:00:08.02 --> 00:00:11.07
We'll do this by building
a chart step-by-step.

6
00:00:11.07 --> 00:00:14.05
We'll start by understanding what D3 is

7
00:00:14.05 --> 00:00:16.06
and learn how to work with selecting

8
00:00:16.06 --> 00:00:20.06
and manipulating regular
HTML DOM elements.

9
00:00:20.06 --> 00:00:23.06
Then, we'll do a quick
introduction to SVG,

10
00:00:23.06 --> 00:00:25.00
so that you can understand

11
00:00:25.00 --> 00:00:29.05
how D3 creates elements using
scalable vector graphics.

12
00:00:29.05 --> 00:00:31.07
Once we go through that,
we'll break into the basics

13
00:00:31.07 --> 00:00:35.07
of scales which D3 uses to
create streams of information.

14
00:00:35.07 --> 00:00:39.07
There are many types of scales
in D3 and we'll cover linear,

15
00:00:39.07 --> 00:00:43.02
ordinal, as well as time-base scales.

16
00:00:43.02 --> 00:00:44.09
We'll also learn how to add events

17
00:00:44.09 --> 00:00:48.01
so that our scales can
respond to regular DOM events

18
00:00:48.01 --> 00:00:50.09
and how to animate our SVG graphics

19
00:00:50.09 --> 00:00:52.06
for a more dramatic effect.

20
00:00:52.06 --> 00:00:54.03
Finally, I'll show you how easy it is

21
00:00:54.03 --> 00:00:56.04
to import data with D3.

22
00:00:56.04 --> 00:00:58.00
We'll create a bar chart

23
00:00:58.00 --> 00:01:00.09
that tracks temperatures
over a period of time.

24
00:01:00.09 --> 00:01:03.00
There's a lot to learn,
but that's why you're here,

25
00:01:03.00 --> 00:01:04.05
so let's get started.

