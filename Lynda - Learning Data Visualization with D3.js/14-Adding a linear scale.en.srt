1
00:00:00.06 --> 00:00:02.01
- [Instructor] So now we have a bar chart,

2
00:00:02.01 --> 00:00:04.08
but it doesn't look that
great and that's because

3
00:00:04.08 --> 00:00:08.09
our data doesn't really fit
our space for our graphics.

4
00:00:08.09 --> 00:00:13.03
So it would be nice to make
these fit the space vertically,

5
00:00:13.03 --> 00:00:16.07
regardless of their actual values.

6
00:00:16.07 --> 00:00:18.09
In order to do that, we're
going to need to learn

7
00:00:18.09 --> 00:00:20.05
about linear scales.

8
00:00:20.05 --> 00:00:23.05
A scale is a method that
allows us to redefine data

9
00:00:23.05 --> 00:00:26.03
so that it fits a certain range of values.

10
00:00:26.03 --> 00:00:29.08
The simplest type of scale
is called the Linear Scale.

11
00:00:29.08 --> 00:00:32.01
It has a method called scaleLinear.

12
00:00:32.01 --> 00:00:34.08
It's also known as a continuous
scale because it maps

13
00:00:34.08 --> 00:00:38.06
a serial set of input
values to output values.

14
00:00:38.06 --> 00:00:41.05
We can use that in our
table to make our data grow

15
00:00:41.05 --> 00:00:43.08
to fit a specific range.

16
00:00:43.08 --> 00:00:45.01
Now it needs two things:

17
00:00:45.01 --> 00:00:48.01
First is the domain, which you can use by

18
00:00:48.01 --> 00:00:50.00
using the domain method.

19
00:00:50.00 --> 00:00:51.09
And that will be an array of values

20
00:00:51.09 --> 00:00:53.05
that you feed to your scale.

21
00:00:53.05 --> 00:00:55.03
Now it defines the original values that

22
00:00:55.03 --> 00:00:56.05
you're going to modify.

23
00:00:56.05 --> 00:00:59.06
So in this case, this will
be pretty much our data.

24
00:00:59.06 --> 00:01:02.09
The range is another set
of values that you feed

25
00:01:02.09 --> 00:01:05.07
in an array to the linear scale.

26
00:01:05.07 --> 00:01:08.07
This represents the values that we want to

27
00:01:08.07 --> 00:01:11.02
scale our data to.

28
00:01:11.02 --> 00:01:13.09
We can also use some
statistical methods to parse

29
00:01:13.09 --> 00:01:16.08
our domain for information that we need.

30
00:01:16.08 --> 00:01:18.09
In this case, we're going
to need to figure out

31
00:01:18.09 --> 00:01:22.03
which is the maximum value in our data

32
00:01:22.03 --> 00:01:24.08
so that we can take all the other values

33
00:01:24.08 --> 00:01:28.01
and scale everything according
to that maximum value.

34
00:01:28.01 --> 00:01:30.04
There are plenty of other
statistical functions

35
00:01:30.04 --> 00:01:33.00
you can find at this URL.

36
00:01:33.00 --> 00:01:35.05
So let's go back into our chart

37
00:01:35.05 --> 00:01:38.00
and start modifying this so that it works.

38
00:01:38.00 --> 00:01:41.07
So first of all, I'm going
to add another variable

39
00:01:41.07 --> 00:01:42.08
right here.

40
00:01:42.08 --> 00:01:44.06
And I'm going to call this Y scale.

41
00:01:44.06 --> 00:01:48.06
And this is going to be
essentially a new map

42
00:01:48.06 --> 00:01:51.06
of information, so it's
going to be a new array

43
00:01:51.06 --> 00:01:54.01
that is going to be mapped depending on

44
00:01:54.01 --> 00:01:56.04
the height of our graphic.

45
00:01:56.04 --> 00:02:02.09
So for this, we're going to
use the D3 scaleLinear method.

46
00:02:02.09 --> 00:02:06.03
And then we will specify the domain.

47
00:02:06.03 --> 00:02:09.01
And the domain is going
to be an array of values

48
00:02:09.01 --> 00:02:13.04
that's going to start
from zero to our data,

49
00:02:13.04 --> 00:02:20.09
so we'll say D3 max and we're
going to use bar data here.

50
00:02:20.09 --> 00:02:23.03
So what we want to do is set up our domain

51
00:02:23.03 --> 00:02:28.00
to be from zero to the
maximum value available

52
00:02:28.00 --> 00:02:30.07
in our data, which right now would be 45,

53
00:02:30.07 --> 00:02:34.01
which would be this item right here.

54
00:02:34.01 --> 00:02:36.06
So once we have that, we
can specify that we want

55
00:02:36.06 --> 00:02:39.09
to map this to a different range.

56
00:02:39.09 --> 00:02:42.06
This will also be an array of values.

57
00:02:42.06 --> 00:02:44.06
It'll also start from
zero, because these are all

58
00:02:44.06 --> 00:02:46.03
going to start from the same place.

59
00:02:46.03 --> 00:02:49.05
And then we will map this to the height.

60
00:02:49.05 --> 00:02:53.01
So the height right now
is defined as 400 pixels.

61
00:02:53.01 --> 00:02:56.03
And this means that these
values will be rearranged

62
00:02:56.03 --> 00:03:00.06
to fit according to the
height of our container,

63
00:03:00.06 --> 00:03:03.02
which is our SVG.

64
00:03:03.02 --> 00:03:05.05
So once we have that,
we're going to need to

65
00:03:05.05 --> 00:03:08.00
redefine a couple of our methods.

66
00:03:08.00 --> 00:03:09.09
So right now we're just sort of outputting

67
00:03:09.09 --> 00:03:14.00
the height of our bar
chart based on the height

68
00:03:14.00 --> 00:03:17.00
of what's available in our array.

69
00:03:17.00 --> 00:03:21.05
But now we need to start
using our Y scale calculation.

70
00:03:21.05 --> 00:03:24.01
So we're going to modify
this height function

71
00:03:24.01 --> 00:03:25.06
so that it returns.

72
00:03:25.06 --> 00:03:29.01
Y scale.

73
00:03:29.01 --> 00:03:34.08
And then the data, according
to this Y scale method.

74
00:03:34.08 --> 00:03:38.03
And then for our Y position,
we're also going to

75
00:03:38.03 --> 00:03:42.07
have to use the Y scale
variable and pass along

76
00:03:42.07 --> 00:03:45.00
the data for this as well.

77
00:03:45.00 --> 00:03:48.01
So now you can see that by
using this linear scale,

78
00:03:48.01 --> 00:03:51.04
we are able to remap our original values

79
00:03:51.04 --> 00:03:53.04
to the height of our graphics.

80
00:03:53.04 --> 00:03:56.03
So now it doesn't really
matter what we put in here,

81
00:03:56.03 --> 00:03:59.03
so let's try to change
some of these values here.

82
00:03:59.03 --> 00:04:02.02
Then let's make a really big value here.

83
00:04:02.02 --> 00:04:03.06
And you'll notice that when I save,

84
00:04:03.06 --> 00:04:07.02
everything maps to the
highest value right here

85
00:04:07.02 --> 00:04:09.07
and it's always going to fit our data

86
00:04:09.07 --> 00:04:12.01
to our SVG graphics height.

87
00:04:12.01 --> 00:04:14.06
And that's really cool
because it makes our graphic

88
00:04:14.06 --> 00:04:16.01
look a lot better.

