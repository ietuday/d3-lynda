1
00:00:00.06 --> 00:00:02.05
- [Instructor] In addition
to appending, inserting,

2
00:00:02.05 --> 00:00:04.06
and removing html elements,

3
00:00:04.06 --> 00:00:09.03
you can control any attributes
within your selections.

4
00:00:09.03 --> 00:00:12.08
You can use this to style
your selection with CSS,

5
00:00:12.08 --> 00:00:15.00
and changing styles is
so important that D3

6
00:00:15.00 --> 00:00:18.08
provides a few special methods
to take care of just that.

7
00:00:18.08 --> 00:00:21.02
So let's take a look at what's available.

8
00:00:21.02 --> 00:00:23.04
Now first of all if
you just want to change

9
00:00:23.04 --> 00:00:27.00
certain style attributes you
can use the style method.

10
00:00:27.00 --> 00:00:30.03
It gives you access to any CSS
style and it's the same thing

11
00:00:30.03 --> 00:00:33.09
as using the style attribute in HTML.

12
00:00:33.09 --> 00:00:35.06
There's another method called classed

13
00:00:35.06 --> 00:00:38.06
and it allows you to
toggle classes on and off

14
00:00:38.06 --> 00:00:41.05
if an element already
has a number of classes

15
00:00:41.05 --> 00:00:44.08
you can ask for D3 to
change just one of them

16
00:00:44.08 --> 00:00:48.04
and toggle it either on or off.

17
00:00:48.04 --> 00:00:52.00
Attribute, or attr, let's
you access any attribute

18
00:00:52.00 --> 00:00:53.09
not just classes or styles

19
00:00:53.09 --> 00:00:55.08
so it's a little bit more flexible

20
00:00:55.08 --> 00:00:58.02
and there is a special
method called property

21
00:00:58.02 --> 00:01:02.06
that allows you in the same
way to access any property.

22
00:01:02.06 --> 00:01:04.03
Most of the time it's going to work

23
00:01:04.03 --> 00:01:06.00
just like attribute but there may be

24
00:01:06.00 --> 00:01:09.09
some instances in which you
can only access properties

25
00:01:09.09 --> 00:01:11.08
through this property method

26
00:01:11.08 --> 00:01:13.09
instead of the attribute method.

27
00:01:13.09 --> 00:01:15.07
So let's take a look at how they work.

28
00:01:15.07 --> 00:01:18.01
So if you're coming
from the previous video

29
00:01:18.01 --> 00:01:19.04
you should have something like this,

30
00:01:19.04 --> 00:01:22.07
I'm going to go ahead
and delete most of this

31
00:01:22.07 --> 00:01:23.05
and I'll save it.

32
00:01:23.05 --> 00:01:25.04
And make sure that you refresh this page.

33
00:01:25.04 --> 00:01:28.02
You should see five items over here.

34
00:01:28.02 --> 00:01:32.01
And then I'm going to
target just the day-high,

35
00:01:32.01 --> 00:01:34.07
I'm not going to go in to
adjust the temperature,

36
00:01:34.07 --> 00:01:37.04
which would be just this 90 right here.

37
00:01:37.04 --> 00:01:44.00
And once I do that I'm going
to append span tag here

38
00:01:44.00 --> 00:01:47.04
and then I need to put
the dot at the beginning,

39
00:01:47.04 --> 00:01:51.08
and after that then I'm
going to type in some HTML

40
00:01:51.08 --> 00:01:53.08
and just say hot.

41
00:01:53.08 --> 00:01:56.05
So now I'm accessing the first one,

42
00:01:56.05 --> 00:01:59.06
so let's go ahead and
do the fourth one here.

43
00:01:59.06 --> 00:02:01.01
So that will be this one right here.

44
00:02:01.01 --> 00:02:04.06
And now I've added the text
hot at the end of that one.

45
00:02:04.06 --> 00:02:07.00
So if I want to I can
use the style attribute

46
00:02:07.00 --> 00:02:09.09
to go ahead and make that
look a little bit different

47
00:02:09.09 --> 00:02:15.02
so I can say style background
and then comma red,

48
00:02:15.02 --> 00:02:16.08
and now the background is red

49
00:02:16.08 --> 00:02:21.01
and I can keep on going style padding

50
00:02:21.01 --> 00:02:22.04
3px

51
00:02:22.04 --> 00:02:25.01
so now it gives it a
little bit more padding,

52
00:02:25.01 --> 00:02:28.01
I can do margin-left,

53
00:02:28.01 --> 00:02:30.04
let's go ahead and add another 3 pixels

54
00:02:30.04 --> 00:02:33.01
just to give it a little
bit more room here

55
00:02:33.01 --> 00:02:34.08
right in between these two elements,

56
00:02:34.08 --> 00:02:35.06
and we just keep on going

57
00:02:35.06 --> 00:02:38.07
and add as many other
styles as we wanted to.

58
00:02:38.07 --> 00:02:43.06
So style and then we'll do border-radius

59
00:02:43.06 --> 00:02:45.09
and we'll use 5px here,

60
00:02:45.09 --> 00:02:48.00
so now it has a nice border radius,

61
00:02:48.00 --> 00:02:51.07
go ahead and make that three.

62
00:02:51.07 --> 00:02:58.05
And let's go ahead and
make the color white.

63
00:02:58.05 --> 00:03:00.04
So that looks like a nice label,

64
00:03:00.04 --> 00:03:04.00
but there's actually an
easier way of doing this.

65
00:03:04.00 --> 00:03:05.08
We're using that with strap framework

66
00:03:05.08 --> 00:03:08.08
and there is a special class called label

67
00:03:08.08 --> 00:03:11.03
and label danger which
would give us pretty much

68
00:03:11.03 --> 00:03:14.01
the same thing as what
we've got right here.

69
00:03:14.01 --> 00:03:16.07
Now one nice thing
about D3 is that you can

70
00:03:16.07 --> 00:03:18.00
just sort of grab stuff

71
00:03:18.00 --> 00:03:20.02
that you don't need to work with right now

72
00:03:20.02 --> 00:03:22.05
and sort of just comment it out,

73
00:03:22.05 --> 00:03:26.03
and sometimes your platforms
have a shortcut for that.

74
00:03:26.03 --> 00:03:29.05
I'm on a mac and I'm
using Visual Studio Code

75
00:03:29.05 --> 00:03:33.06
so that's Cmd-/ and if we wanted to

76
00:03:33.06 --> 00:03:35.06
I think it's under one of the menus.

77
00:03:35.06 --> 00:03:39.03
So you can say toggle, line comment,

78
00:03:39.03 --> 00:03:42.02
or do this toggle block
comment right here.

79
00:03:42.02 --> 00:03:45.05
I'm sure your editor probably
has something like that.

80
00:03:45.05 --> 00:03:48.00
And then we could sort
of just keep on going.

81
00:03:48.00 --> 00:03:50.04
Kind of leave what we're doing intact.

82
00:03:50.04 --> 00:03:54.00
So if I want to, I can say

83
00:03:54.00 --> 00:03:58.06
use the classed method and
then just turn on use label

84
00:03:58.06 --> 00:04:02.05
and I'll use label-danger
because in Bootstrap

85
00:04:02.05 --> 00:04:05.03
you have to have both of
these or this doesn't work.

86
00:04:05.03 --> 00:04:07.01
And we'll make those true.

87
00:04:07.01 --> 00:04:11.01
So notice that it made the label
sort of the Bootstrap style

88
00:04:11.01 --> 00:04:12.09
and we didn't have to do all this.

89
00:04:12.09 --> 00:04:16.00
You could do label-warning,

90
00:04:16.00 --> 00:04:18.03
and I think another one is primary,

91
00:04:18.03 --> 00:04:20.03
so let's go ahead and leave it danger

92
00:04:20.03 --> 00:04:21.03
because I think that looks pretty good.

93
00:04:21.03 --> 00:04:22.02
Now we've got a problem

94
00:04:22.02 --> 00:04:25.00
that we don't have the extra space here.

95
00:04:25.00 --> 00:04:29.01
We could add a margin to
the left if we wanted to,

96
00:04:29.01 --> 00:04:33.00
let's go ahead and do that,

97
00:04:33.00 --> 00:04:35.04
and that gets us that
little bit of extra space.

98
00:04:35.04 --> 00:04:37.04
Now if wanted to we could actually modify

99
00:04:37.04 --> 00:04:38.08
this specific element.

100
00:04:38.08 --> 00:04:40.07
This is what's cool about D3,

101
00:04:40.07 --> 00:04:43.06
that you have sort of chaining

102
00:04:43.06 --> 00:04:45.05
of all the things that you want to do,

103
00:04:45.05 --> 00:04:48.05
and that sort of structural
works really well.

104
00:04:48.05 --> 00:04:52.02
So in order to turn off my label danger

105
00:04:52.02 --> 00:04:54.02
I would have to do classed,

106
00:04:54.02 --> 00:04:58.04
and I'm just going to target label danger,

107
00:04:58.04 --> 00:05:01.07
and I'm going to make that false,

108
00:05:01.07 --> 00:05:03.05
and that's actually going
to turn everything off

109
00:05:03.05 --> 00:05:06.05
because in Bootstrap you
can't just use the label class

110
00:05:06.05 --> 00:05:08.04
you have to use both of these together.

111
00:05:08.04 --> 00:05:11.04
So then,

112
00:05:11.04 --> 00:05:13.09
we'll change this one to label-warning,

113
00:05:13.09 --> 00:05:15.03
and we'll make it true

114
00:05:15.03 --> 00:05:18.06
so because we've turned off label danger

115
00:05:18.06 --> 00:05:23.06
and now we are adding label
warning by turning it on to true

116
00:05:23.06 --> 00:05:27.00
then we can see this work properly.

117
00:05:27.00 --> 00:05:29.04
Now let me show you just
another way of doing this.

118
00:05:29.04 --> 00:05:32.00
I'm going to do another D3 selection here.

119
00:05:32.00 --> 00:05:36.04
And I'm going to select the h2 span

120
00:05:36.04 --> 00:05:39.06
so if you remember from
the HTML this right here

121
00:05:39.06 --> 00:05:42.05
is my h2, and inside this h2

122
00:05:42.05 --> 00:05:45.03
I have a span with the class of small.

123
00:05:45.03 --> 00:05:47.09
So if we look at the index.html

124
00:05:47.09 --> 00:05:51.05
there it is, weather forecast
span with a class of small.

125
00:05:51.05 --> 00:05:53.07
So we can actually just use this

126
00:05:53.07 --> 00:05:56.09
classed and look for small

127
00:05:56.09 --> 00:05:59.00
and turn that to false,

128
00:05:59.00 --> 00:06:01.09
and now this text has actually gotten rid

129
00:06:01.09 --> 00:06:04.07
of the small class that makes it small

130
00:06:04.07 --> 00:06:06.06
so that's sort of
another thing you can do.

131
00:06:06.06 --> 00:06:10.06
Target any element and
then modify this item

132
00:06:10.06 --> 00:06:13.05
within that element, which
is this class right here.

133
00:06:13.05 --> 00:06:16.03
So let's try something a little cooler.

134
00:06:16.03 --> 00:06:20.04
We will use D3 to add
a series of check boxes

135
00:06:20.04 --> 00:06:23.08
at the beginning of all
these table items here.

136
00:06:23.08 --> 00:06:26.02
So we'll do d3

137
00:06:26.02 --> 00:06:28.03
selectAll

138
00:06:28.03 --> 00:06:29.08
tr,

139
00:06:29.08 --> 00:06:32.04
so we'll use all the table rows,

140
00:06:32.04 --> 00:06:36.00
and then we'll use the insert method

141
00:06:36.00 --> 00:06:38.02
to add a td

142
00:06:38.02 --> 00:06:42.04
and we'll need to add a td
sort of at the beginning

143
00:06:42.04 --> 00:06:46.02
so we'll use first-child right.

144
00:06:46.02 --> 00:06:49.02
So if we use the pen remember
it adds things to the end

145
00:06:49.02 --> 00:06:52.03
and in this case we want this
td to be at the beginning

146
00:06:52.03 --> 00:06:54.07
of each one of these rows.

147
00:06:54.07 --> 00:06:57.02
Since we're grabbing each
one of the rows right here

148
00:06:57.02 --> 00:06:58.06
we'll need to do that.

149
00:06:58.06 --> 00:07:03.04
And then we'll append an input field

150
00:07:03.04 --> 00:07:04.09
so let's go ahead and say that by default

151
00:07:04.09 --> 00:07:09.04
and put fields show up as
these sort of text boxes.

152
00:07:09.04 --> 00:07:11.01
And we don't have any attributes right now

153
00:07:11.01 --> 00:07:12.01
in the input field

154
00:07:12.01 --> 00:07:14.03
so it's not particularly useful

155
00:07:14.03 --> 00:07:19.03
so we could say attr and then use a type

156
00:07:19.03 --> 00:07:21.03
of checkbox

157
00:07:21.03 --> 00:07:23.08
and this has to be in quotes too.

158
00:07:23.08 --> 00:07:25.06
Let's save it and now we've added

159
00:07:25.06 --> 00:07:27.03
a bunch of check boxes right here.

160
00:07:27.03 --> 00:07:29.01
So we could like click on each one

161
00:07:29.01 --> 00:07:30.01
and maybe this one could be used

162
00:07:30.01 --> 00:07:32.00
to sort of select all of them.

163
00:07:32.00 --> 00:07:34.02
And if you wanted to select all of them,

164
00:07:34.02 --> 00:07:38.04
then you could also
turn on a property here

165
00:07:38.04 --> 00:07:40.08
and we'll use the checked property

166
00:07:40.08 --> 00:07:43.01
and turn that to true.

167
00:07:43.01 --> 00:07:46.00
So that would make all
of these check boxes

168
00:07:46.00 --> 00:07:47.08
automatically clicked on.

169
00:07:47.08 --> 00:07:50.01
So you can use a combination
of these methods and

170
00:07:50.01 --> 00:07:53.01
D3 really needs to be experienced

171
00:07:53.01 --> 00:07:55.08
so maybe play around with some
of these different methods

172
00:07:55.08 --> 00:07:59.05
and see what else you
can target and turn on.

173
00:07:59.05 --> 00:08:03.01
D3 provides lots of methods for
working with HTML properties

174
00:08:03.01 --> 00:08:06.07
and attributes as well as a few handy ways

175
00:08:06.07 --> 00:08:08.08
of managing your styles.

