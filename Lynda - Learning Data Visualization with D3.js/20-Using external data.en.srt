1
00:00:00.07 --> 00:00:02.03
- [Instructor] In a real
project you would want

2
00:00:02.03 --> 00:00:04.08
to load your data from an external file

3
00:00:04.08 --> 00:00:07.06
or database and D3 has
some excellent functions

4
00:00:07.06 --> 00:00:09.03
to help you with just that.

5
00:00:09.03 --> 00:00:11.04
So D3 offers a variety of methods

6
00:00:11.04 --> 00:00:14.00
to deal with data import
and the nice thing

7
00:00:14.00 --> 00:00:16.06
is that it will take
care of parsing the data

8
00:00:16.06 --> 00:00:18.00
automatically for you.

9
00:00:18.00 --> 00:00:21.05
So essentially these
functions are AJAX calls,

10
00:00:21.05 --> 00:00:23.06
you can get data from many formats

11
00:00:23.06 --> 00:00:26.01
including the popular JSON format

12
00:00:26.01 --> 00:00:28.07
which you'll be using in this video

13
00:00:28.07 --> 00:00:30.05
and once you import the information

14
00:00:30.05 --> 00:00:34.03
it will be easily accessible
as a Java Script object.

15
00:00:34.03 --> 00:00:37.01
Other formats that are pretty common

16
00:00:37.01 --> 00:00:40.03
are CSV which you can import from Excel

17
00:00:40.03 --> 00:00:44.02
and also TSV which are
tab separated values

18
00:00:44.02 --> 00:00:47.01
and those are easy to copy and paste

19
00:00:47.01 --> 00:00:50.05
from an Excel table or other tables.

20
00:00:50.05 --> 00:00:52.08
Once you import the data
you can use that data

21
00:00:52.08 --> 00:00:55.08
in a call back to create your chart.

22
00:00:55.08 --> 00:00:58.03
I've already got a pretty
complicated data file

23
00:00:58.03 --> 00:01:00.08
in this data folder right here.

24
00:01:00.08 --> 00:01:03.07
It's called forcast.json,
it's a file that I've gotten

25
00:01:03.07 --> 00:01:07.04
from a website called openweathermap.org.

26
00:01:07.04 --> 00:01:10.06
Now JSON data can be pretty
complicated to understand,

27
00:01:10.06 --> 00:01:13.02
especially when it's in
a text file like this.

28
00:01:13.02 --> 00:01:15.05
So I like to use another website called

29
00:01:15.05 --> 00:01:17.03
the JSON Editor Online.

30
00:01:17.03 --> 00:01:19.09
And I'm going to copy all this data

31
00:01:19.09 --> 00:01:21.09
and switch over to that site.

32
00:01:21.09 --> 00:01:24.03
And what you can do
here is grab some data.

33
00:01:24.03 --> 00:01:26.00
So I've already copied it.

34
00:01:26.00 --> 00:01:28.01
I'm just going to paste it right here

35
00:01:28.01 --> 00:01:29.08
and then hit this right arrow.

36
00:01:29.08 --> 00:01:32.01
And if it's a properly formatted JSON file

37
00:01:32.01 --> 00:01:35.02
it's going to sort of restructure the file

38
00:01:35.02 --> 00:01:37.03
and it's going to give you the ability

39
00:01:37.03 --> 00:01:39.00
to see the structure of the file.

40
00:01:39.00 --> 00:01:41.02
So in this object we have five elements.

41
00:01:41.02 --> 00:01:43.05
Here are the different elements.

42
00:01:43.05 --> 00:01:47.05
And if you open this right
here it's just a list

43
00:01:47.05 --> 00:01:51.02
of the individual items
for the weather data.

44
00:01:51.02 --> 00:01:53.07
So if we open one of these you can see

45
00:01:53.07 --> 00:01:57.03
that we have some
information here called data

46
00:01:57.03 --> 00:02:02.00
or dt_txt, that's the date of the entry.

47
00:02:02.00 --> 00:02:05.03
And then in here we
have weather information

48
00:02:05.03 --> 00:02:07.08
as well as some other data.

49
00:02:07.08 --> 00:02:10.07
The one that you want is called main,

50
00:02:10.07 --> 00:02:13.01
it has information about the temperature

51
00:02:13.01 --> 00:02:15.09
at that particular time
as well as pressure,

52
00:02:15.09 --> 00:02:18.03
humidity, et cetera, et cetera.

53
00:02:18.03 --> 00:02:20.06
So we want to get to this temperature data

54
00:02:20.06 --> 00:02:22.09
and then eventually I'm also going to grab

55
00:02:22.09 --> 00:02:24.04
this date right here.

56
00:02:24.04 --> 00:02:26.04
So to get that we just have to remember

57
00:02:26.04 --> 00:02:28.05
that we go to the object and then we find

58
00:02:28.05 --> 00:02:31.03
the list item and then
we have sort of an array

59
00:02:31.03 --> 00:02:33.06
of elements and there's 40 of them.

60
00:02:33.06 --> 00:02:35.02
These will be our different bars

61
00:02:35.02 --> 00:02:37.09
and then once you get to each element

62
00:02:37.09 --> 00:02:42.01
then you can go to either
dt_txt for the date

63
00:02:42.01 --> 00:02:45.02
and then main and if we
want say, the temperature

64
00:02:45.02 --> 00:02:48.05
we can just go to temp to get that.

65
00:02:48.05 --> 00:02:50.06
So let's go back into our file,

66
00:02:50.06 --> 00:02:53.06
you don't really need to look
at this forecast anymore.

67
00:02:53.06 --> 00:02:58.02
And what we're going to do
is go to the d3.json method

68
00:02:58.02 --> 00:03:00.08
and then read the data file.

69
00:03:00.08 --> 00:03:03.02
So it's in the Java Script folder, data

70
00:03:03.02 --> 00:03:08.00
and it's called forecast.json.

71
00:03:08.00 --> 00:03:11.08
Then we're going to use a
call back with that data

72
00:03:11.08 --> 00:03:15.02
and then we can do whatever we want.

73
00:03:15.02 --> 00:03:18.07
And let's go ahead and
put a comment in here.

74
00:03:18.07 --> 00:03:20.07
I just want to make a note
that if you are working

75
00:03:20.07 --> 00:03:24.01
with this data, it's not going
to work on your local directory.

76
00:03:24.01 --> 00:03:26.03
So if you just take the index.html file

77
00:03:26.03 --> 00:03:29.03
and you preview it in your browser

78
00:03:29.03 --> 00:03:31.04
it's not going to work,
it needs to be uploaded

79
00:03:31.04 --> 00:03:34.01
to a server or running on a server.

80
00:03:34.01 --> 00:03:35.08
If you're using the exercise files

81
00:03:35.08 --> 00:03:38.06
you should have a server running under

82
00:03:38.06 --> 00:03:42.01
local host 3000, so that's
not going to be an issue.

83
00:03:42.01 --> 00:03:44.04
And what I'm going to do
next is grab everything,

84
00:03:44.04 --> 00:03:49.05
just cut everything out and
put it inside this method.

85
00:03:49.05 --> 00:03:52.00
This is actually cool
because putting it in here

86
00:03:52.00 --> 00:03:55.01
is going to protect all
this information in here

87
00:03:55.01 --> 00:03:58.01
from anything else that I'm
doing with this index.html.

88
00:03:58.01 --> 00:04:02.02
And it's sort of like
doing what we call an iife,

89
00:04:02.02 --> 00:04:05.00
or an immediately executed function.

90
00:04:05.00 --> 00:04:08.01
And once I have that in here then I can do

91
00:04:08.01 --> 00:04:10.04
whatever I want, the data is going to come

92
00:04:10.04 --> 00:04:12.08
in this D sort of attribute.

93
00:04:12.08 --> 00:04:15.02
And so I'll need to modify
my file a little bit

94
00:04:15.02 --> 00:04:17.04
so that I can take advantage of the data.

95
00:04:17.04 --> 00:04:21.03
So I'm not going to go through
a loop like I've done here,

96
00:04:21.03 --> 00:04:23.03
or actually I am going
to go through a loop,

97
00:04:23.03 --> 00:04:25.07
but it's not going to fill this variable

98
00:04:25.07 --> 00:04:27.05
called bardata like this.

99
00:04:27.05 --> 00:04:32.04
So our loop, it's actually
going to be similar to this.

100
00:04:32.04 --> 00:04:34.09
And what we're going to do in that loop

101
00:04:34.09 --> 00:04:39.08
is go from zero to instead of I equals

102
00:04:39.08 --> 00:04:44.04
a hard number, we're going
to go to the D list length.

103
00:04:44.04 --> 00:04:50.04
And then we're going to
push this information

104
00:04:50.04 --> 00:04:53.06
right and instead of it
being this random number

105
00:04:53.06 --> 00:04:56.06
that we got here we can
go to an item in the data,

106
00:04:56.06 --> 00:04:58.08
so we'll say D list.

107
00:04:58.08 --> 00:05:05.04
And then we will use this
index to get our main temp.

108
00:05:05.04 --> 00:05:07.08
So that's sort of how I was saying

109
00:05:07.08 --> 00:05:12.04
you're going to go to list, main
and get this temp right here.

110
00:05:12.04 --> 00:05:15.01
So that's going to give
us all of our temperatures

111
00:05:15.01 --> 00:05:18.01
pushed into this bardata array.

112
00:05:18.01 --> 00:05:21.09
And I'm going to go ahead
and rename this bardata

113
00:05:21.09 --> 00:05:24.07
because it's not really bardata anymore,

114
00:05:24.07 --> 00:05:26.02
it's really temperatures.

115
00:05:26.02 --> 00:05:30.04
And so we'll rename this temperatures.

116
00:05:30.04 --> 00:05:32.03
If you're wonderin' how I did that,

117
00:05:32.03 --> 00:05:35.02
this particular editor
which is Visual Studio Code

118
00:05:35.02 --> 00:05:38.00
allows me to select an
element and then repeat

119
00:05:38.00 --> 00:05:40.06
that selection, most of the other editors

120
00:05:40.06 --> 00:05:42.02
have that same functionality,

121
00:05:42.02 --> 00:05:44.04
it's usually control, D or command, D

122
00:05:44.04 --> 00:05:46.07
depending on which computer you're using.

123
00:05:46.07 --> 00:05:48.08
So I'm on a Mac so it's command, D

124
00:05:48.08 --> 00:05:52.01
and it just selects a bunch
of the same sort of selections

125
00:05:52.01 --> 00:05:53.08
over and over.

126
00:05:53.08 --> 00:05:56.03
And most of the time you
can find it right here.

127
00:05:56.03 --> 00:06:00.04
So you can see it's
called add next occurrence

128
00:06:00.04 --> 00:06:02.02
in this particular editor.

129
00:06:02.02 --> 00:06:04.03
Alright, so once I have that data

130
00:06:04.03 --> 00:06:07.02
then I can start working
with it and what I want to do

131
00:06:07.02 --> 00:06:10.07
also is sort of clean up my variables.

132
00:06:10.07 --> 00:06:13.05
Right now I sort of create variables

133
00:06:13.05 --> 00:06:15.09
whenever I want and it's
getting a little bit messy

134
00:06:15.09 --> 00:06:19.01
and it's probably not how you should work.

135
00:06:19.01 --> 00:06:21.09
It's usually the way
that you do things in D3,

136
00:06:21.09 --> 00:06:25.09
but it's probably not the
best way of doing things.

137
00:06:25.09 --> 00:06:28.03
So what you want to usually
do is create your variables

138
00:06:28.03 --> 00:06:34.00
at the top of your file or your function

139
00:06:34.00 --> 00:06:36.09
and then what we're going to do then

140
00:06:36.09 --> 00:06:39.07
is create this variable
called temperatures

141
00:06:39.07 --> 00:06:42.03
that used to be called bardata

142
00:06:42.03 --> 00:06:45.02
and this will be an empty array.

143
00:06:45.02 --> 00:06:49.03
And then I'm going to move
all these other variables.

144
00:06:49.03 --> 00:06:52.06
So all this height, width,
banwidth, tempcolor,

145
00:06:52.06 --> 00:06:55.07
all that stuff is going
to just be declared

146
00:06:55.07 --> 00:06:57.03
up here as well.

147
00:06:57.03 --> 00:07:01.02
So let's just go ahead and use one var

148
00:07:01.02 --> 00:07:06.09
and we can indent these to
make them look a little nicer.

149
00:07:06.09 --> 00:07:09.06
And I'm also going to create
another variable section

150
00:07:09.06 --> 00:07:12.03
for variables that I'm not just declaring.

151
00:07:12.03 --> 00:07:14.05
So variables that are empty.

152
00:07:14.05 --> 00:07:18.03
So tempcolor will just sort of go there

153
00:07:18.03 --> 00:07:20.00
by itself for right now.

154
00:07:20.00 --> 00:07:22.04
And I'm also going to grab, any time I'm

155
00:07:22.04 --> 00:07:25.08
using the word var, the
keyword var right here

156
00:07:25.08 --> 00:07:28.02
I'm going to go ahead
and move those variables

157
00:07:28.02 --> 00:07:30.04
into just declarations.

158
00:07:30.04 --> 00:07:41.02
So I'll take Y scale and put that here.

159
00:07:41.02 --> 00:07:42.07
And I'm going to go ahead and do that

160
00:07:42.07 --> 00:07:48.01
for all the other var declarations.

161
00:07:48.01 --> 00:07:50.03
Alright, I'm also going
to clean up the spacing

162
00:07:50.03 --> 00:07:53.03
just a little bit, it's again,
getting a little bit messy.

163
00:07:53.03 --> 00:07:56.02
And I want to sort of
keep things consistent

164
00:07:56.02 --> 00:07:58.01
so let's indent this.

165
00:07:58.01 --> 00:08:05.06
And we'll clean up some of
this other spacing right here.

166
00:08:05.06 --> 00:08:07.03
Alright, so I finished cleaning things up

167
00:08:07.03 --> 00:08:09.08
and usually what I like
to do is indent things

168
00:08:09.08 --> 00:08:12.01
as consistently as I can.

169
00:08:12.01 --> 00:08:14.09
And with D3 you want to indent in

170
00:08:14.09 --> 00:08:19.06
whenever you do a dot method
and just usually one level.

171
00:08:19.06 --> 00:08:23.08
Sometimes when you do
enters I like to indent

172
00:08:23.08 --> 00:08:26.07
maybe another level and
then all theses attributes

173
00:08:26.07 --> 00:08:28.03
belong to that enter.

174
00:08:28.03 --> 00:08:30.05
So they go on that same level.

175
00:08:30.05 --> 00:08:33.02
On mouse over, on mouse
out, actually this one

176
00:08:33.02 --> 00:08:37.04
should be maybe indented in a little bit.

177
00:08:37.04 --> 00:08:41.04
And then all the variables
should be at the same levelage.

178
00:08:41.04 --> 00:08:44.01
It just makes things a little bit easier

179
00:08:44.01 --> 00:08:46.00
to work with in the future.

180
00:08:46.00 --> 00:08:48.06
Now let me go ahead and save this.

181
00:08:48.06 --> 00:08:51.08
You can see that the bars
are showing up really well,

182
00:08:51.08 --> 00:08:54.00
everything looks good.

183
00:08:54.00 --> 00:08:56.05
So as you can see, D3 is
making reading our data

184
00:08:56.05 --> 00:08:59.03
extremely simple, it's
even taken the JSON file

185
00:08:59.03 --> 00:09:02.00
that would normally be super hard to parse

186
00:09:02.00 --> 00:09:04.09
and given it to us in
a Java Script object.

187
00:09:04.09 --> 00:09:08.00
It would do the same
thing with either a CSV

188
00:09:08.00 --> 00:09:10.08
or a TSV, so whatever format your data

189
00:09:10.08 --> 00:09:13.03
is already in, D3 probably has a way

190
00:09:13.03 --> 00:09:16.02
of accessing the data using one of

191
00:09:16.02 --> 00:09:18.05
it's own AJAX based methods.

