1
00:00:00.08 --> 00:00:03.01
- [Instructor] Right now our
chart is using colors that

2
00:00:03.01 --> 00:00:04.09
are not really meaningful.

3
00:00:04.09 --> 00:00:08.04
We can improve this by
making the height of the bars

4
00:00:08.04 --> 00:00:10.08
be related to the temperature

5
00:00:10.08 --> 00:00:13.02
of each one of these days.

6
00:00:13.02 --> 00:00:15.08
So let's go ahead and do
that, should be pretty easy.

7
00:00:15.08 --> 00:00:18.01
First I'm going to go into index.html

8
00:00:18.01 --> 00:00:20.04
and I'm going to change the title here.

9
00:00:20.04 --> 00:00:22.08
This should be

10
00:00:22.08 --> 00:00:24.08
temperature changes,

11
00:00:24.08 --> 00:00:26.00
and I'm going to save that,

12
00:00:26.00 --> 00:00:27.08
it's going to update just the title.

13
00:00:27.08 --> 00:00:34.08
And then I want to go into my script file.

14
00:00:34.08 --> 00:00:36.04
So this is where we're creating the colors

15
00:00:36.04 --> 00:00:37.07
for our different bars.

16
00:00:37.07 --> 00:00:40.06
And right now they're sort
of horizontally relevant,

17
00:00:40.06 --> 00:00:42.08
I want to make them vertically relevant.

18
00:00:42.08 --> 00:00:48.01
So we're going to change our domain here.

19
00:00:48.01 --> 00:00:51.03
We're going to base our domain
on three different values.

20
00:00:51.03 --> 00:00:55.05
So from sort of zero, and
then another stopping point

21
00:00:55.05 --> 00:00:58.05
will be 65 degrees at which point

22
00:00:58.05 --> 00:01:01.00
things are going to be
sort of getting colder,

23
00:01:01.00 --> 00:01:05.09
and then we're going to use D3 max

24
00:01:05.09 --> 00:01:09.03
and pass along temperatures

25
00:01:09.03 --> 00:01:13.00
and that's going to
give us a set of numbers

26
00:01:13.00 --> 00:01:14.08
that is going to be, you know,

27
00:01:14.08 --> 00:01:17.03
essentially it's three numbers, zero, 65,

28
00:01:17.03 --> 00:01:20.01
and then sort of the hottest
temperature available.

29
00:01:20.01 --> 00:01:22.01
And I'm going to modify
the colors a little bit.

30
00:01:22.01 --> 00:01:24.07
So if it's sort of below
65, which none of these

31
00:01:24.07 --> 00:01:26.05
are below 65.

32
00:01:26.05 --> 00:01:29.05
Technically we're going
to make this white,

33
00:01:29.05 --> 00:01:34.01
then the next color is
going to be a blue color,

34
00:01:34.01 --> 00:01:36.01
and then we'll do a red color

35
00:01:36.01 --> 00:01:37.09
if it's sort of hotter than that.

36
00:01:37.09 --> 00:01:42.01
DA3637 here,

37
00:01:42.01 --> 00:01:43.09
let's go ahead and put
these on the same line

38
00:01:43.09 --> 00:01:46.00
so they look a little bit better.

39
00:01:46.00 --> 00:01:48.09
All right, also down
here when we're actually

40
00:01:48.09 --> 00:01:51.03
applying the colors we don't really need

41
00:01:51.03 --> 00:01:53.03
the color to be related to the index,

42
00:01:53.03 --> 00:01:54.08
that would relate it to the position

43
00:01:54.08 --> 00:01:56.09
and that's kind of what's
happening right now.

44
00:01:56.09 --> 00:02:00.00
So all we have to do
here is just modify this

45
00:02:00.00 --> 00:02:02.04
so that it says attribute fill,

46
00:02:02.04 --> 00:02:05.05
and then use colors.

47
00:02:05.05 --> 00:02:08.05
Right, so if we say that
you're going to see that the,

48
00:02:08.05 --> 00:02:10.05
sort of taller the temperature

49
00:02:10.05 --> 00:02:13.09
the hotter it looks, and then
the cooler the temperature

50
00:02:13.09 --> 00:02:15.03
the cooler it looks.

51
00:02:15.03 --> 00:02:17.03
That's because we're going
to those break points,

52
00:02:17.03 --> 00:02:21.03
there are some places in which
it gets a little bit below

53
00:02:21.03 --> 00:02:26.02
60, and we had set sort of anything

54
00:02:26.02 --> 00:02:30.09
from that range, anything
that's 65 to zero

55
00:02:30.09 --> 00:02:33.08
gets to be either like a blue or

56
00:02:33.08 --> 00:02:35.07
it would actually start
going towards a white

57
00:02:35.07 --> 00:02:37.09
as it got a little bit colder

58
00:02:37.09 --> 00:02:39.02
if our temperatures went that far.

59
00:02:39.02 --> 00:02:40.05
So you can see that this is pretty easy,

60
00:02:40.05 --> 00:02:43.05
all we have to do is create
sort of the different stops

61
00:02:43.05 --> 00:02:47.03
and we can just add numbers and then

62
00:02:47.03 --> 00:02:49.02
in relation to those numbers our ranges

63
00:02:49.02 --> 00:02:51.06
are going to be these colors,
and it's just going to change

64
00:02:51.06 --> 00:02:53.03
the colors to match.

65
00:02:53.03 --> 00:02:56.00
So I want to do a couple of
other little cleanup things.

66
00:02:56.00 --> 00:02:59.00
I don't like this sort of gap being so big

67
00:02:59.00 --> 00:03:02.07
so I'm going to modify this
padding inner right here,

68
00:03:02.07 --> 00:03:05.08
I'll make that just point
one, let's see how that looks.

69
00:03:05.08 --> 00:03:07.04
That looks a little bit better I think.

70
00:03:07.04 --> 00:03:11.09
And I don't need the barwidth

71
00:03:11.09 --> 00:03:14.03
or the baroffset anymore,

72
00:03:14.03 --> 00:03:16.02
let's go ahead and get rid of those.

73
00:03:16.02 --> 00:03:18.02
It doesn't really make a
difference because we're actually

74
00:03:18.02 --> 00:03:21.03
making our widths out
of the available data,

75
00:03:21.03 --> 00:03:24.01
so it's better to get
rid of those variables.

76
00:03:24.01 --> 00:03:27.08
And then my sort of temperatures
don't look that great,

77
00:03:27.08 --> 00:03:31.04
so I'm going to improve
on that just a little bit.

78
00:03:31.04 --> 00:03:34.07
And I can do that where I go in to

79
00:03:34.07 --> 00:03:40.06
modify my HTML, and you can
see that it's right here.

80
00:03:40.06 --> 00:03:43.01
So instead of just having a simple D here,

81
00:03:43.01 --> 00:03:45.05
we're going make this a
little more complicated.

82
00:03:45.05 --> 00:03:48.02
And I can just type in HTML in here,

83
00:03:48.02 --> 00:03:51.07
so I'll do a div, and
I can sort of just put

84
00:03:51.07 --> 00:03:54.06
a style attribute in
here, this is a quick way,

85
00:03:54.06 --> 00:03:56.05
instead of doing individual attributes

86
00:03:56.05 --> 00:03:58.07
you can just sort of do HTML

87
00:03:58.07 --> 00:04:01.02
and type all these at once.

88
00:04:01.02 --> 00:04:07.02
So I'm going to do two rems,
bold font weight there,

89
00:04:07.02 --> 00:04:10.08
and then we're going to
put the D, or the data,

90
00:04:10.08 --> 00:04:14.03
and then we'll put the degree symbol

91
00:04:14.03 --> 00:04:16.09
and close out that div.

92
00:04:16.09 --> 00:04:18.02
And let's check it out.

93
00:04:18.02 --> 00:04:19.07
So that looks a lot better now

94
00:04:19.07 --> 00:04:22.03
when you go over the individual bars,

95
00:04:22.03 --> 00:04:24.05
that looks great.

96
00:04:24.05 --> 00:04:25.07
Another thing that I want to fix is,

97
00:04:25.07 --> 00:04:27.05
notice that if I sort of go over this bar

98
00:04:27.05 --> 00:04:30.09
and I get off of the chart,
or even as I go in between

99
00:04:30.09 --> 00:04:33.05
the bars if I sort of come up here,

100
00:04:33.05 --> 00:04:37.00
this actually stays there
and I want to fix that.

101
00:04:37.00 --> 00:04:41.04
So all I have to do is
just sort of, on mouse out

102
00:04:41.04 --> 00:04:46.04
I just need to sort of clear
out the HTML, so I'll do that.

103
00:04:46.04 --> 00:04:51.03
And I'll put a empty string here.

104
00:04:51.03 --> 00:04:52.05
A few adjustments here and there

105
00:04:52.05 --> 00:04:55.00
by making the colors
track our temperatures,

106
00:04:55.00 --> 00:04:57.08
we've created a chart that
is a little more meaningful

107
00:04:57.08 --> 00:05:01.01
and also improve things
by changing the way

108
00:05:01.01 --> 00:05:03.07
the HTML works in our rollovers.

