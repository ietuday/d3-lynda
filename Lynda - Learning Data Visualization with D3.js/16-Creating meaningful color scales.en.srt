1
00:00:00.05 --> 00:00:02.07
- [Narrator] Right now our
chart uses a single color

2
00:00:02.07 --> 00:00:05.07
and that's perfectly acceptable
but D3 will allow you

3
00:00:05.07 --> 00:00:08.02
to create color scales to make our charts

4
00:00:08.02 --> 00:00:10.03
look a little more interesting.

5
00:00:10.03 --> 00:00:13.09
So, let's start by making
the height of our data be

6
00:00:13.09 --> 00:00:17.03
meaningful to the way that
it maps to a range of colors.

7
00:00:17.03 --> 00:00:19.04
To do that I'm going to create a new scale

8
00:00:19.04 --> 00:00:22.09
and I'm going to create a
variable here called colors

9
00:00:22.09 --> 00:00:26.05
and set that to a D3 linear scale.

10
00:00:26.05 --> 00:00:29.09
Then, just like with any
other scale I need to create a

11
00:00:29.09 --> 00:00:33.07
domain and that needs to be an array,

12
00:00:33.07 --> 00:00:36.00
which I'll set from zero.

13
00:00:36.00 --> 00:00:37.03
And because I want the height

14
00:00:37.03 --> 00:00:40.01
of the elements to be meaningful,

15
00:00:40.01 --> 00:00:42.07
going to set that to D3 and then the

16
00:00:42.07 --> 00:00:47.06
maximum value of the bardata.

17
00:00:47.06 --> 00:00:51.07
As the range I'm just going
to include a range of colors,

18
00:00:51.07 --> 00:00:58.03
that also needs to go in an array.

19
00:00:58.03 --> 00:00:59.07
Alright, so in addition to that I need

20
00:00:59.07 --> 00:01:02.01
to change the fill attributes.

21
00:01:02.01 --> 00:01:04.05
So right now we're using Style,

22
00:01:04.05 --> 00:01:09.01
I'm going to change that to Attribute.

23
00:01:09.01 --> 00:01:11.01
And then use a method here.

24
00:01:11.01 --> 00:01:13.01
So instead of using a single color

25
00:01:13.01 --> 00:01:17.04
I'm going to use a callback.

26
00:01:17.04 --> 00:01:21.06
And then return the colors right here.

27
00:01:21.06 --> 00:01:23.07
So when they do that you
can see that the shorter

28
00:01:23.07 --> 00:01:27.05
the bar is the more it goes
towards this first color.

29
00:01:27.05 --> 00:01:30.08
Then the longer the bar is,
and the maximum bar here,

30
00:01:30.08 --> 00:01:34.07
will be adjusted to this color right here.

31
00:01:34.07 --> 00:01:38.07
So, the height of the bar
determines the color of the item.

32
00:01:38.07 --> 00:01:41.08
If you want to make the
horizontal position of the bar

33
00:01:41.08 --> 00:01:45.01
meaningful you can modify this
color scale so that it uses

34
00:01:45.01 --> 00:01:48.08
the number of elements in
our data as the domain.

35
00:01:48.08 --> 00:01:52.06
So, let's go ahead and do
that in here for colors as the

36
00:01:52.06 --> 00:01:57.01
domain we're going to use
something different, bardata,

37
00:01:57.01 --> 00:02:00.09
length, we'll leave the
range the same for right now.

38
00:02:00.09 --> 00:02:05.09
And then in our colors
we're going to use instead

39
00:02:05.09 --> 00:02:10.06
the index of the color as
what gets returned here.

40
00:02:10.06 --> 00:02:13.06
So now you can see that it
goes from this first color

41
00:02:13.06 --> 00:02:16.06
all the way to this last color right here.

42
00:02:16.06 --> 00:02:18.07
Now if we wanted to add additional colors

43
00:02:18.07 --> 00:02:22.05
then let's go ahead and modify our scale

44
00:02:22.05 --> 00:02:24.07
to just have more values.

45
00:02:24.07 --> 00:02:28.02
So to do that, what I'll
do is I'm going to set

46
00:02:28.02 --> 00:02:30.08
this first to an empty array

47
00:02:30.08 --> 00:02:37.05
and then I'm going to
create a variable loop here.

48
00:02:37.05 --> 00:02:42.00
And here I'll just create a random number

49
00:02:42.00 --> 00:02:45.00
for each of the values of our data.

50
00:02:45.00 --> 00:02:47.00
And then I'll create 30 of them just

51
00:02:47.00 --> 00:02:49.02
so that we have a bunch of them.

52
00:02:49.02 --> 00:02:51.06
And it looks like I have
to put in I right here.

53
00:02:51.06 --> 00:02:53.07
And now you can see 30 pieces of data

54
00:02:53.07 --> 00:02:56.09
every time I refresh you'll
see sort of a different

55
00:02:56.09 --> 00:03:00.09
set of data but you can see
now that all the gradation

56
00:03:00.09 --> 00:03:03.06
shows up a little bit better
when you have more data.

57
00:03:03.06 --> 00:03:06.02
And you can put different
numbers in here if you want to.

58
00:03:06.02 --> 00:03:09.07
Now I'm going to modify
our scale so that it allows

59
00:03:09.07 --> 00:03:12.03
me to use more values than just two.

60
00:03:12.03 --> 00:03:15.00
If you want to do that
then what I want to do is

61
00:03:15.00 --> 00:03:18.09
here just sort of add
a couple of variables

62
00:03:18.09 --> 00:03:20.08
or a couple of sort of steps.

63
00:03:20.08 --> 00:03:30.01
First I'll do zero the
bardata length times .33 here

64
00:03:30.01 --> 00:03:38.09
and then I will do
bardata length times .66.

65
00:03:38.09 --> 00:03:42.05
And let's go ahead and add
sort of carriage returns here

66
00:03:42.05 --> 00:03:44.07
so we can see these a little bit better.

67
00:03:44.07 --> 00:03:50.09
And then I'll create just
one that says bardata length.

68
00:03:50.09 --> 00:03:55.04
So what this will do is create
stops at the zero position

69
00:03:55.04 --> 00:03:58.00
then a third of the way and then sort of

70
00:03:58.00 --> 00:04:02.02
two thirds of the way and then
the full length of the data.

71
00:04:02.02 --> 00:04:08.00
And then I can just add
more values right here.

72
00:04:08.00 --> 00:04:09.05
So now that we have all these values

73
00:04:09.05 --> 00:04:13.03
you can see that we get a much
more sort of colorful scale

74
00:04:13.03 --> 00:04:15.03
since we have all these new values

75
00:04:15.03 --> 00:04:18.02
and we're able to add different stops

76
00:04:18.02 --> 00:04:22.03
by using these different ranges.

77
00:04:22.03 --> 00:04:24.00
I think this table is going
to look a little bit nicer

78
00:04:24.00 --> 00:04:25.07
without this background color.

79
00:04:25.07 --> 00:04:28.03
So let me go ahead and remove that.

80
00:04:28.03 --> 00:04:31.00
And you can see that by
adding a color scale here,

81
00:04:31.00 --> 00:04:33.08
just a simple linear
scale that maps colors to

82
00:04:33.08 --> 00:04:37.00
different values we can make
our data look a little bit

83
00:04:37.00 --> 00:04:41.02
more visually appealing
and also more informative.

