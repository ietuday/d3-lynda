1
00:00:00.03 --> 00:00:02.04
- A guide in D3 provides a way

2
00:00:02.04 --> 00:00:05.04
to show meaningful information
next to your chart.

3
00:00:05.04 --> 00:00:06.05
To create a guide,

4
00:00:06.05 --> 00:00:08.00
you create a scale just like you did

5
00:00:08.00 --> 00:00:11.03
for any of the other
elements like the bars

6
00:00:11.03 --> 00:00:12.06
and although most of the time,

7
00:00:12.06 --> 00:00:14.06
the data you sent to the scale is similar,

8
00:00:14.06 --> 00:00:18.01
sometimes it has to be modified a bit.

9
00:00:18.01 --> 00:00:19.09
You'll also need to use one of the methods

10
00:00:19.09 --> 00:00:23.07
for defining in what position
your scale should go.

11
00:00:23.07 --> 00:00:28.02
So top right, bottom or
left next to your chart.

12
00:00:28.02 --> 00:00:29.02
Once you have a scale,

13
00:00:29.02 --> 00:00:32.01
you'll need to define how
the tick marks will work

14
00:00:32.01 --> 00:00:34.02
inside the scale with a number of methods

15
00:00:34.02 --> 00:00:36.09
relating to these ticks.

16
00:00:36.09 --> 00:00:39.01
You'll also need to make
sure that your chart,

17
00:00:39.01 --> 00:00:41.03
as well as these different guides

18
00:00:41.03 --> 00:00:44.04
are also in groups of their own.

19
00:00:44.04 --> 00:00:46.05
So let's take a look at how that works.

20
00:00:46.05 --> 00:00:49.09
So I'm going to come up
here and in this section

21
00:00:49.09 --> 00:00:52.07
where I define my yScale,

22
00:00:52.07 --> 00:00:54.03
and of course this will be the scale

23
00:00:54.03 --> 00:00:56.01
that makes these bars tall.

24
00:00:56.01 --> 00:00:57.07
Let's go ahead and move this over

25
00:00:57.07 --> 00:01:01.05
so that we can see it
on this side right here.

26
00:01:01.05 --> 00:01:04.07
I'm going to add a new sort of scale

27
00:01:04.07 --> 00:01:09.00
and I'm going to call this yAxisValues

28
00:01:09.00 --> 00:01:11.02
and this is going to be a linear scale,

29
00:01:11.02 --> 00:01:14.07
just like our, the height of our elements,

30
00:01:14.07 --> 00:01:17.03
d3scaleLinear.

31
00:01:17.03 --> 00:01:19.08
As a matter of fact, this is
going to be pretty similar

32
00:01:19.08 --> 00:01:22.04
to the yScale and I'll show you

33
00:01:22.04 --> 00:01:25.08
how we want to make it
different in just a minute.

34
00:01:25.08 --> 00:01:28.08
So here we're going to
have the same domain

35
00:01:28.08 --> 00:01:31.06
using our temperatures, but these values

36
00:01:31.06 --> 00:01:35.04
are going to need to be
reversed because our bars

37
00:01:35.04 --> 00:01:39.04
are being drawn from the bottom to the top

38
00:01:39.04 --> 00:01:43.09
and if we live this right
here, you're going to see

39
00:01:43.09 --> 00:01:47.07
that our scales are going to
be drawn in the wrong order.

40
00:01:47.07 --> 00:01:50.03
So we need to actually reverse this

41
00:01:50.03 --> 00:01:52.09
to be height first

42
00:01:52.09 --> 00:01:55.04
and then zero.

43
00:01:55.04 --> 00:01:56.08
So once we have that,

44
00:01:56.08 --> 00:01:59.09
then we're going to need
to define our ticks.

45
00:01:59.09 --> 00:02:03.06
That's the little tick
marks that appear in a guide

46
00:02:03.06 --> 00:02:06.08
and we're going to call that variable

47
00:02:06.08 --> 00:02:09.09
yAxisTicks

48
00:02:09.09 --> 00:02:14.05
and we're going to set that to axisLeft

49
00:02:14.05 --> 00:02:19.01
and then pass along the yAxisValues.

50
00:02:19.01 --> 00:02:20.09
We're sort of passing along this

51
00:02:20.09 --> 00:02:22.05
and since we've created
these two variables,

52
00:02:22.05 --> 00:02:24.02
we're going to need to add it

53
00:02:24.02 --> 00:02:26.04
to our list of variables right here.

54
00:02:26.04 --> 00:02:32.08
So let's go ahead and do
that before we forget.

55
00:02:32.08 --> 00:02:35.08
So passing along those values,
then what we want to do

56
00:02:35.08 --> 00:02:39.04
is just specify how many
of the tick marks we want.

57
00:02:39.04 --> 00:02:42.01
So the easiest function
to use is called ticks

58
00:02:42.01 --> 00:02:44.01
and you simply pass it along

59
00:02:44.01 --> 00:02:47.09
how many elements you
want this thing to have.

60
00:02:47.09 --> 00:02:48.08
So we're almost done.

61
00:02:48.08 --> 00:02:52.01
We've sort of defined our axis and ticks

62
00:02:52.01 --> 00:02:55.07
and now we need to sort of
insert this into our graphic.

63
00:02:55.07 --> 00:02:58.02
Now to do that, I need
to go ahead and group

64
00:02:58.02 --> 00:03:00.04
all of my stuff together.

65
00:03:00.04 --> 00:03:04.05
So right now, since we're
drawing just one element,

66
00:03:04.05 --> 00:03:06.07
it's really easy to just sort of draw

67
00:03:06.07 --> 00:03:10.09
that element continuously,
but in reality you may have

68
00:03:10.09 --> 00:03:12.06
more than one element.

69
00:03:12.06 --> 00:03:15.03
To do that, we need to
put things in a group

70
00:03:15.03 --> 00:03:18.04
so right after we define
the width and the height

71
00:03:18.04 --> 00:03:21.07
of this chart, then I'm
going to hand another

72
00:03:21.07 --> 00:03:25.06
element here and that's
going to be the G element,

73
00:03:25.06 --> 00:03:27.08
which as you know groups anything

74
00:03:27.08 --> 00:03:31.05
inside or underneath all that.

75
00:03:31.05 --> 00:03:33.04
So all of these bars are

76
00:03:33.04 --> 00:03:36.00
going to be grouped inside a G element

77
00:03:36.00 --> 00:03:38.00
Once we have that,

78
00:03:38.00 --> 00:03:40.03
then we can add

79
00:03:40.03 --> 00:03:42.09
that guide right here.

80
00:03:42.09 --> 00:03:46.00
So after we define sort of this chart

81
00:03:46.00 --> 00:03:47.08
and before we define the transition,

82
00:03:47.08 --> 00:03:49.08
you could do it after if you wanted to,

83
00:03:49.08 --> 00:03:54.01
then you can create
sort of the ticks here.

84
00:03:54.01 --> 00:03:56.03
So we'll call this the yGuide

85
00:03:56.03 --> 00:04:00.02
and make that equal to d3 select.

86
00:04:00.02 --> 00:04:04.07
We want to select our visualization

87
00:04:04.07 --> 00:04:08.00
and the svg inside our visualization

88
00:04:08.00 --> 00:04:10.05
and then we're going to
append another group.

89
00:04:10.05 --> 00:04:12.08
So we have one group that is the chart

90
00:04:12.08 --> 00:04:14.08
and we're adding a second group

91
00:04:14.08 --> 00:04:17.09
that's going to be our guides

92
00:04:17.09 --> 00:04:19.08
and then

93
00:04:19.08 --> 00:04:23.00
we can just use this call method

94
00:04:23.00 --> 00:04:28.04
that is going to use our yAxisTicks.

95
00:04:28.04 --> 00:04:31.00
Alright, so you can see
actually like a small line

96
00:04:31.00 --> 00:04:33.08
appearing right next to our chart.

97
00:04:33.08 --> 00:04:35.04
You're not going to see anything else

98
00:04:35.04 --> 00:04:37.06
until we actually add
a little bit of padding

99
00:04:37.06 --> 00:04:39.04
or a little bit of room here.

100
00:04:39.04 --> 00:04:42.03
So we're going to need to add an attribute

101
00:04:42.03 --> 00:04:45.03
and then do a transform

102
00:04:45.03 --> 00:04:48.08
that uses the translate method

103
00:04:48.08 --> 00:04:50.08
to add a little bit of room here

104
00:04:50.08 --> 00:04:55.00
to the left of this element

105
00:04:55.00 --> 00:04:56.05
and we actually

106
00:04:56.05 --> 00:04:57.06
need to make sure

107
00:04:57.06 --> 00:05:00.01
we put this quote right here

108
00:05:00.01 --> 00:05:01.06
and then that we close this one.

109
00:05:01.06 --> 00:05:05.01
Now you can see sort of
all the ticks, right?

110
00:05:05.01 --> 00:05:06.09
Going from zero to the top.

111
00:05:06.09 --> 00:05:08.09
Obviously we need to add some margins.

112
00:05:08.09 --> 00:05:11.02
We'll do that in a later video.

113
00:05:11.02 --> 00:05:14.00
So notice that we use this call method

114
00:05:14.00 --> 00:05:16.05
to retrieve the ticks

115
00:05:16.05 --> 00:05:18.01
and notice that

116
00:05:18.01 --> 00:05:21.09
we did have to use a different scale

117
00:05:21.09 --> 00:05:22.09
so you may think,

118
00:05:22.09 --> 00:05:25.07
well why didn't we just use the yScale?

119
00:05:25.07 --> 00:05:28.00
If you do that, what's
going to happen is that

120
00:05:28.00 --> 00:05:30.04
these are going to display
in the wrong order.

121
00:05:30.04 --> 00:05:33.03
They're displaying from top to bottom.

122
00:05:33.03 --> 00:05:35.02
Like they sort of normally should,

123
00:05:35.02 --> 00:05:37.07
except in this case we want them reversed

124
00:05:37.07 --> 00:05:39.03
and that's really why we had to create

125
00:05:39.03 --> 00:05:42.08
a different sort of linear scale

126
00:05:42.08 --> 00:05:44.04
to make sure that these tick marks

127
00:05:44.04 --> 00:05:47.03
are displaying in the proper order.

128
00:05:47.03 --> 00:05:50.02
It's not going to always
happen, but it's a good idea

129
00:05:50.02 --> 00:05:53.06
to maybe create yourself
a separate sort of scale

130
00:05:53.06 --> 00:05:57.03
just for your tick marks.

131
00:05:57.03 --> 00:05:59.06
Simple guides are super
useful and help your charts

132
00:05:59.06 --> 00:06:01.05
make a little bit more sense.

133
00:06:01.05 --> 00:06:03.04
D3 provides a number of methods

134
00:06:03.04 --> 00:06:05.04
that take care of building them.

135
00:06:05.04 --> 00:06:07.03
If you want to learn more about guides,

136
00:06:07.03 --> 00:06:11.00
take a look at this URL
in the documentation.

