1
00:00:00.03 --> 00:00:02.04
- [Instructor] Now that we
have a good understanding

2
00:00:02.04 --> 00:00:04.07
of how selections and attributes work

3
00:00:04.07 --> 00:00:07.03
it's time to add some data to the mix.

4
00:00:07.03 --> 00:00:09.06
Now data can be added in a
number of different ways.

5
00:00:09.06 --> 00:00:12.01
In this video I'm going to
show you the simplest way

6
00:00:12.01 --> 00:00:15.03
to add information using the data method.

7
00:00:15.03 --> 00:00:17.05
The data method joins
the current selection

8
00:00:17.05 --> 00:00:19.08
with some information that you provide.

9
00:00:19.08 --> 00:00:22.07
Now it takes an array of
elements and the first piece

10
00:00:22.07 --> 00:00:26.01
of data in that array is
going to be assigned to

11
00:00:26.01 --> 00:00:28.09
the first element in the selection

12
00:00:28.09 --> 00:00:31.04
and in the case of
select all the second one

13
00:00:31.04 --> 00:00:33.05
is going to be assigned
to the second element

14
00:00:33.05 --> 00:00:37.02
and any additional elements
are going to be assigned

15
00:00:37.02 --> 00:00:39.04
to anything else in the selection.

16
00:00:39.04 --> 00:00:42.04
Now you can also pass the
data to a function and this

17
00:00:42.04 --> 00:00:45.00
is called a callback in java script

18
00:00:45.00 --> 00:00:48.01
and that function can inherit
the data passed down to it

19
00:00:48.01 --> 00:00:49.06
and do something with it.

20
00:00:49.06 --> 00:00:52.04
In addition to the data
within this function

21
00:00:52.04 --> 00:00:56.03
you also have access to an
index which you can use as well

22
00:00:56.03 --> 00:01:00.06
as the data to manage what
d3 does with the selection

23
00:01:00.06 --> 00:01:02.05
or the information that you get back.

24
00:01:02.05 --> 00:01:04.05
So let's take a look at how this works.

25
00:01:04.05 --> 00:01:07.02
Here we are with the script
from the previous video

26
00:01:07.02 --> 00:01:09.01
so I'm going to clear this out.

27
00:01:09.01 --> 00:01:11.05
I'll leave the first selection part alone

28
00:01:11.05 --> 00:01:16.06
and in here what I want to
do is access the day-high

29
00:01:16.06 --> 00:01:20.07
temperatures so we'll
target day-high and temp

30
00:01:20.07 --> 00:01:23.07
and if you remember from my index at html

31
00:01:23.07 --> 00:01:29.02
I have different td's and
inside those I have either

32
00:01:29.02 --> 00:01:33.06
a class of day-high or day-lows
so we're targeting just

33
00:01:33.06 --> 00:01:36.08
the high temperatures and then
we have an item right here

34
00:01:36.08 --> 00:01:39.07
with a class of temp which
gives you just the number

35
00:01:39.07 --> 00:01:43.06
right here so we can just
target those temperatures

36
00:01:43.06 --> 00:01:45.03
that are in the high dates.

37
00:01:45.03 --> 00:01:47.06
So all this column and just the numbers,

38
00:01:47.06 --> 00:01:50.00
we don't need the degree or the farenheit.

39
00:01:50.00 --> 00:01:53.00
So what you could do here
is use the data method

40
00:01:53.00 --> 00:01:55.04
and pass along an array of data.

41
00:01:55.04 --> 00:01:57.01
So I'm just going to
type in one for right now

42
00:01:57.01 --> 00:01:58.08
so you can see what happens.

43
00:01:58.08 --> 00:02:02.06
And then once you have that
data then you can create

44
00:02:02.06 --> 00:02:04.09
some sort of function that uses the data

45
00:02:04.09 --> 00:02:09.01
so I'm going to use the html
method that we've used before

46
00:02:09.01 --> 00:02:13.00
and I'm going to pass
this along to a callback.

47
00:02:13.00 --> 00:02:16.06
This means that I want to
do something with that data.

48
00:02:16.06 --> 00:02:21.04
This d is going to assume
that there's some data before

49
00:02:21.04 --> 00:02:26.01
and d3 sort of remembers the
last bit of data I have added

50
00:02:26.01 --> 00:02:30.00
with this data method so we
don't have to do anything

51
00:02:30.00 --> 00:02:32.08
special in here and then what I can do

52
00:02:32.08 --> 00:02:34.06
is just return that data.

53
00:02:34.06 --> 00:02:37.04
So let's go ahead and save
and take a look right here

54
00:02:37.04 --> 00:02:38.07
when I save this,

55
00:02:38.07 --> 00:02:41.01
this is going to be updated to 45

56
00:02:41.01 --> 00:02:44.00
because this is the first
element and I'm using select all.

57
00:02:44.00 --> 00:02:47.08
So it's returning an array
of elements and it's just

58
00:02:47.08 --> 00:02:51.06
replacing the first one because
it didn't find any others.

59
00:02:51.06 --> 00:02:55.06
Let's go ahead and type in some
additional information here.

60
00:02:55.06 --> 00:02:58.05
It's sort of pretty smart
so now when I type all these

61
00:02:58.05 --> 00:03:01.00
you can see that it replaced all of them.

62
00:03:01.00 --> 00:03:02.06
if I type anything else,

63
00:03:02.06 --> 00:03:05.02
it's just sort of going to
ignore the fact that I don't have

64
00:03:05.02 --> 00:03:07.05
another record for this

65
00:03:07.05 --> 00:03:10.05
and if I forget to type one
in or I don't type one in

66
00:03:10.05 --> 00:03:12.04
then it just doesn't change that one.

67
00:03:12.04 --> 00:03:14.08
So it's really pretty smart

68
00:03:14.08 --> 00:03:17.05
and since this is the html method,

69
00:03:17.05 --> 00:03:20.09
I could have used text in here
or any of the other methods

70
00:03:20.09 --> 00:03:22.09
like append or anything else.

71
00:03:22.09 --> 00:03:26.00
they all understand this
sort of callback method

72
00:03:26.00 --> 00:03:27.05
and in here if I want to

73
00:03:27.05 --> 00:03:30.02
because this is the html method
I can go and add something

74
00:03:30.02 --> 00:03:37.02
else like an html tag and
let's make this stuff look bold

75
00:03:37.02 --> 00:03:41.08
by adding a strong tag and
as we save that you can see

76
00:03:41.08 --> 00:03:43.01
that they are all bold.

77
00:03:43.01 --> 00:03:44.05
Now it didn't do anything to this one

78
00:03:44.05 --> 00:03:46.05
because we didn't have data for that one.

79
00:03:46.05 --> 00:03:51.04
Now let's go ahead and add some
data here for this other one

80
00:03:51.04 --> 00:03:54.05
and we can see that
now it has modified it.

81
00:03:54.05 --> 00:03:56.03
So in addition to the data,

82
00:03:56.03 --> 00:04:00.04
you can also receive an index
and this is just a number

83
00:04:00.04 --> 00:04:03.04
that allows you to target
an individual element

84
00:04:03.04 --> 00:04:05.00
in any particular order.

85
00:04:05.00 --> 00:04:06.09
So we could say something like

86
00:04:06.09 --> 00:04:08.07
if this is the first element,

87
00:04:08.07 --> 00:04:11.04
if the index happens to be 0,

88
00:04:11.04 --> 00:04:14.04
remember java script is 0 indexed

89
00:04:14.04 --> 00:04:19.01
then I want you to return
this but I'm going to add

90
00:04:19.01 --> 00:04:25.02
a class and this is going to
be a bootstrap class of text.

91
00:04:25.02 --> 00:04:26.06
Mute it.

92
00:04:26.06 --> 00:04:28.01
Let's go ahead and save that.

93
00:04:28.01 --> 00:04:30.04
You can see that the number
here is a little bit grayer

94
00:04:30.04 --> 00:04:34.04
and then if you want to we
can add a style tag here

95
00:04:34.04 --> 00:04:36.09
and save font size.

96
00:04:36.09 --> 00:04:40.07
We'll make this 2rem and
then I'm going to save that

97
00:04:40.07 --> 00:04:43.08
and now it makes this
text a little bit bigger.

98
00:04:43.08 --> 00:04:45.05
So that's pretty cool

99
00:04:45.05 --> 00:04:50.01
and then what we can do is say otherwise,

100
00:04:50.01 --> 00:04:53.04
then go ahead and return this

101
00:04:53.04 --> 00:04:56.03
and then we can do some
other checks in here.

102
00:04:56.03 --> 00:05:01.02
We can say if the value
of this data is greater

103
00:05:01.02 --> 00:05:04.04
than or equal to some number,

104
00:05:04.04 --> 00:05:07.01
so 76 sounds like,

105
00:05:07.01 --> 00:05:10.04
(typing

106
00:05:10.04 --> 00:05:12.00
yes maybe 77,

107
00:05:12.00 --> 00:05:15.01
sounds like it's getting a
little warm for my taste.

108
00:05:15.01 --> 00:05:22.07
So if the value of that
number is greater than 77

109
00:05:22.07 --> 00:05:27.06
then I'm going to make
this class here text-danger

110
00:05:27.06 --> 00:05:31.02
(typing)

111
00:05:31.02 --> 00:05:33.07
and then I'll just do another
else here and than otherwise

112
00:05:33.07 --> 00:05:37.04
this return the text by itself.

113
00:05:37.04 --> 00:05:40.06
so anything that's above 77 degrees,

114
00:05:40.06 --> 00:05:42.06
it's going to make red right here.

115
00:05:42.06 --> 00:05:44.00
so that's pretty cool;

116
00:05:44.00 --> 00:05:47.07
we can use not just the
data but also the index

117
00:05:47.07 --> 00:05:50.00
and the other thing to
remember that's important

118
00:05:50.00 --> 00:05:54.02
is that d3 is going to
pass this data along

119
00:05:54.02 --> 00:05:55.05
and it's kind of smart.

120
00:05:55.05 --> 00:05:59.06
It sort of just replaces as
many elements as you specify.

121
00:05:59.06 --> 00:06:01.08
If you don't specify an element

122
00:06:01.08 --> 00:06:04.04
it's just sort of going
to ignore things and then

123
00:06:04.04 --> 00:06:08.03
it allows you to modify that
data or control how that data

124
00:06:08.03 --> 00:06:11.08
is displayed with these
two extra variables,

125
00:06:11.08 --> 00:06:14.03
the data as well as the index.

