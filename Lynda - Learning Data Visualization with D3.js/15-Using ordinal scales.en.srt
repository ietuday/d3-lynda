1
00:00:00.06 --> 00:00:02.04
- [Narrator] Sometimes
the order of your data has

2
00:00:02.04 --> 00:00:04.03
a certain importance to the type

3
00:00:04.03 --> 00:00:06.02
of information you need to plot,

4
00:00:06.02 --> 00:00:08.06
and D3 provides a number of scales known

5
00:00:08.06 --> 00:00:11.09
as ordinal scales to
take care of just that.

6
00:00:11.09 --> 00:00:13.06
So let's take a look at how that works.

7
00:00:13.06 --> 00:00:15.01
Now the difference between a linear

8
00:00:15.01 --> 00:00:17.01
and an ordinal scale
is like the difference

9
00:00:17.01 --> 00:00:21.01
between an unordered
list and an ordered list.

10
00:00:21.01 --> 00:00:23.06
When you have values that
have some inherent order,

11
00:00:23.06 --> 00:00:25.04
you use an ordinal scale.

12
00:00:25.04 --> 00:00:26.09
So in a traditional bar chart,

13
00:00:26.09 --> 00:00:29.02
the horizontal axis is often used

14
00:00:29.02 --> 00:00:32.04
to compare data over things
like a period of time,

15
00:00:32.04 --> 00:00:36.04
so the relationship between the
items is actually important.

16
00:00:36.04 --> 00:00:39.04
D3 provides a number of
different ordinal scales

17
00:00:39.04 --> 00:00:41.01
that you can use to plot your data.

18
00:00:41.01 --> 00:00:44.04
The simplest one is a
band scale, and the method

19
00:00:44.04 --> 00:00:47.00
for using those is called scaleBand.

20
00:00:47.00 --> 00:00:49.09
Now this method allows you
to create regular bar charts

21
00:00:49.09 --> 00:00:53.01
from data and manage the
space in between them.

22
00:00:53.01 --> 00:00:55.09
Just like before, we'll
need to specify a domain

23
00:00:55.09 --> 00:00:58.09
and a range, but then we'll
also need to manage a couple

24
00:00:58.09 --> 00:01:01.04
of additional methods
to control the spacing

25
00:01:01.04 --> 00:01:03.03
in between our bars.

26
00:01:03.03 --> 00:01:06.01
Now once we set up our scale,
we can use some properties

27
00:01:06.01 --> 00:01:09.06
and methods available to
control properties of the bars

28
00:01:09.06 --> 00:01:11.09
like the width of the
bars, and in that case,

29
00:01:11.09 --> 00:01:14.05
we'll use the bandwidth method.

30
00:01:14.05 --> 00:01:17.02
For more information,
check out the documentation

31
00:01:17.02 --> 00:01:19.07
for additional info on ordinal scales.

32
00:01:19.07 --> 00:01:22.05
Alright, so let's take a
look at how this works.

33
00:01:22.05 --> 00:01:25.03
So here we're going to add a new variable,

34
00:01:25.03 --> 00:01:29.02
and this is going to be called
xScale, and just like before,

35
00:01:29.02 --> 00:01:32.04
we'll call it the D3
method, and obviously here,

36
00:01:32.04 --> 00:01:34.00
we're going to use something else,

37
00:01:34.00 --> 00:01:38.03
which will be the method called scaleBand,

38
00:01:38.03 --> 00:01:40.06
and to that method, just like before,

39
00:01:40.06 --> 00:01:43.01
we're going to pass a domain,

40
00:01:43.01 --> 00:01:46.00
and this will just take the entire data.

41
00:01:46.00 --> 00:01:49.00
So we'll just use bardata right here.

42
00:01:49.00 --> 00:01:54.01
And then for the range, we'll specify

43
00:01:54.01 --> 00:01:57.01
an array of zero,

44
00:01:57.01 --> 00:02:00.05
and then the width of the container.

45
00:02:00.05 --> 00:02:04.05
So this is the 600 pixel width right here.

46
00:02:04.05 --> 00:02:08.09
So once we have that, then we
can modify what we are using

47
00:02:08.09 --> 00:02:11.08
for the width of the items here.

48
00:02:11.08 --> 00:02:14.09
So instead of the barWidth
variable right here,

49
00:02:14.09 --> 00:02:18.07
we can create a callback

50
00:02:18.07 --> 00:02:22.03
and return the xScale method,

51
00:02:22.03 --> 00:02:26.06
and we'll use the
bandwidth submethod there

52
00:02:26.06 --> 00:02:29.03
to get the width of the bands.

53
00:02:29.03 --> 00:02:31.09
That's obviously going
to be based on the width

54
00:02:31.09 --> 00:02:33.05
of the container here.

55
00:02:33.05 --> 00:02:35.00
In addition to that, we'll also need

56
00:02:35.00 --> 00:02:36.07
to adjust the x position.

57
00:02:36.07 --> 00:02:39.01
We're not going to need
this index anymore.

58
00:02:39.01 --> 00:02:44.01
And what we'll do here
is just return xScale

59
00:02:44.01 --> 00:02:46.06
and that d variable.

60
00:02:46.06 --> 00:02:48.06
Right if we do that, what you're
going to see on your table

61
00:02:48.06 --> 00:02:51.03
is essentially all the data being plotted

62
00:02:51.03 --> 00:02:55.09
and it's going to sort of
fit nicely into the table.

63
00:02:55.09 --> 00:02:58.07
There's not going to be any
spacing in between the elements,

64
00:02:58.07 --> 00:03:01.00
so if we want to adjust
that, then we'll need

65
00:03:01.00 --> 00:03:04.01
to add some additional submethods here.

66
00:03:04.01 --> 00:03:05.09
One of them is called padding,

67
00:03:05.09 --> 00:03:08.06
and into padding we can just put a value.

68
00:03:08.06 --> 00:03:11.07
And what that does is
adds a spacing at the end

69
00:03:11.07 --> 00:03:15.02
and at the beginning,
as well as in between.

70
00:03:15.02 --> 00:03:17.04
If you want to control the
padding a little bit better,

71
00:03:17.04 --> 00:03:20.02
you can use a couple of other methods,

72
00:03:20.02 --> 00:03:23.01
and those are paddingInner,

73
00:03:23.01 --> 00:03:25.09
and I'm going to set that to .3.

74
00:03:25.09 --> 00:03:28.03
That will sort of adjust the spacing

75
00:03:28.03 --> 00:03:30.09
in between the elements, and
then you can also of course

76
00:03:30.09 --> 00:03:34.00
do paddingOuter, which will
let you adjust the spacing

77
00:03:34.00 --> 00:03:39.08
on the outside.

78
00:03:39.08 --> 00:03:42.07
So now we have a different
padding in between the items,

79
00:03:42.07 --> 00:03:45.04
as well as outside, and you can see

80
00:03:45.04 --> 00:03:52.02
that if I add some additional values here,

81
00:03:52.02 --> 00:03:56.01
the bar will adjust nicely
to our additional data.

82
00:03:56.01 --> 00:03:58.03
So the linear scales are pretty useful.

83
00:03:58.03 --> 00:04:00.04
Sometimes you need some type of content

84
00:04:00.04 --> 00:04:03.00
that requires an ordered relation,

85
00:04:03.00 --> 00:04:06.04
and that's what order scales
are for, and the simplest one

86
00:04:06.04 --> 00:04:10.02
that everyone starts
with is the scaleBand,

87
00:04:10.02 --> 00:04:12.09
and learning how to work
with band scales is going

88
00:04:12.09 --> 00:04:14.01
to show you how to work

89
00:04:14.01 --> 00:04:17.08
with really any other
type of ordinal scales.

