1
00:00:00.07 --> 00:00:01.07
- [Narrator] Time based guides

2
00:00:01.07 --> 00:00:03.09
are a little different
and more complicated

3
00:00:03.09 --> 00:00:07.05
than regular guides, so
let's dig into how they work.

4
00:00:07.05 --> 00:00:11.02
The method for time scales
is called scaleTime(),

5
00:00:11.02 --> 00:00:13.00
and of course, it's going
to be a little bit different

6
00:00:13.00 --> 00:00:15.06
from other scales in that it expects

7
00:00:15.06 --> 00:00:18.08
a date based, or time based, domain.

8
00:00:18.08 --> 00:00:20.04
That's perfect for our purposes,

9
00:00:20.04 --> 00:00:21.09
because we're creating a chart

10
00:00:21.09 --> 00:00:25.04
that displays different
temperatures over time.

11
00:00:25.04 --> 00:00:26.04
Because of that, we can use

12
00:00:26.04 --> 00:00:29.05
a few special properties
to create our ticks.

13
00:00:29.05 --> 00:00:32.08
That's a little bit different
than what we did before.

14
00:00:32.08 --> 00:00:35.08
And just as before, we're going
to group elements together,

15
00:00:35.08 --> 00:00:38.04
so that they're separate
from the main chart,

16
00:00:38.04 --> 00:00:40.07
as well as any other guides.

17
00:00:40.07 --> 00:00:42.09
So let's take a look at how this works.

18
00:00:42.09 --> 00:00:45.09
The first thing I need to do
is create a special array,

19
00:00:45.09 --> 00:00:48.08
to hold the values for my dates.

20
00:00:48.08 --> 00:00:51.09
I'm going to go ahead and
hide this sidebar right here,

21
00:00:51.09 --> 00:00:54.04
just so we can see everything
a little bit better.

22
00:00:54.04 --> 00:00:57.07
And then right here, where
I push the temperatures

23
00:00:57.07 --> 00:01:02.02
into their own array, I'm
also going to push some dates,

24
00:01:02.02 --> 00:01:05.05
and the dates are going to come from

25
00:01:05.05 --> 00:01:10.03
a location in my JSON
object, so if you remember,

26
00:01:10.03 --> 00:01:13.06
from what our JSON object looks like,

27
00:01:13.06 --> 00:01:16.06
we have the object with a list, which has

28
00:01:16.06 --> 00:01:19.02
all the individual elements, and in there

29
00:01:19.02 --> 00:01:21.07
we have an item called main that has

30
00:01:21.07 --> 00:01:24.03
all our main information,
like temperature,

31
00:01:24.03 --> 00:01:27.04
minimum/maximum temp
and so on and so forth.

32
00:01:27.04 --> 00:01:28.07
And then at the same level though,

33
00:01:28.07 --> 00:01:31.04
we have this thing called dt_txt,

34
00:01:31.04 --> 00:01:34.05
which gives you the date in a text format.

35
00:01:34.05 --> 00:01:37.08
And as you can see, if I
pull up this other one,

36
00:01:37.08 --> 00:01:41.09
this one is three hours
away from that one,

37
00:01:41.09 --> 00:01:46.02
and actually every
other set is going to be

38
00:01:46.02 --> 00:01:49.06
three hours away from the other one,

39
00:01:49.06 --> 00:01:52.02
so it's going to take us a few of these

40
00:01:52.02 --> 00:01:54.00
to get to a different day.

41
00:01:54.00 --> 00:01:58.08
So we're going to come back
here and push these dates in.

42
00:01:58.08 --> 00:02:00.06
To make sure they are dates, though,

43
00:02:00.06 --> 00:02:03.02
we're going to use a new Date function.

44
00:02:03.02 --> 00:02:05.03
This is just a JavaScript function

45
00:02:05.03 --> 00:02:08.08
that will coerce our
text into actual dates.

46
00:02:08.08 --> 00:02:10.06
And then we'll pass along the information,

47
00:02:10.06 --> 00:02:12.03
so this will be in the data.

48
00:02:12.03 --> 00:02:15.03
Then this will be in the
list element, in the data.

49
00:02:15.03 --> 00:02:17.09
And then we'll use this incrementor here

50
00:02:17.09 --> 00:02:20.06
to loop through the different elements.

51
00:02:20.06 --> 00:02:26.01
And then we'll pass along the dt_txt,

52
00:02:26.01 --> 00:02:29.00
which is where our dates are stored.

53
00:02:29.00 --> 00:02:31.03
So these will actually be real dates,

54
00:02:31.03 --> 00:02:34.09
that we can use in a new scale.

55
00:02:34.09 --> 00:02:36.09
So let's go ahead and create those scales,

56
00:02:36.09 --> 00:02:41.08
just like we did with the yAxis.

57
00:02:41.08 --> 00:02:45.01
We created this yAxisValues and the Ticks,

58
00:02:45.01 --> 00:02:48.07
so we're going to do the same
thing right after xScale.

59
00:02:48.07 --> 00:02:52.09
We're going to do xAxisValues,

60
00:02:52.09 --> 00:02:58.03
and that's going to be
a new d3.scaleTime().

61
00:02:58.03 --> 00:03:05.01
And for our domain, we're
going to use the array.

62
00:03:05.01 --> 00:03:07.03
When you create a time scale, your domain

63
00:03:07.03 --> 00:03:10.01
is going to be set to a series of dates.

64
00:03:10.01 --> 00:03:13.08
And for us, that's going
to be our first date

65
00:03:13.08 --> 00:03:16.07
and our last date, which
we'll have to calculate

66
00:03:16.07 --> 00:03:22.05
by using dates[0], we've got
that array now, full of dates.

67
00:03:22.05 --> 00:03:26.06
And then the first one, of
course, is going to be dates[0].

68
00:03:26.06 --> 00:03:29.02
The last one is going to be dates,

69
00:03:29.02 --> 00:03:32.04
and then we'll use dates, we'll use

70
00:03:32.04 --> 00:03:35.02
JavaScript's length property of arrays.

71
00:03:35.02 --> 00:03:36.05
And we'll have to subtract one,

72
00:03:36.05 --> 00:03:39.09
because length will give
you the amount of elements,

73
00:03:39.09 --> 00:03:42.04
and since JavaScript is zero indexed,

74
00:03:42.04 --> 00:03:47.07
we'll have to subtract one
to get the last element.

75
00:03:47.07 --> 00:03:49.03
The range is going to be pretty simple.

76
00:03:49.03 --> 00:03:51.08
It's just going to be set as an array from

77
00:03:51.08 --> 00:03:58.03
0 to the width of our graphic.

78
00:03:58.03 --> 00:04:02.04
And then we can set the xAxis

79
00:04:02.04 --> 00:04:04.07
and we'll call this xAxisTicks.

80
00:04:04.07 --> 00:04:09.06
So these ticks are going to use an axis

81
00:04:09.06 --> 00:04:10.09
and they're going to go in the bottom,

82
00:04:10.09 --> 00:04:13.00
so we're going to use axis.Bottom,

83
00:04:13.00 --> 00:04:17.03
and then we'll pass along
the values of the xAxis.

84
00:04:17.03 --> 00:04:19.05
And then in here, we'll
need to create our ticks.

85
00:04:19.05 --> 00:04:21.07
We'll still use the ticks function,

86
00:04:21.07 --> 00:04:25.09
but d3 provides a number
of properties here,

87
00:04:25.09 --> 00:04:31.03
that we can use and we can say d3.time

88
00:04:31.03 --> 00:04:34.05
and then something like hour, minute;

89
00:04:34.05 --> 00:04:36.02
in this case we'll want to do day,

90
00:04:36.02 --> 00:04:38.04
so we'll do timeDay and we can say

91
00:04:38.04 --> 00:04:42.00
every, and then an offset here.

92
00:04:42.00 --> 00:04:43.06
So we'll say every single day,

93
00:04:43.06 --> 00:04:46.00
I want you to create a different tick.

94
00:04:46.00 --> 00:04:49.02
And that is going to be
based on this xAxisValues,

95
00:04:49.02 --> 00:04:52.02
which has our range of dates here.

96
00:04:52.02 --> 00:04:55.02
That's all we have to do there.

97
00:04:55.02 --> 00:04:58.00
Once we have that, then we
can start creating our guide.

98
00:04:58.00 --> 00:05:00.01
We'll do it right next to the yGuide.

99
00:05:00.01 --> 00:05:04.00
And we'll do xGuide, it's
going to be equal to,

100
00:05:04.00 --> 00:05:05.05
actually, pretty much like before,

101
00:05:05.05 --> 00:05:08.01
so let's just copy this one,

102
00:05:08.01 --> 00:05:11.04
and we'll change this to xGuide.

103
00:05:11.04 --> 00:05:15.01
And we'll select the
svg and append a group,

104
00:05:15.01 --> 00:05:19.03
so that it's separate from
the chart and the yGuide.

105
00:05:19.03 --> 00:05:22.06
And we'll need to
transform it, just as well,

106
00:05:22.06 --> 00:05:26.04
and that is going to be
pretty much the same,

107
00:05:26.04 --> 00:05:29.02
except we'll have to
account for the the height,

108
00:05:29.02 --> 00:05:31.06
otherwise it'll be up here.

109
00:05:31.06 --> 00:05:39.08
So we'll say, in here,
we will just do height

110
00:05:39.08 --> 00:05:44.02
and then call xAxisTicks.

111
00:05:44.02 --> 00:05:46.04
All right, so it looks like
there's a mistake right here.

112
00:05:46.04 --> 00:05:49.09
Let's check out the console
to see what the problem is.

113
00:05:49.09 --> 00:05:51.03
And if we look at the console,

114
00:05:51.03 --> 00:05:56.04
it says that dates is not
defined, and that's on line 19.

115
00:05:56.04 --> 00:05:58.09
So one of the things I need to do

116
00:05:58.09 --> 00:06:01.03
is make sure that I
actually have a variable

117
00:06:01.03 --> 00:06:08.02
called dates and that needs to be up here.

118
00:06:08.02 --> 00:06:11.00
Let's go ahead and create an
array for that, and save it.

119
00:06:11.00 --> 00:06:14.03
And now you can see the guide down here,

120
00:06:14.03 --> 00:06:17.04
so that's pretty cool.

121
00:06:17.04 --> 00:06:19.05
It's probably a good idea to go ahead

122
00:06:19.05 --> 00:06:23.05
and also create the variables
for these other elements.

123
00:06:23.05 --> 00:06:26.07
JavaScript will automatically
hoist these variables

124
00:06:26.07 --> 00:06:28.06
and create them for you,
if you don't have them,

125
00:06:28.06 --> 00:06:34.02
but it's always good
to make them yourself.

126
00:06:34.02 --> 00:06:37.04
We'll just copy these two, right here.

127
00:06:37.04 --> 00:06:41.09
And change this to xAxis, and then we

128
00:06:41.09 --> 00:06:45.06
actually need to create
the yGuide and xGuide

129
00:06:45.06 --> 00:06:48.07
as well, just to finish that up.

130
00:06:48.07 --> 00:06:52.09
So we'll do the yGuide here.

131
00:06:52.09 --> 00:06:58.08
And we'll do the xGuide here.

132
00:06:58.08 --> 00:07:01.03
Now let's go ahead and clear
that error in our console.

133
00:07:01.03 --> 00:07:05.04
Take a look at our chart and
it looks pretty beautiful.

134
00:07:05.04 --> 00:07:08.09
So time based scales are just
a different sort of scale,

135
00:07:08.09 --> 00:07:10.06
that you can use whenever you have

136
00:07:10.06 --> 00:07:14.01
date or time based elements.

137
00:07:14.01 --> 00:07:16.04
There's a lot of other
different types of scales

138
00:07:16.04 --> 00:07:20.02
that you can use, but these
are the most common ones.

139
00:07:20.02 --> 00:07:23.04
Once you get the hang of doing
a specific type of scale,

140
00:07:23.04 --> 00:07:25.02
all of the other ones are pretty similar.

141
00:07:25.02 --> 00:07:27.07
If you want to take a look at scales

142
00:07:27.07 --> 00:07:29.09
and the different types that you can make,

143
00:07:29.09 --> 00:07:32.02
and time based scales specifically,

144
00:07:32.02 --> 00:07:34.09
make sure you take a look at this URL.

