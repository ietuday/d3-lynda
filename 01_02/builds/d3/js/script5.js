var mydata = [
    {
        date: '18/09/2018',high:80,low:68
    },

    {
        date: '18/08/2018',high:70,low:58
    },

    {
        date: '18/07/2018',high:60,low:48
    },

    {
        date: '18/06/2018',high:50,low:38
    },

    {
        date: '18/05/2018',high:40,low:28
    },

    {
        date: '18/04/2018',high:30,low:18
    }
]

d3.select('tbody')
  .selectAll('tr')
  .data(mydata)
  .enter().append('tr')
  .html(function(d){
      return '<th scope="row">' + d.date + 
             '</th><td>' + d.low +
             '</td><td>' + d.high + 
             '</td>'
  })