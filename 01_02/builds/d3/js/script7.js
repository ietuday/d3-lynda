d3.json('js/data/forecast.json', function(d){
var temperatures = [],
    margin = { top:0, right: 0, bottom: 30, left: 20 }
    height = 450 - margin.top - margin.bottom,
    width = 800 - margin.left - margin.right;
    dates = [];

var tempColor,
    yScale,
    yAxisValues,
    yAxisTicks,
    yGuide,
    xScale,
    xAxisValues,
    xAxisTicks,
    xGuide,
    colors,
    tooltip,
    myChart;

for (let index = 0; index < d.list.length; index++) {
    temperatures.push(d.list[index].main.temp);
    dates.push( new Date(d.list[index].dt_txt) );
}


    yScale = d3.scaleLinear()
                .domain([0, d3.max(temperatures)])
                .range([0, height]);
    
    yAxisValues = d3.scaleLinear()
                   .domain([0, d3.max(temperatures)])
                   .range([height, 0]);
    
    yAxisTicks = d3.axisLeft(yAxisValues)
     .ticks(10)

    xScale = d3.scaleBand()
               .domain(temperatures)
               .paddingInner(.1)
               .paddingOuter(.1)
               .range([0, width])

    xAxisValues = d3.scaleTime()
    .domain([dates[0],dates[(dates.length-1)]])
    .range([0, width])

    xAxisTicks = d3.axisBottom(xAxisValues)
    .ticks(d3.timeDay.every(1))
    
    colors = d3.scaleLinear()
            //    .domain([0, temperatures.length *.33,
            //                temperatures.length *.66,
            //                temperatures.length
            //            ])
               .domain([0, 65, d3.max(temperatures)])
               .range(['#FFFFFF', '#2D8BCF', '#FF0000'])

    tooltip = d3.select('body')
                .append('div')
                .style('position', 'absolute')
                .style('padding', '0 10px')
                .style('background', 'white')
                .style('opacity',0)

    myChart = 
                d3.select('#viz')
                .append('svg')
                    .attr('width', width + margin.left + margin.right)
                    .attr('height', height + margin.top + margin.bottom)
                    .append('g', )
                    .attr('transform', 
                        'translate(' + margin.left + ',' + margin.right + ')' 
                        )
                    // .style('background', '#C9D7D6')
                .selectAll('rect')
                .data(temperatures)
                .enter()
                    .append('rect')
                    .style('fill',colors)
                    .attr('width', function(d){
                        return xScale.bandwidth();
                    })
                    .attr('height',0)
                    .attr('x', function(d){
                            return xScale(d);
                    })
                    .attr('y', height)


                    .on('mouseover', function(d){
                        tooltip.transition().duration(200)
                            .style('opacity', '.9')

                        tooltip.html(
                            '<div style="font-size:1em; font-weight:bold">' + 
                            d + '&deg;' + '</div>'
                        )
                            .style('left', (d3.event.pageX - 35) + 'px')
                            .style('top', (d3.event.pageY - 30) + 'px')

                        tempColor = this.style.fill,
                        d3.select(this)
                        //   .transition()
                        //   .delay(400)
                        //   .duration(1000)
                        //   .style('opacity', .5)
                        .style('fill', 'yellow')
                    })


                    .on('mouseout', function(d){
                        tooltip.html('')
                        d3.select(this)
                        //   .transition()
                        //   .style('opacity', 1)
                        .style('fill', tempColor)
                    });


yGuide = d3.select('#viz svg').append('g')
           .attr('transform', 'translate(20, 0)')
           .call(yAxisTicks)

xGuide = d3.select('#viz svg').append('g')
            .attr('transform', 'translate(20,'+ height + ')')
            .call(xAxisTicks)

myChart.transition()
       .attr('height', function(d){
            return yScale(d);
        })
        .attr('y', function(d, i){
            return height -yScale(d);
        })
        .delay(function(d, i){
            return i*20;
        })
        .duration(1000)
        .ease(d3.easeBounceOut)

});//json import



